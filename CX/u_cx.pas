unit u_cx;

interface

uses
  Windows,SysUtils,Forms ,Classes, Controls, cxInplaceContainer, cxTL,
  cxGridStrs,cxFilterConsts, cxFilterControlStrs,
  DB, cxPropertiesStore,  cxGridDBBandedTableView, cxClasses,  cxGridCustomView,
  Variants, cxImageComboBox,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxCustomData, cxFilter, cxGridExportLink,  Vcl.Dialogs,

    //cxExportGrid4Link,

   u_func,
   u_DB,

   cxDBTL, cxEdit;

//  procedure cx_UpdateSelectedItems_arr(aGrid: TcxGridDBTableView;  aKeyFieldName: string; aParams: array of Variant);


   //
  procedure cx_Footer(aView: TcxGridDBTableView; aColumn: TcxGridColumn);

  procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aFieldName: string);


//  procedure cx_UpdateSelectedItems(aGrid: TcxGridDBBandedTableView;
//      aKeyFieldName, aFieldName: string; aValue: Variant); overload;


//  procedure cx_UpdateSelectedItems(aGrid: TcxGridDBTableView;
//      aKeyFieldName, aFieldName: string; aValue: Variant);overload;


  function cx_IsCursorInGrid(Sender: TObject): Boolean;

  procedure cx_Post(Sender: TcxGridDBTableView);


//  procedure cx_UpdateSelectedItems_Table(aGrid: TcxGridDBTableView; aFieldName: string;
//      aValue: Variant);

//  procedure cx_UpdateSelectedItems_BandedTable(aGrid: TcxGridDBBandedTableView; aFieldName:
//      string; aValue: Variant);

//  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
//      aKeyFieldName, aCheckFieldName: string; aOutStrList: TStringList);overload;

  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aKeyFieldName,
      aCheckFieldName: string; aOutStrList: TStringList);overload;

function cx_CheckCursorInGrid(Sender: TcxGrid; aGridView: TcxCustomGridView): Boolean;

procedure cx_GetSelectedList(Sender: TcxDBTreeList; aKeyColumn:
    TcxDBTreeListColumn; aList: TList);

procedure cx_DBTreeList_Customizing(cxDBTreeList: TcxDBTreeList);


  procedure cx_CheckDataFields(aGrid: TcxGridDBBandedTableView); overload;
  procedure cx_CheckDataFields(aGrid: TcxGridDBTableView); overload;
  procedure cx_CheckDataFields(aTree: TcxDBTreeList); overload;



procedure cx_SetColumnCaptions1(aGrid: TcxGridDBBandedTableView; aFieldNames : array of string);  overload;
procedure cx_Grid_SetColumnCaptions(aGrid: TcxGridDBTableView; aFieldNames :
    array of string); overload;
procedure cx_SetColumnCaptions1(aGrid: TcxDBTreeList; aFieldNames : array  of string); overload;



//procedure cx_SetColumnCaptions1(aGrid: TcxDBTreeList; aFieldNames : array
//    of TStrArray); overload;

//procedure cx_SetColumnCaptions1(aGrid: TcxGridDBBandedTableView; aFieldNames : array
//    of TStrArray); overload;

procedure cxGrid_RemoveUnusedColumns(aView: TcxGridDBBandedTableView);

procedure cxGrid_RemoveUnusedColumns_DBTableView(aView: TcxGridDBTableView);

procedure cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn;
    aReadOnly : boolean);

//procedure cx_ExportGridToExcel(aFileName: string; aGrid: TcxGrid);

//function cx_FindNodeByText(aTreeList1: TcxTreeList; aColumn: TcxTreeListColumn; aText: string):
//    TcxTreeListNode;

//procedure cx_Search_111111111111(aGrid: TcxGridTableView; aText: string);

procedure cx_ExportGridToExcel_dlg(aGrid: TcxGrid);


 // procedure cx_ShowColumns(aGrid: TcxGridDBTableView; aTag : Integer);



procedure cx_ImgComboBox_Add(aCol: TcxGridColumn; aImgIndex: integer;
          aValue: variant; const aDescription: string);

procedure cx_Init_TcxImageComboBoxProperties(aProperties:
    TcxCustomEditProperties; aImageList: TImageList);

procedure cx_Init_GridDBColumn(aColumn: TcxGridDBColumn; aImageList:
    TImageList);




//========================================================================
implementation


//========================================================================

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
end;

//-------------------------------------------------------------------
procedure cx_Footer(aView: TcxGridDBTableView; aColumn: TcxGridColumn);
//-------------------------------------------------------------------
begin
  aView.OptionsView.Footer:=True;
//  aView.DataController.Summary.FooterSummaryItems[0].DataField.

  if aView.DataController.Summary.FooterSummaryItems.Count =0 then
    aView.DataController.Summary.FooterSummaryItems.Add;

  aView.DataController.Summary.FooterSummaryItems[0].Kind:=skCount;

  TcxGridDBTableSummaryItem( aView.DataController.Summary.FooterSummaryItems[0]).Column:=aColumn;

//  aView.DataController.Summary.FooterSummaryItems[0].Field.DataController.f

end;


//--------------------------------------------------------------------------
procedure cx_ImgComboBox_Add(aCol: TcxGridColumn; aImgIndex: integer;
          aValue: variant; const aDescription: string);
//------------------------------------------------------------------------------
var oItem: TcxImageComboBoxItem;
begin
  oItem:= TcxImageComboBoxProperties(aCol.Properties).Items.Add();

  if aImgIndex >= 0 then oItem.ImageIndex:= aImgIndex;
  if aValue <> null then oItem.Value:= aValue;
  if aDescription <> '' then oItem.Description:= aDescription;
end;



// ---------------------------------------------------------------
procedure cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn;
    aReadOnly : boolean);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aColumns) - 1 do
   aColumns[i].Options.Editing := not aReadOnly;
end;



procedure cx_DBTreeList_Customizing(cxDBTreeList: TcxDBTreeList);
begin
  raise Exception.Create('');
//  cxDBTreeList.Customizing.MakeColumnSheetVisible;

end;





//-------------------------------------------------------------------
procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aKeyFieldName,
    aCheckFieldName: string; aOutStrList: TStringList);
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var i: integer;

  bChecked : boolean;
  sKey : string;

//  iKeyFieldInd,iValueFieldInd: integer;
  iCheckColumnInd: integer;

  iKeyColumnInd: Integer;
  iCheckFieldInd: Integer;
begin
  aOutStrList.Clear;


  Assert(aKeyFieldName<>'');
  Assert(aCheckFieldName<>'');

  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aCheckFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  iCheckFieldInd :=aGrid.GetColumnByFieldName(aCheckFieldName).Index;

 // i:=aGrid.DataController.RecordCount;

  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      bChecked:=Values[i, iCheckFieldInd];
      sKey:=Values[i, iKeyColumnInd];

      if bChecked then
        aOutStrList.Add(sKey);
    end;

end;



 //TcxDBTreeList

 //----------------------------------------------------------------------------
procedure cx_SetColumnCaptions1(aGrid: TcxDBTreeList; aFieldNames : array
    of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oColumn: TcxDBTreeListColumn;

begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
    oColumn := TcxDBTreeListColumn(aGrid.Columns[i]);
                                              
   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=oColumn.DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
          aGrid.Columns[i].Caption.Text := aFieldNames[j*2+1]
  end;

  
end;


//----------------------------------------------------------------------------
procedure cx_Grid_SetColumnCaptions(aGrid: TcxGridDBTableView; aFieldNames :
    array of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

 // oColumn: TcxDBTreeListColumn;

begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
          aGrid.Columns[i].Caption := aFieldNames[j*2+1]
  end;
end;



//----------------------------------------------------------------------------
procedure cx_SetColumnCaptions1(aGrid: TcxGridDBBandedTableView; aFieldNames : array
    of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

 // oColumn: TcxDBTreeListColumn;

begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to (Length(aFieldNames) div 2)-1 do
        if Eq(sFieldName, aFieldNames[j*2]) then
          aGrid.Columns[i].Caption := aFieldNames[j*2+1]
  end;
end;





//----------------------------------------------------------------------------
procedure cx_Validate(aGrid: TcxGridDBTableView);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;
  oDataSet: TDataSet;
begin
  oDataSet:=aGrid.DataController.DataSet;

  for I := aGrid.ColumnCount-1 downto 0 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    Assert(Assigned(aGrid.DataController.DataSet));


(*    aGrid.Columns[i].DataBinding.

    if (sFieldName='') or (not Assigned) then


      for j := 0 to High(aFieldNames) do
        if Eq(sFieldName, aFieldNames[j][0]) then
         aGrid.Columns[i].Caption := aFieldNames[j][1]
*)

  end;
end;


  {

//----------------------------------------------------------------------------
procedure cx_SetColumnCaptions1(aGrid: TcxDBTreeList; aFieldNames : array
    of TStrArray);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oDBTreeListColumn: TcxDBTreeListColumn;
begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
    oDBTreeListColumn := TcxDBTreeListColumn(aGrid.Columns[i]);

    sFieldName:=oDBTreeListColumn.DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to High(aFieldNames) do
        if Eq(sFieldName, aFieldNames[j][0]) then
         aGrid.Columns[i].Caption.Text := aFieldNames[j][1]
  end;
end;


  }



//  


procedure cx_AddComponentProp(aPropertiesStore: TcxPropertiesStore; aComponent: TComponent; aFieldName: string);
var
  oStoreComponent: TcxPropertiesStoreComponent;
begin
  oStoreComponent :=  TcxPropertiesStoreComponent(aPropertiesStore.Components.Add);

  oStoreComponent.Component:=aComponent;
  oStoreComponent.Properties.Add(aFieldName);
end;


//-------------------------------------------------------------------
//procedure cx_Dlg_ExportToFile (aGrid: TcxGrid);
//-------------------------------------------------------------------
//const
//  DX_FILTER_STR =
//     'Excel|.xls|' +
//     'HTML|*.html|'+
//     'XML|.xml|'   +
//     'Text|.txt';
//
//  ARRAY_FILTER_EXT : array[1..4] of string =
//    ('.xls','.html','.xml','.txt');
//
//var
//  oSaveDialog: TSaveDialog;
//begin
// (* oSaveDialog:=TSaveDialog.Create(Application);
//  oSaveDialog.Filter:=DX_FILTER_STR;
//
//  with oSaveDialog do
//    if Execute then
//  begin
//     if ExtractFileExt(FileName) = '' then
//       FileName := FileName + ARRAY_FILTER_EXT[FilterIndex];
//
//    case FilterIndex of
//      1: ExportGrid4ToExcel (FileName, aGrid);
//      2: ExportGrid4ToHTML(FileName, aGrid);
//      3: ExportGrid4ToXML (FileName, aGrid);
//      4: ExportGrid4ToText(FileName, aGrid);//, #9, '', '');
//    end;
//  end;
//
//  oSaveDialog.Free;*)
//end;



//-------------------------------------------------------------------
function cx_IsCursorInGrid(Sender: TObject): Boolean;
//-------------------------------------------------------------------   
var
  lHitTest: TcxCustomGridHitTest;
  lPt: TPoint;

begin
  Result := False;

  if (Sender is TcxGridSite) then
  begin
    lPt := TcxGridSite(Sender).ScreenToClient(Mouse.CursorPos);
    lHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(lPt);


    if (lHitTest.HitTestCode = htCell) and
      (lHitTest is TcxGridRecordCellHitTest) and
      Assigned(TcxGridRecordCellHitTest(lHitTest).GridRecord) and
      TcxGridRecordCellHitTest(lHitTest).GridRecord.IsData then
    begin
      Result := True;
    end;
  end;

end;

(*
function cx_SetColumnReadOnly(aColumns: array of TcxDBTreeListColumn; aReadOnly
    : boolean): TcxDBTreeListColumn;
var
  I: Integer;
begin
  for I := 0 to High(aColumns) - 1 do
    aColumns[i].Properties.ReadOnly := aReadOnly;
end;
*)


procedure cx_Post(Sender: TcxGridDBTableView);
begin
  if assigned(Sender.DataController.DataSource.DataSet) then
    with Sender.DataController.DataSource.DataSet do
      if State in [dsEdit,dsInsert] then Post;
end;




// ---------------------------------------------------------------
procedure cx_GetSelectedList(Sender: TcxDBTreeList; aKeyColumn:
    TcxDBTreeListColumn; aList: TList);
// ---------------------------------------------------------------
var
  i: Integer;
  iID: Integer;
begin
  aList.Clear;

 // oList :=TList.Create;

  with Sender do
    for i := 0 to SelectionCount - 1 do
    begin
      iID:=Selections[i].Values[aKeyColumn.ItemIndex];
      aList.Add(Pointer(iID));
    end;
end;

// ---------------------------------------------------------------
function cx_CheckCursorInGrid(Sender: TcxGrid; aGridView: TcxCustomGridView): Boolean;
// ---------------------------------------------------------------
Var
  P: TPoint;
  a: TcxCustomGridView;
  h: TcxCustomGridHitTest;
//  i: integer;
begin
  P := Sender.ScreenToClient(Mouse.CursorPos);

  h:=aGridView.GetHitTest(P);

//  i:=h.HitTestCode;

  Result:=h.HitTestCode = htRecord or htCell;
end;

// ---------------------------------------------------------------
procedure cxGrid_RemoveUnusedColumns_DBTableView(aView: TcxGridDBTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for I := aView.ColumnCount - 1  downto 0 do
  begin
    s:=aView.Columns[i].Caption;

    if aView.Columns[i].Caption='' then
      aView.Columns[i].Free;
  end;
end;

{

procedure cx_ExportGridToExcel(aFileName: string; aGrid: TcxGrid);
begin

//  with TSaveDialog.Create(self) do
//  try
//    DefaultExt := 'xls';
//    Filter := 'Excel files|*.xls';
//    if Execute then
//      ExportGridToExcel(FileName, cxGrid_parcel, False, True, True);
//  finally
//    Free;
//  end;


 raise Exception.Create('');
//  ExportGrid4ToExcel(aFileName, aGrid);
end;
 }


// ---------------------------------------------------------------
procedure cx_ExportGridToExcel_dlg(aGrid: TcxGrid);
// ---------------------------------------------------------------
begin

  with TSaveDialog.Create(nil) do
  try
    DefaultExt := 'xls';
    Filter := 'Excel files|*.xls';
    if Execute then
      ExportGridToExcel(FileName, aGrid, False, True, True);

  finally
    Free;
  end;


// raise Exception.Create('');
//  ExportGrid4ToExcel(aFileName, aGrid);
end;



// ---------------------------------------------------------------
procedure cxGrid_RemoveUnusedColumns(aView: TcxGridDBBandedTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for I := aView.ColumnCount - 1  downto 0 do
  begin
    s:=aView.Columns[i].Caption;

    if aView.Columns[i].Caption='' then
      aView.Columns[i].Free;
  end;

end;

// ---------------------------------------------------------------
procedure cx_CheckDataFields(aGrid: TcxGridDBBandedTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aGrid.DataController.DataSource), 'aGrid.DataController.DataSource not assigned');

  oDataSet:=aGrid.DataController.DataSource.DataSet;

  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aGrid.ColumnCount - 1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)), sFieldName);
  end;

 // if aGrid.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;


// ---------------------------------------------------------------
procedure cx_CheckDataFields(aTree: TcxDBTreeList);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  oDBTreeListColumn: TcxDBTreeListColumn;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aTree.DataController.DataSource), 'aTree.DataController.DataSource not assigned');

  oDataSet:=aTree.DataController.DataSource.DataSet;

  Assert(aTree.DataController.KeyField<>'');
  Assert(aTree.DataController.ParentField<>'');


  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aTree.ColumnCount - 1 do
  begin
    oDBTreeListColumn := TcxDBTreeListColumn(aTree.Columns[i]);


//    sFieldName:=aTree.Columns[i].DataBinding.FieldName;
    sFieldName:=oDBTreeListColumn.DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)), sFieldName);
  end;

 // if aTree.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;


// ---------------------------------------------------------------
procedure cx_CheckDataFields(aGrid: TcxGridDBTableView);
// ---------------------------------------------------------------
var
  I: Integer;
  oDataSet: TDataSet;
  //s: string;
  sFieldName: string;
begin
  Assert(Assigned(aGrid.DataController.DataSource), 'aGrid.DataController.DataSource not assigned');

  oDataSet:=aGrid.DataController.DataSource.DataSet;

  Assert(Assigned(oDataSet));

 // i:=oDataSet.FieldCount;

//  if oDataSet.Active then

  for I := 0 to aGrid.ColumnCount - 1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    Assert(sFieldName<>'');

    Assert(Assigned(oDataSet.FindField(sFieldName)));
  end;

 // if aGrid.DataController.DataSource then


  // TODO -cMM: cx_CheckDataFields default body inserted
end;





// ---------------------------------------------------------------
procedure cx_Init_GridDBColumn(aColumn: TcxGridDBColumn; aImageList:
    TImageList);
// ---------------------------------------------------------------
var
  I: Integer;  
begin  
  with TcxImageComboBoxProperties(aColumn.Properties) do 
  begin
    Images:=aImageList;
    
    for I := 0 to Items.Count-1 do
      Items.Add.Value:=i;
      
  end;   
end;

// ---------------------------------------------------------------
procedure cx_Init_TcxImageComboBoxProperties(aProperties:
    TcxCustomEditProperties; aImageList: TImageList);
// ---------------------------------------------------------------
var
  I: Integer;         
   
begin    
  with TcxImageComboBoxProperties(aProperties) do 
  begin
    Images:=aImageList;
    
    for I := 0 to aImageList.Count-1 do
      Items.Add.Value:=i;
  end;    
end;




begin
  //cx_LocalizeRus();
//  InitRes();
 // dx_Init (Application, Mouse);
end.



(*



{

procedure InitRes;
begin
   cxSetResourceString(@scxGridGroupByBoxCaption, '����������� ���� ��������� ...');
   cxSetResourceString(@scxGridNoDataInfoText    , '* ��� ������ *');
   cxSetResourceString(@scxGridFilterIsEmpty        , '<������ �� �����>');
   cxSetResourceString(@scxGridFilterCustomizeButtonCaption, '�������������..');
   cxSetResourceString(@scxGridDeletingSelectedConfirmationText, '������� ��� ���������� ������ ?');
   cxSetResourceString(@scxGridDeletingFocusedConfirmationText, '������� ��������� ������ ?');
   cxSetResourceString(@scxGridDeletingConfirmationCaption, '�����������...');

end;
}





{
//-------------------------------------------------------------------
procedure dx_ClearDBGridColumnsExcept (adxDBGrid: TdxDBGrid;
                      aExceptColumnArr: array of TdxDBTreeListColumn);
//-------------------------------------------------------------------
// bands[0] - not deleted !
//-------------------------------------------------------------------
var  i,j: integer;
  sName: string;
label
  label_Continue;
begin
  adxDBGrid.BeginUpdate;
 // try

  for i := adxDBGrid.ColumnCount - 1 downto 0 do
  begin
  //  sName:=adxDBGrid.Columns[i].Name;

    for j:=0 to High(aExceptColumnArr) do
      if adxDBGrid.Columns[i] = aExceptColumnArr[j]
        then Goto label_Continue;

    if (adxDBGrid.Columns[i].Tag <> 0) then
      TStringList(adxDBGrid.Columns[i].Tag).Free;
    adxDBGrid.Columns[i].Free;

    label_Continue:
  end;

  //del all except 1
  for i := adxDBGrid.Bands.Count - 1 downto 1 do
    adxDBGrid.Bands[i].Free;

//  finally
  adxDBGrid.EndUpdate;
//  end;
end;
}


{

 var i,j,iID: integer; b: boolean;
  s,sName: string;
  oDataset: TDataset;
  oNode: TdxTreeListNode;

//  oIDList: TIDList;
  oIDList: TStringLIst;
begin
  oIDList:=TStringList.Create;
//  oIDList:=TIDList.Create;

  with aGrid do
    for i := 0 to SelectedCount - 1 do
    begin
      oNode:=SelectedNodes[i];

      if Assigned(oNode) then
      begin
//       oIDList.AddID (oNode.Values [ColumnByFieldName(KeyField).Index]);
        s:=AsString(oNode.Values [ColumnByFieldName(KeyField).Index]);
        oIDList.Add (s);
      end;
    end;

   oDataset:=aGrid.DataSource.Dataset;

   oDataset.DisableControls;

   with aGrid do
     for i := 0 to oIDList.Count - 1 do
      if oDataset.Locate (KeyField, oIDList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);

   oDataset.EnableControls;


  oIDList.Free;
end;
}










{
  procedure dx_ShowHintForDbTreeColumns (aTree: TdxDbTreeList;
    								     	              aHotTrackInfo: TdxTreeListHotTrackInfo;
	  							                      aColumnArray: array of TdxTreeListColumn);

  procedure dx_ShowHintForDbGridColumns (aGrid: TdxDbGrid;
									                      aHotTrackInfo: TdxTreeListHotTrackInfo;
								                        aColumnArray: array of TdxTreeListColumn);
}


{  IdxTools = interface(IInterface)

  end;
}




{//------------------------------------------------------------------------------
procedure dx_ShowHintForDbTreeColumns(aTree: TdxDbTreeList;
								  	  aHotTrackInfo: TdxTreeListHotTrackInfo;
								      aColumnArray: array of TdxTreeListColumn);
//------------------------------------------------------------------------------
begin
  dx_ShowHint (aTree, aHotTrackInfo, aColumnArray);
end;


//------------------------------------------------------------------------------
procedure dx_ShowHintForDbGridColumns(aGrid: TdxDbGrid;
									  aHotTrackInfo: TdxTreeListHotTrackInfo;
								      aColumnArray: array of TdxTreeListColumn);
//------------------------------------------------------------------------------
begin
  dx_ShowHint (aGrid, aHotTrackInfo, aColumnArray);
end;


//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems(aTableView: TcxGridDBBandedTableView; aFieldName: string; aChecked:
    boolean); overload;
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------

 function SelectedRowCount: Integer;
  begin
    Result := aTableView.Controller.SelectedRowCount;
  end;

  function SelectedColumnCount: Integer;
  begin
    Result := aTableView.Controller.SelectedColumnCount;
  end;

{

  function GetSummOfSelection: Integer;
  var
    I, J: Integer;
    val: Variant;
  begin
    Result := 0;
    for I := 0 to SelectedRowCount - 1 do
      for J := 0 to SelectedColumnCount - 1 do
      begin
        val := TableView.DataController.GetValue(
          TableView.Controller.SelectedRows[I].RecordIndex,
          TableView.Controller.SelectedColumns[J].Index);
        if not VarIsNull(val) then
          Inc(Result,  Integer(val));
      end;
  end;}

var
  j: integer;
  v: Variant;
  s: string;
  sKeyField: string;
 // sKeyField: string;
  i: integer;
  iKeyFieldInd,iColumnInd: integer;
  oList: TStringLIst;
  oDataset: TDataset;
begin
  oList:=TStringList.Create;
//  oIDList:=TIDList.Create;




  Assert( Assigned(aTableView.GetColumnByFieldName(aFieldName)));

  sKeyField:=aTableView.DataController.KeyFieldNames;

  iColumnInd:=aTableView.GetColumnByFieldName(aFieldName).Index;
  iKeyFieldInd:=aTableView.GetColumnByFieldName(sKeyField).Index;

//  else
 //   raise Exception.Create('if Assigned(aTableView.GetColumnByFieldName(aFieldName)) then');

//  aTableView.DataController.aTableView.Controller.SelectedRecords[aTableView.Controller.SelectedRecordCount-3].Enabled := ;

{

  i:=aTableView.ViewData.Rows[0].
}
//  aTableView.DataController.r

//    aTableView.DataController.selectedr

   i:=aTableView.Controller.SelectedRecordCount;



{
    for I := 0 to SelectedRowCount - 1 do
      begin
        v := aTableView.DataController.GetValue(
          aTableView.Controller.SelectedRows[I].RecordIndex,  iKeyFieldInd);
      end;
}

    with aTableView.Controller do
      for i := 0 to SelectedRecordCount - 1 do
         if Assigned(aTableView.Controller.SelectedRecords[i]) then
      begin
        v:=SelectedRows[i].Values[iKeyFieldInd];

     //    assert(Assigned(aTableView.Controller.SelectedRecords[i]));

 //..       v:=aTableView.Controller.SelectedRecords[i].Values[iKeyFieldInd];

      //   j:=aTableView.Controller.SelectedRows[i].RecordIndex;

       //  v:=aTableView.DataController.Values[aTableView.Controller.SelectedRows[i].RecordIndex, iKeyFieldInd];

         s:=AsString(v);

  //      s:=aTableView.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];

        oList.Add (s);
      //  aTableView.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;
      end else
        j:=0;



{    with aTableView.Controller do
      for i := 0 to SelectedRowCount - 1 do
         if Assigned(aTableView.Controller.SelectedRows[i]) then
      begin
         assert(Assigned(aTableView.Controller.SelectedRows[i]));

         v:=aTableView.Controller.SelectedRows[i].Values[iKeyFieldInd];

      //   j:=aTableView.Controller.SelectedRows[i].RecordIndex;

       //  v:=aTableView.DataController.Values[aTableView.Controller.SelectedRows[i].RecordIndex, iKeyFieldInd];

         s:=AsString(v);

  //      s:=aTableView.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];

        oList.Add (s);
      //  aTableView.DataController.Values[SelectedRows[i].RecordIndex, iColumnInd]:=aChecked;
      end;
}

   oDataset:=aTableView.DataController.DataSet;;
   oDataset.DisableControls;
  // oDataset.DisableControls;



   with aTableView do
     for i := 0 to oList.Count - 1 do
      if oDataset.Locate (sKeyField, oList[i], []) then
        db_UpdateRecord (oDataset, [db_Par(aFieldName, aChecked)]);

  oDataset.EnableControls;

  oList.Free;
end;



}



{
//----------------------------------------------
procedure cx_ColorRow(arr: array of TcxCustomRow;
                      aSender: TcxCustomRow;
                      ACanvas: TCanvas;
                      var AColor: TColor;
                      aIsRequiredParam: boolean = false;
                      Color: TColor = clSilver );
//----------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(arr) do
  begin
    if aSender = arr[i] then
    begin
      if arr[i].ReadOnly then
        ACanvas.Font.Color:=clGrayText
      else

      if (aIsRequiredParam) then
        AColor:=Color;
    end;
  end;
end;
}



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems_Table(aGrid: TcxGridDBTableView; aFieldName: string;
    aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;

  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
  r:  TcxCustomGridRecord;

  bkm_safe, bkm: TBookmarkStr;
  iColIdx: integer;
  vKeyValue: Variant;
  iFieldIndex: integer;
  sKeyFieldNames: string;
begin
  Screen.Cursor:=crHourGlass;
//  Application.ProcessMessages;

  Assert(aGrid.DataController.KeyFieldNames<>'', 'DataController.KeyFieldNames=''''');

  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
  Assert(Assigned(aGrid.DataController.DataSet));

  oDataSet:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.BeginUpdate;
  aGrid.DataController.BeginLocate;

  iFieldIndex:=oDataset.FieldList.IndexOf(aFieldName);

  sKeyFieldNames:=aGrid.DataController.KeyFieldNames;


  try
    bkm_safe:=oDataSet.Bookmark;

    for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
      if aGrid.DataController.DataModeController.GridMode = True then
      begin
        // GridMode= TRUE !!!!!!!!!
        bkm := aGrid.DataController.GetSelectedBookmark(i);

        if oDataSet.BookmarkValid(TBookmark(bkm)) then
        begin
          oDataSet.Bookmark:=bkm;
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
        end;
      end else
      begin
        iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;
        Assert(iRecIx>=0, 'Value <0');

        Assert(Assigned(aGrid.DataController.GetItemByFieldName(sKeyFieldNames)), 'Value not assigned');

        iColIdx := aGrid.DataController.GetItemByFieldName(sKeyFieldNames).Index;
        Assert(iColIdx>=0, 'Value <0');

        // Obtains the value of the required field
       // OutputVal :=
        vKeyValue:=aGrid.DataController.Values[iRecIx, iColIdx];

        if oDataset.Locate(sKeyFieldNames, vKeyValue, []) then
         if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
      end;

  finally
    if oDataSet.BookmarkValid(TBookmark(bkm_safe)) then
      oDataSet.Bookmark:=bkm_safe;

    aGrid.DataController.EndLocate;
    aGrid.EndUpdate;
  end;

  oDataSet.EnableControls;

  Screen.Cursor:=crDefault;

end;



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems_BandedTable(aGrid: TcxGridDBBandedTableView; aFieldName:
    string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;

  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
  r:  TcxCustomGridRecord;

  bkm_safe, bkm: TBookmarkStr;
  iColIdx: integer;
  vKeyValue: Variant;
  iFieldIndex: Integer;
  oCustomGridTableItem: TcxCustomGridTableItem;
  sKeyFieldNames: string;
begin
 // Application.ProcessMessages;
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));
  Assert(Assigned(aGrid.DataController.DataSet));

  Assert(aGrid.DataController.KeyFieldNames<>'', 'DataController.KeyFieldNames=''''');

  Screen.Cursor:=crHourGlass;


  oDataSet:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.BeginUpdate;
  aGrid.DataController.BeginLocate;

   //��� ��������
  iFieldIndex:=oDataset.FieldList.IndexOf(aFieldName);

  sKeyFieldNames:=aGrid.DataController.KeyFieldNames;

  try
    bkm_safe:=oDataSet.Bookmark;

    for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
      if aGrid.DataController.DataModeController.GridMode = True then
      begin
        // GridMode= TRUE !!!!!!!!!
        bkm := aGrid.DataController.GetSelectedBookmark(i);

        if oDataSet.BookmarkValid(TBookmark(bkm)) then
        begin
          oDataSet.Bookmark:=bkm;
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);

        end;

      end else
      begin
        iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;
        Assert(iRecIx>=0, 'Value <0');


      //  Assert(aKeyFieldName<>'', 'KeyFieldName=''''');
//        Assert(Assigned(aGrid.DataController.GetItemByFieldName(aKeyFieldName)), 'Value not assigned');


        oCustomGridTableItem:=aGrid.DataController.GetItemByFieldName(sKeyFieldNames);
        Assert(Assigned(oCustomGridTableItem), 'oCustomGridTableItem not assigned');


        iColIdx := oCustomGridTableItem.Index;

     //   iColIdx := aGrid.DataController.GetItemByFieldName(sKeyFieldNames).Index;
        Assert(iColIdx>=0, 'Value <0');

        // Obtains the value of the required field
       // OutputVal :=
        vKeyValue:=aGrid.DataController.Values[iRecIx, iColIdx];

        if oDataset.Locate(sKeyFieldNames, vKeyValue, []) then
          if oDataset.Fields[iFieldIndex].Value<>aValue then
            db_UpdateRecord (oDataset, iFieldIndex, aValue);
      end;

  finally
    if oDataSet.BookmarkValid(TBookmark(bkm_safe)) then
      oDataSet.Bookmark:=bkm_safe;

    aGrid.DataController.EndLocate;
    aGrid.EndUpdate;
  end;

  oDataSet.EnableControls;

  Screen.Cursor:=crDefault;

end;



// ---------------------------------------------------------------
procedure Tframe_List.Search;
// ---------------------------------------------------------------
var
 // AItemList: TcxFilterCriteriaItemList;
  I: Integer;
  s: string;
  sFilter: string;

  oFilter: TcxFilterCriteria;

begin
 {
  dmMain.Filter.Enabled :=True;
  dmMain.Filter.Value:= Trim(ed_Search.Text);

//  cxGrid1.Refresh;
  cxGrid1DBBandedTableView1.DataController.Refresh;

//  dmMain.qry_Parcel.r
  
  Exit;
  }
  
  cxGrid1DBBandedTableView1.FilterBox.Visible:=fvNever;// :=false;
  
  
  /////////////////////////////////////////////

  s:=Trim(ed_Search.Text);
  if Length(s)<2 then
  begin
   //  dmMain.Filter.Enabled :=False;
  
    cxGrid1DBBandedTableView1.DataController.Filter.Active := False; 
    
    Exit;
  end;
  
  
  with cxGrid1DBBandedTableView1.DataController.Filter do
  try
    BeginUpdate;
    Root.Clear;

    sFilter:= '%'+ s + '%';

    Root.BoolOperatorKind:= fboOr;
               

    for I := 1 to cxGrid1DBBandedTableView1.ColumnCount-1 do
      Root.AddItem(cxGrid1DBBandedTableView1.Columns[i], foLike, sFilter, sFilter);

  finally
    EndUpdate;
    Active := true;
  end;

end;


uses
  Vcl.Dialogs;



//--------------------------------------------------------------------
function cx_FindNodeByText(aTreeList1: TcxTreeList; aColumn: TcxTreeListColumn; aText: string):
    TcxTreeListNode;
//--------------------------------------------------------------------
var
  I: Integer;
  s: string;
begin

  Result :=nil;

  for i:=0 to aTreeList1.AbsoluteCount-1 Do
  begin
    s:= VarToString( aTreeList1.AbsoluteItems[i].Values[aColumn.ItemIndex]);

//    cxTreeList1.Items[i].Count

    if Eq(s, aText) then
    begin
      Result := aTreeList1.AbsoluteItems[i];
      Break;
    end;
  end;

end;



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems(aGrid: TcxGridDBBandedTableView;
    aKeyFieldName, aFieldName: string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  oStrList: TStringLIst;
  oDataset: TDataset;
  iKeyColumnInd: Integer;
begin
  oStrList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;

  Assert(Assigned(aGrid.DataController.DataSet));

  oDataset:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  with aGrid.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      v:=SelectedRows[i].Values[iKeyColumnInd];

      try
        s:=SelectedRows[i].Values[iKeyColumnInd];
      except

      end;

      oStrList.Add (s);
    end;


   with aGrid do
     for i := 0 to oStrList.Count - 1 do
      if oDataset.Locate (aKeyFieldName, oStrList[i], []) then
        db_UpdateRecord (oDataset, aFieldName, aValue);

  oStrList.Free;

  oDataSet.EnableControls;

end;


//-------------------------------------------------------------------
procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
    aKeyFieldName, aCheckFieldName: string; aOutStrList: TStringList);
//-------------------------------------------------------------------
//  aKeyFieldName - string
//-------------------------------------------------------------------
var i: integer;
 // v: Variant;

  bChecked : boolean;
  sKey : string;

//  iKeyFieldInd,iValueFieldInd: integer;
  iCheckColumnInd: integer;

  iKeyColumnInd: Integer;
  iCheckFieldInd: Integer;
  v: Variant;
begin
  aOutStrList.Clear;


  Assert(aKeyFieldName<>'');
  Assert(aCheckFieldName<>'');

  Assert(Assigned(aOutStrList));
  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aCheckFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  iCheckFieldInd :=aGrid.GetColumnByFieldName(aCheckFieldName).Index;

  i:=aGrid.DataController.RecordCount;

  with aGrid.DataController do
    for i := 0 to RecordCount - 1 do
    begin
      v:=Values[i, iCheckFieldInd];
      bChecked:=AsBoolean(v);

      if bChecked then
      begin
        v:=Values[i, iKeyColumnInd];
        sKey    :=AsString(v);

        aOutStrList.Add(sKey);
      end;
    end;

end;


{

//----------------------------------------------------------------------------
procedure cx_SetColumnCaptions1(aGrid: TcxGridDBBandedTableView; aFieldNames : array
    of TStrArray);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;
begin
  for I := 0 to aGrid.ColumnCount-1 do
  begin
    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;

    if sFieldName<>'' then
      for j := 0 to High(aFieldNames) do
        if Eq(sFieldName, aFieldNames[j][0]) then
         aGrid.Columns[i].Caption := aFieldNames[j][1]
  end;
end;
 }



//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems_arr(aGrid: TcxGridDBTableView;
    aKeyFieldName: string; aParams: array of Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
  iCheckColumnInd: integer;
  oStrList: TStringLIst;
  oDataset: TDataset;
  iKeyColumnInd: Integer;
  iRecIx: integer;
     r:  TcxCustomGridRecord;
begin
  oStrList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  //Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));

  s:=aGrid.DataController.KeyFieldNames;

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  Assert(iKeyColumnInd>=0, 'Value <0');

  Assert(Assigned(aGrid.DataController.DataSet));

  oDataset:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.DataController.SelectAll;


  for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
  begin
    r:=aGrid.Controller.SelectedRecords[I];

    Assert(Assigned(r), 'Value not assigned');
(*
    if not Assigned(r) then
       ShowMessage('');
*)
    Assert(Assigned(aGrid.Controller.SelectedRecords[I]), 'Value not assigned');

    iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;

    oStrList.Add (s);
  end;


  for i := 0 to oStrList.Count - 1 do
  begin
    Assert(oStrList[i]<>'');

    if oDataset.Locate (aKeyFieldName, oStrList[i], []) then
       db_UpdateRecord__ (oDataset, aParams);
  end;

  oStrList.Free;

  oDataSet.EnableControls;

end;





//-------------------------------------------------------------------
procedure cx_UpdateSelectedItems(aGrid: TcxGridDBTableView;
    aKeyFieldName, aFieldName: string; aValue: Variant);
//-------------------------------------------------------------------
// KeyField for BEELINE TN  - hopsname
// -------------------------------------------------------------------
var i: integer;
  v: Variant;
  s: string;
  iKeyFieldInd: integer;
//  iCheckColumnInd: integer;
  oStrList: TStringLIst;
  oDataset: TDataset;
  iKeyColumnInd: Integer;
 // iRecIx: integer;
     r:  TcxCustomGridRecord;
begin
  oStrList:=TStringList.Create;

  Assert(Assigned(aGrid.GetColumnByFieldName(aKeyFieldName)));
  Assert(Assigned(aGrid.GetColumnByFieldName(aFieldName)));

  iKeyColumnInd  :=aGrid.GetColumnByFieldName(aKeyFieldName).Index;
  Assert(iKeyColumnInd>=0, 'Value <0');

  Assert(Assigned(aGrid.DataController.DataSet));

  oDataset:=aGrid.DataController.DataSet;
  oDataSet.DisableControls;

  aGrid.DataController.SelectAll;


  for i := 0 to aGrid.Controller.SelectedRecordCount - 1 do
  begin
    r:=aGrid.Controller.SelectedRecords[I];

    Assert(Assigned(r), 'Value not assigned');
(*
    if not Assigned(r) then
       ShowMessage('');
*)
    Assert(Assigned(aGrid.Controller.SelectedRecords[I]), 'Value not assigned');

 //   iRecIx := aGrid.Controller.SelectedRecords[I].RecordIndex;

    oStrList.Add (s);
  end;


  for i := 0 to oStrList.Count - 1 do
  begin
    Assert(oStrList[i]<>'');

    if oDataset.Locate (aKeyFieldName, oStrList[i], []) then
       db_UpdateRecord (oDataset, aFieldName, aValue);
  end;

  oStrList.Free;

  oDataSet.EnableControls;

end;





//------------------------------------------------------------------------------
procedure cx_LocalizeRus();
//------------------------------------------------------------------------------
begin
  cxSetResourceString(@scxGridGroupByBoxCaption, '���������� ���� ��������� ������� ��� ����������� �� ����...');
  cxSetResourceString(@scxGridFilterIsEmpty    , '<������ �� �����>');
  cxSetResourceString(@scxGridFilterCustomizeButtonCaption, '�������������...');
  cxSetResourceString(@scxGridDeletingSelectedConfirmationText, '������� ��� ���������� ������ ?');
  cxSetResourceString(@scxGridDeletingFocusedConfirmationText, '������� ��������� ������ ?');
//  cxSetResourceString(@scxGridDeletingConfirmationCaption, '�����������...');

  cxSetResourceString(@cxSFilterOperatorEqual       ,  '�����'                );
  cxSetResourceString(@cxSFilterOperatorNotEqual    ,  '�� �����'             );
  cxSetResourceString(@cxSFilterOperatorLess        ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorLessEqual   ,  '������ ��� �����'     );
  cxSetResourceString(@cxSFilterOperatorGreater     ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorGreaterEqual,  '������ ��� �����'     );
  cxSetResourceString(@cxSFilterOperatorLike        ,  '������'               );
  cxSetResourceString(@cxSFilterOperatorNotLike     ,  '�� ������'            );
  cxSetResourceString(@cxSFilterOperatorBetween     ,  '�����'                );
  cxSetResourceString(@cxSFilterOperatorNotBetween  ,  '�� �����'             );
  cxSetResourceString(@cxSFilterOperatorInList      ,  '��'                   );
  cxSetResourceString(@cxSFilterOperatorNotInList   ,  '�� ��'                );


  cxSetResourceString(@cxSFilterOperatorYesterday, '�����');
  cxSetResourceString(@cxSFilterOperatorToday, '�������');
  cxSetResourceString(@cxSFilterOperatorTomorrow, '������');

  cxSetResourceString(@cxSFilterOperatorLastWeek, '��������� ������');
  cxSetResourceString(@cxSFilterOperatorLastMonth, '��������� �����');
  cxSetResourceString(@cxSFilterOperatorLastYear, '��������� ���');

  cxSetResourceString(@cxSFilterOperatorThisWeek, '��� ������');
  cxSetResourceString(@cxSFilterOperatorThisMonth, '���� �����');
  cxSetResourceString(@cxSFilterOperatorThisYear, '���� ���');

  cxSetResourceString(@cxSFilterOperatorNextWeek, '��������� ������');
  cxSetResourceString(@cxSFilterOperatorNextMonth, '��������� �����');
  cxSetResourceString(@cxSFilterOperatorNextYear, '��������� ���');


  cxSetResourceString(@cxSFilterOperatorIsNull, '�����');
  cxSetResourceString(@cxSFilterOperatorIsNotNull, '�� �����');
  cxSetResourceString(@cxSFilterOperatorBeginsWith, '���������� �');
  cxSetResourceString(@cxSFilterOperatorDoesNotBeginWith, '�� ���������� �');
  cxSetResourceString(@cxSFilterOperatorEndsWith, '��������� ��');
  cxSetResourceString(@cxSFilterOperatorDoesNotEndWith, '�� ��������� ��');
  cxSetResourceString(@cxSFilterOperatorContains, '��������');
  cxSetResourceString(@cxSFilterOperatorDoesNotContain, '�� ��������');
  cxSetResourceString(@cxSFilterBoxAllCaption, '(���)');
  cxSetResourceString(@cxSFilterBoxCustomCaption, '(���������...)');
  cxSetResourceString(@cxSFilterBoxBlanksCaption, '(������)');
  cxSetResourceString(@cxSFilterBoxNonBlanksCaption, '(��������)');


  cxSetResourceString(@cxSFilterBoolOperatorAnd, '�');  // all
  cxSetResourceString(@cxSFilterBoolOperatorOr, '���');
  cxSetResourceString(@cxSFilterBoolOperatorNotAnd, '� ��'); // not all
  cxSetResourceString(@cxSFilterBoolOperatorNotOr, '��� ��');
  cxSetResourceString(@cxSFilterRootButtonCaption, '������');
  cxSetResourceString(@cxSFilterAddCondition, '�������� �������');
  cxSetResourceString(@cxSFilterAddGroup, '�������� ������');
  cxSetResourceString(@cxSFilterRemoveRow, '������� �������');
  cxSetResourceString(@cxSFilterClearAll, '��������');
  cxSetResourceString(@cxSFilterFooterAddCondition, '������� ������ ����� �������� �������');

  cxSetResourceString(@cxSFilterGroupCaption, '����������� � ��������');
  cxSetResourceString(@cxSFilterRootGroupCaption, '<������>');
  cxSetResourceString(@cxSFilterControlNullString, '<���>');

  cxSetResourceString(@cxSFilterErrorBuilding, 'Can''t build filter from source');

  //FilterDialog
  cxSetResourceString(@cxSFilterDialogCaption, '��������� �������');
  cxSetResourceString(@cxSFilterDialogInvalidValue, '�������� ��������');
  cxSetResourceString(@cxSFilterDialogUse, '������������');
  cxSetResourceString(@cxSFilterDialogSingleCharacter, '��� ������ �������');
  cxSetResourceString(@cxSFilterDialogCharactersSeries, '��� ������ ��������');
  cxSetResourceString(@cxSFilterDialogOperationAnd, '�');
  cxSetResourceString(@cxSFilterDialogOperationOr, '���');
  cxSetResourceString(@cxSFilterDialogRows, '���������� ������, ��� �������:');

  // FilterControlDialog
  cxSetResourceString(@cxSFilterControlDialogCaption, '�������� �������');
  cxSetResourceString(@cxSFilterControlDialogNewFile, '��������.flt');
  cxSetResourceString(@cxSFilterControlDialogOpenDialogCaption, '�������� �������');
  cxSetResourceString(@cxSFilterControlDialogSaveDialogCaption, '���������� �������');
  cxSetResourceString(@cxSFilterControlDialogActionSaveCaption, '���������...');
  cxSetResourceString(@cxSFilterControlDialogActionOpenCaption, '�������...');
  cxSetResourceString(@cxSFilterControlDialogActionApplyCaption, '���������');
  cxSetResourceString(@cxSFilterControlDialogActionOkCaption, '��');
  cxSetResourceString(@cxSFilterControlDialogActionCancelCaption, '������');
  cxSetResourceString(@cxSFilterControlDialogFileExt, 'flt');
  cxSetResourceString(@cxSFilterControlDialogFileFilter, '����� �������� (*.flt)|*.flt');

end;


{




// ---------------------------------------------------------------
procedure cx_Search_111111111111(aGrid: TcxGridTableView; aText: string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFilter: string;

  oFilter: TcxFilterCriteria;

begin

//  aGrid.FilterBox.Visible:=fvNever;// :=false;


  /////////////////////////////////////////////

  s:=Trim(aText);
  if Length(s)<2 then
  begin
   //  dmMain.Filter.Enabled :=False;

    aGrid.DataController.Filter.Root.Clear;
    aGrid.DataController.Filter.Active := False;


    Exit;
  end;


  with aGrid.DataController.Filter do
  try
    BeginUpdate;
    Root.Clear;

    sFilter:= '%'+ s + '%';

    Root.BoolOperatorKind:= fboOr;


    for I := 1 to aGrid.ColumnCount-1 do
      Root.AddItem(aGrid.Columns[i], foLike, sFilter, sFilter);

  finally
    EndUpdate;
    Active := true;
  end;

end;


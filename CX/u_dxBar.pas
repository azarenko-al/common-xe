unit u_dxBar;

interface
uses Menus,SysUtils,dxBar;

//  procedure dlg_AssignPopupMenu_old (aPopupMenu: TPopupMenu; aTBSubMenu: TTBSubmenuItem);
//  procedure dlg_AssignPopupMenu (aPopupMenu: TPopupMenu; aTBSubMenu: TTBSubmenuItem);


//https://www.devexpress.com/Support/Center/Question/Details/Q512129/how-to-create-menu-item

  procedure dlg_AssignPopupMenu_dx(aPopupMenu: TPopupMenu; adxBarManager: TdxBarManager;  aSubMenu:
      TdxBarSubItem);


//  --    dxBarSubItem_Menu: TdxBarSubItem;



//==================================================================
implementation


//==================================================================

 {
//-------------------------------------------------------------------
procedure dlg_AssignPopupMenu (aPopupMenu: TPopupMenu; aTBSubMenu: TTBSubmenuItem);
//-------------------------------------------------------------------


   // ---------------------------------------------------------------
   procedure DoInsert (aMenuItem: TMenuItem; aTBItem: TTBCustomItem);
   // ---------------------------------------------------------------
   var
    i: integer;
    oTBItem: TTBCustomItem;
   begin

      if aMenuItem.Caption='-' then
        oTBItem:=TTBSeparatorItem.Create(aTBSubMenu)

      else begin
//        aTBItem.
//        oTBItem:=TTBSubmenuItem.Create(aTBItem);

        if aMenuItem.Count=0 then
          oTBItem:=TTBItem.Create(aTBItem)
        else
          oTBItem:=TTBSubmenuItem.Create(aTBItem);


    //    oTBItem:=TTBItem.Create(aTBItem);
        oTBItem.Action :=aMenuItem.Action;
        oTBItem.Caption:=aMenuItem.Caption;
      end;

      aTBItem.Add (oTBItem);
  //  end;


      for i:=0 to aMenuItem.Count-1 do
        DoInsert (aMenuItem.Items[i], oTBItem);



   end;    //


var
  i: integer;
 // oPopupItem: TMenuItem;
//  oTBItem: TTBCustomItem;
begin
  if not Assigned(aPopupMenu) then
    raise Exception.Create('');

  aTBSubMenu.Clear;

//  DoInsert (aPopupMenu);

  for i:=0 to aPopupMenu.Items.Count-1 do
  begin
    DoInsert (aPopupMenu.Items[i], aTBSubMenu);

  end;

end;

}


// ---------------------------------------------------------------
procedure dlg_AssignPopupMenu_dx(aPopupMenu: TPopupMenu; adxBarManager: TdxBarManager; aSubMenu: TdxBarSubItem);
// ---------------------------------------------------------------

   // ---------------------------------------------------------------
   function DoInsert(aMenuItem: TMenuItem; aBarSubItem: TdxBarSubItem):  TdxBarItemLink;
   // ---------------------------------------------------------------
   var
      i: integer;
      oItem: TdxBarItemLink;
      oSubMenu: TdxBarSubItem;

      oButton: TdxBarButton;
      oSubItem: TdxBarSubItem;
    
   begin
   
      if aMenuItem.Count=0 then
      begin
         oButton:=adxBarManager.AddButton;
         oButton.Caption:=aMenuItem.Caption;
         oButton.Action :=aMenuItem.Action;
        
         oItem:=aBarSubItem.ItemLinks.Add;
         oItem.Item:=oButton;           
         
      end else begin
         oSubItem:=adxBarManager.AddSubItem;
         oSubItem.Caption:=aMenuItem.Caption;
        
         oItem:=aBarSubItem.ItemLinks.Add;
         oItem.Item:=oSubItem;              
          
      end;


      Result:=oItem;
                                       
      //-------------------------------------------------

      for i:=0 to aMenuItem.Count-1 do
        if aMenuItem.Items[i].Caption<>'-' then
        begin
          oItem:=DoInsert (aMenuItem.Items[i], oSubItem);

          if (i>0) and (aMenuItem.Items[i-1].Caption='-') then
            oItem.BeginGroup:=true;
        end;   

   end;    //


var
  i: integer;

  oItem: TdxBarItemLink;
begin

  aSubMenu.ItemLinks.Clear;


  for i:=0 to aPopupMenu.Items.Count-1 do
    if aPopupMenu.Items[i].Caption<>'-' then
    begin
      oItem:=DoInsert (aPopupMenu.Items[i], aSubMenu);

      if (i>0) and (aPopupMenu.Items[i-1].Caption='-') then
        oItem.BeginGroup:=true;
    end;
end;



end.
         {

// -------------------------------------------------------------------
procedure CopyMenu1111111111(aMenu: TMainMenu; aToolbar: TToolBar);
// -------------------------------------------------------------------
var  i: integer;
     b: TToolButton;

  //  ����������� ��������� ����������� ��������� �� ������ ���� � ������
  procedure CreateDropdownMenu(aToItems: TMenuItem; aFromItem: TMenuItem);
  var m: TMenuItem;
      j: integer;
  begin
    for j := 0 to aFromItem.Count - 1 do
    begin
      m := TMenuItem.Create(aToItems.Owner);
      m.Caption := aFromItem.Items[j].Caption;
      m.Action := aFromItem.Items[j].Action;
      aToItems.Add(m);
      if aFromItem.Items[j].Count > 0 then
         CreateDropdownMenu(m, aFromItem.Items[j]);
    end;

  end;

begin
  for i := aMenu.Items.Count - 1 downto 0 do
  begin
    // ������ �������� ������
    b := TToolButton.Create(aToolbar);

    b.Parent := aToolbar;
    b.Caption := aMenu.Items[i].Caption;
 //  b.Style:=tbsDropDown;
//    b.AutoPopup:=True;

    // ���� ��� �������, �� �������� Action
    if aMenu.Items[i].Count = 0 then
    begin
      b.Action := aMenu.Items[i].Action;
    end
    // ����� �������� ������� � �������� ��� ����������
    else begin
      b.DropdownMenu := TPopupMenu.Create(nil);
    //  b.DropdownMenu.AutoPopup:=True;

      CreateDropdownMenu(b.DropdownMenu.Items, aMenu.Items[i]);
    end;
  end;
end;



//-------------------------------------------------------------------
procedure dlg_AssignPopupMenu (aPopupMenu: TPopupMenu; aTBSubMenu: TTBSubmenuItem);
//-------------------------------------------------------------------




var
  i: integer;
  oPopupItem: TMenuItem;
  oTBItem: TTBCustomItem;
begin
  if not Assigned(aPopupMenu) then
    raise Exception.Create('');

  aTBSubMenu.Clear;

//  DoInsert (aPopupMenu);

  for i:=0 to aPopupMenu.Items.Count-1 do
  begin

    oPopupItem:=aPopupMenu.Items[i];

  //  if oPopupItem.Count = 0 then


    if oPopupItem.Caption='-' then
      oTBItem:=TTBSeparatorItem.Create(aTBSubMenu)

    else begin
      oTBItem:=TTBItem.Create(aTBSubMenu);
      oTBItem.Action:=oPopupItem.Action;
      oTBItem.Caption:=oPopupItem.Caption;
    end;

    aTBSubMenu.Add (oTBItem);

  end;

end;




    dxBarButton2: TdxBarButton;
    cxBarEditItem1: TcxBarEditItem;

    dxBarButton3: TdxBarButton;
    dxBarToolbarsListItem1: TdxBarToolbarsListItem;
    dxBarListItem1: TdxBarListItem;


    [Delphi]Open in popup window
var
  M: TdxBarManager;
  B1, B2: TdxBarButton;
  I: TdxBarItem;
begin

  M := dxBarManager1;
  // Cria barra vazia do Main Menu
  with M.Bars.Add do
  begin
    DockingStyle := dsTop;
    Visible      := True;
    IsMainMenu   := True;
    Caption      := 'Menu';
  end;

  // Cria a button

  B1 := TdxBarButton.Create(Self);

  with B1 do
  begin
    Caption := 'BarButton 1';
    Category := M.Categories.IndexOf('Main category');
    Description := 'A button';
    OnClick := Button1Click;
  end;

  // create a subitem

  I := TdxBarSubItem.Create(Self);
  I.Caption := '&amp;SubMenu';
//
//// assign item links of the main menu
//
  M.MainMenuBar.LockUpdate := True;
  M.MainMenuBar.ItemLinks.Add.Item := I;
//
  with M.MainMenuBar.ItemLinks.Add do
  begin
    Item := B1;

    BeginGroup := True;
    UserPaintStyle := psCaptionGlyph;
  end;
//
  M.MainMenuBar.LockUpdate := False;
//
//  // a submenu
//
  B2 := TdxBarButton.Create(Self);
  B2.Caption := 'BarButton 2';

  TdxBarSubItem(I).ItemLinks.Add.Item := B1;
//
  with TdxBarSubItem(I).ItemLinks.Add do
  begin
    Item := B2;
    Index := 0; // make it to be the first item link of the submenu
  end;

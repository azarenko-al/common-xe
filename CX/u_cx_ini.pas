unit u_cx_ini;

interface
uses
//  u_func,
  IniFiles, Forms,  cxGridCustomTableView, cxGridTableView,
  cxGridDBBandedTableView,  cxGridDBTableView, cxDBTL, cxDBVGrid;


procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxGridDBTableView; aSection: string=''); overload;
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxDBTreeList; aSection: string=''); overload;
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxDBVerticalGrid; aSection: string=''); overload;
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxGridDBBandedTableView; aSection: string=''); overload;


//     cxGrid1DBBandedTableView1
implementation

uses
  System.SysUtils;

const
  DEF_MAIN = 'main';


//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


//----------------------------------------------------------------------------
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxGridDBTableView; aSection: string='');
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

 // oColumn: TcxDBTreeListColumn;

var
  oIni: TMemIniFile;
  sCaption: string;
  sFileName: string;
begin
  if aSection='' then
    aSection:=DEF_MAIN;


//  if aFileName='' then
  sFileName:=ExtractFilePath(Application.ExeName)+ 'ui.ini';
  Assert(FileExists(sFileName));

  oIni:=TMemIniFile.Create(sFileName,  TEncoding.UTF8);

  for I := 0 to aGrid.ColumnCount-1 do
  begin
   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;
    sCaption:=oIni.ReadString(aSection, sFieldName, '');

    if sCaption<>'' then
      aGrid.Columns[i].Caption := sCaption
  end;

  FreeAndNil(oIni);

end;



 //----------------------------------------------------------------------------
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxDBTreeList; aSection: string='');
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oColumn: TcxDBTreeListColumn;

var
  oIni: TMemIniFile;
  sCaption: string;
  sFileName: string;
begin
  if aSection='' then
    aSection:=DEF_MAIN;

//  if aFileName='' then
  sFileName:=ExtractFilePath(Application.ExeName)+ 'ui.ini';
  Assert(FileExists(sFileName));

  oIni:=TMemIniFile.Create(sFileName,  TEncoding.UTF8);

  for I := 0 to aGrid.ColumnCount-1 do
  begin
    oColumn := TcxDBTreeListColumn(aGrid.Columns[i]);

   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=oColumn.DataBinding.FieldName;
    sCaption:=oIni.ReadString(aSection, sFieldName, '');

    if sCaption<>'' then
      aGrid.Columns[i].Caption.Text := sCaption


  end;


  FreeAndNil(oIni);
end;


//----------------------------------------------------------------------------
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxDBVerticalGrid; aSection: string='');
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oRow: TcxDBEditorRow;
//  oMultiRow: TcxDBMultiEditorRow;

var
  oIni: TMemIniFile;
  sCaption: string;
  sFileName: string;
begin
  if aSection='' then
    aSection:=DEF_MAIN;


//  if aFileName='' then
  sFileName:=ExtractFilePath(Application.ExeName)+ 'ui.ini';
  Assert(FileExists(sFileName));

  oIni:=TMemIniFile.Create(sFileName,  TEncoding.UTF8);

  i:=aGrid.Rows.Count;


  for I := 0 to aGrid.Rows.Count -1 do
  begin
//    // ------------------------------------------
//    if aGrid.Rows[i] is TcxDBMultiEditorRow then
//    begin
//      oMultiRow:=aGrid.Rows[i] as TcxDBMultiEditorRow;
//    end else


    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=aGrid.Rows[i] as TcxDBEditorRow;

      sFieldName:=oRow.Properties.DataBinding.FieldName;


    //  sFieldName:=oColumn.DataBinding.FieldName;
      sCaption:=oIni.ReadString(aSection, sFieldName, '');

      if sCaption<>'' then
        oRow.Properties.Caption:=sCaption;

      //='' then
       // ShowMessage('procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);  '+oRow.Name);
    end;


   // sFieldName:=aGrid.Rows[i].DataBinding.FieldName;

//
//    if sFieldName<>'' then
//      for j := 0 to High(aFieldNames) do
//        if Eq(sFieldName, aFieldNames[j][0]) then
//         aGrid.Columns[i].Caption := aFieldNames[j][1]

  end;

  FreeAndNil(oIni);
end;

//-------------------------------------------------------------------
procedure cx_LoadColumnCaptions_FromIni(aGrid: TcxGridDBBandedTableView; aSection: string='');
//-------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

var
  oIni: TMemIniFile;
//  oIni: TIniFile;
  sCaption: string;
  sFileName: string;
begin
  if aSection='' then
    aSection:=DEF_MAIN;


//  _MemIniU := TMemIniFile.Create(ChangeFileExt(_SettingsPath,
//        'Settings.ini'), TEncoding.UTF8);

//  if aFileName='' then
  sFileName:=ExtractFilePath(Application.ExeName)+ 'ui.ini';
  Assert(FileExists(sFileName));

  oIni:=TMemIniFile.Create(sFileName,  TEncoding.UTF8);

  for I := 0 to aGrid.ColumnCount-1 do
  begin
   // oColumn := aGrid.Columns[i] as TcxDBTreeListColumn;

    sFieldName:=aGrid.Columns[i].DataBinding.FieldName;
    sCaption:=oIni.ReadString(aSection, sFieldName, '');

    if sCaption<>'' then
      aGrid.Columns[i].Caption := sCaption
  end;

  FreeAndNil(oIni);

end;



end.

{

   _MemIniU := TMemIniFile.Create(ChangeFileExt(_SettingsPath,
        'Settings.ini'), TEncoding.UTF8);

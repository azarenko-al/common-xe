﻿unit u_DB;

interface

uses
//  u_log_,


  Vcl.OleAuto,
  SysUtils, Classes, Data.DB, Variants,  Forms, Dialogs, StrUtils, Math, cxCustomData,Windows,
  System.Generics.Collections, codeSiteLogging,

//  codeSiteLogging,


//  I_db_login,


//Classes,Forms,ADODB,SysUtils,Variants, Dialogs,

 // Windows, Sysutils, classes,  Registry, Dialogs,IniFiles,
  //DB, ADODB, ActiveX, ComObj, AdoInt, OleDB,


//   u_Logger,
   u_classes,

//     u_func_arrays,
   u_func, Data.Win.ADODB ; //, Mxarrays;


  //NEW !!!!!!!!!!!!!!!!!!
  procedure db_CreateFields_FromSrcToDest(aSrcDataset, aDestDataset: TDataset);
  procedure db_SaveDatasetPosition(aDataset: TDataset);
  procedure db_RestoreDatasetPosition(aDataset: TDataset);



type
//  TdbCommandFlags = set of (flOpenQuery);
//  TReplaceFlags = set of (rfReplaceAll, rfIgnoreCase);

//..  TDataSetAfterPostEvent = procedure (DataSet: TDataSet) of object;


  TdbFieldRec = record
               Name: string;
               Type_: TFieldType;
               Size: integer;
             end;

//  TdbFieldRecArr = array of TdbFieldRec;

{
  TdbParamRec = record
                  FieldName : string;
                  FieldValue: OleVariant;
                end;

  TdbParamRecArr= array of TdbParamRec;
 }

  // ---------------------------------------------------------------
  TdbFieldItem = class
                 Name: string;
                 Type_: TFieldType;
                 Size: integer;

                 Value: Variant;
               end;

  // ---------------------------------------------------------------
  TdbFieldList = class(TList<TdbFieldItem>)
  // ---------------------------------------------------------------
  private
//    function GetItems(Index: Integer): TdbFieldItem;
  public
    function AddItem(aFieldName: string; aType: TFieldType; aSize: integer=200):
        TdbFieldItem;

    function FindByFieldName(aFieldName : string): TdbFieldItem;

  //  property Items[Index: Integer]: TdbFieldItem read GetItems; default;

  //  function FindBYFieldName(aFieldName : string): TdbParamObj;
  end;

  // ---------------------------------------------------------------
  TdbParamObj = class
  // ---------------------------------------------------------------
  public
    FieldName : string;
    FieldValue: Variant;

    FieldType: TFieldType;

    Stream: TMemoryStream;

    destructor Destroy; override;
//
//
//  TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord,
//    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime,
//    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
//    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
//    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
//    ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd);


    function GetValueForSQL: string;

  end;

  // ---------------------------------------------------------------
  TdbParamList = class(TStringList)
  // ---------------------------------------------------------------
  type
    TVariantArray = array of Variant;

  private
    procedure DelBYFieldName(aFieldName : string);
    function GetItems(Index: Integer): TdbParamObj;
    function GetValueByName(aFieldName : string): Variant;
    procedure SetValueByName(aFieldName : string; aValue: Variant);

  public

    function AddItem(aFieldName: string; aFieldValue: Variant): TdbParamObj;
    function FindBYFieldName(aFieldName : string): TdbParamObj;

    function MakeArr: TVariantArray;

//    function MakeSelectCountSQL133333333(aTableName: string): string;

    function MakeInsertSQL(aTableName: string; aIsTest: boolean = false): string;
    function MakeSelectSQL(aTableName: string; aFieldName: string = ''): string;
    function MakeUpdateString(aTableName: string; aID : integer): string;

//    function LoadFromArr(aParams: array of TDBParamRec): Integer;
    function LoadFromArr_variant(aParams: array of Variant): Integer;

    procedure LoadFromDataset(aDataset: TDataset);


//    procedure SetNullByName(aFieldName : string);

    property Items[Index: Integer]: TdbParamObj read GetItems; default;

    property FieldValues[aFieldName : string]: Variant read GetValueByName write
        SetValueByName;

  end;


(*  //------------------------------------------
  TdbSPParamRecOutput = record
  //------------------------------------------
    FieldName  : string;
    FieldValue : PVariant;
  end;
*)

//  TdbParamRecArray = array of TDBParamRec;


  procedure db_View(aDataSet: TDataSet; aCaption: string = '');

//  procedure db_UpdateAllRecords_(aDataset: TDataset; aFieldName: string;   aFieldValue: variant);

//  procedure db_UpdateAllRecords (aDataset: TDataset; aParams: array of TDBParamRec);


  function db_ExecCommand (aADOConnection: TADOConnection;
                           aSQL   : string;
                           aParams: array of variant): boolean;


  function IIF(aCondition: boolean; TrueValue, FalseValue: variant): variant;

  procedure db_ClearFields (aDataset: TDataset);


//  function  db_OutPar (aFieldName: string; var aFieldValue: PVariant): TdbSPParamRecOutput;


  //---------------------------------------------------------
  // Add, Copy
  //---------------------------------------------------------

//  procedure db_AddRecord1111(aDataset: TDataset;
//                         aFieldNames: array of String;
//                         aFieldValues: array of Variant); overload;


  procedure db_CreateFieldArr (aDataSet: TDataset; aFieldArr: array of TdbFieldRec);

                         
//  function db_AddRecord(aDataset: TDataset; aParams: array of TDBParamRec):
 //     Boolean; overload;



(*  procedure db_AddRecordFromDataset1 (aDataset, aDataSetWithFieldValues: TDataset;
            aFieldNames: array of string; aParams: array of TDBParamRec); overload;
*)

  procedure db_AddRecordFromDataSet(aSrcDataset,aDestDataset: TDataset);

(*  procedure db_AddRecordFromSource (aDataset, aSrcDataset: TDataset;
                                    aFieldNames: array of string;
                                    aParams: array of TDBParamRec);
*)

  procedure db_CopyRecords(aSrcDataset, aDestDataset: TDataset); overload;
  procedure db_CopyRecords(aSrcDataset,aDestDataset: TDataset; aFieldNames: array of string); overload;

  procedure db_CopyRecords_FieldNameList (aSrcDataset, aDestDataset: TDataset; aFieldNames: TStringList);


// TODO: db_CopyRecords_ID_NAME11111122
//procedure db_CopyRecords_ID_NAME11111122(aSrcDataset,aDestDataset: TDataset);

//  procedure db_AddRecordFromDS(aDataset, aDataSetWithFieldValues: TDataset;
 //     aFieldNames: array of string; aParams: array of TDBParamRec);


//  procedure db_UpdateRecord (aDataset: TDataset; aParams: array of TDBParamRec); overload;

  procedure db_UpdateRecord(aDataset: TDataset; aParams: array of Variant);
      overload;

//  procedure db_UpdateRecord (aDataset: TDataset; aFieldName: string; aFieldValue: variant); overload;

//  procedure db_UpdateRecord (aDataset: TDataset; aFieldINdex: integer; aFieldValue: variant); overload;


(*
  procedure db_UpdateRecordFromSource (aDestDataset, aSrcDataset: TDataset;
                                       aFieldNames: array of string;
                                       aParams: array of TDBParamRec); overload;
*)

//  procedure db_UpdateRecordField (aDataset: TDataset; aFieldName: string; aFieldValue: variant);

  //---------------------------------------------------------
  // Fields
  //---------------------------------------------------------

  function db_CreateField(aDataSet: TDataset; aFieldName: string; aType:
      TFieldType; aSize: integer=200): TField;

  //proc//edure db_CreateField(aDataSet: Data.DB.TDataset; aFieldArr: array of
//      TdbFieldRec); overload;

//  procedure db_CreateFieldArr (aDataSet: TDataset; aFieldArr: array of TdbFieldRec);

  function db_CreateCalculatedField(aDataSet: TDataset; aFieldName: string; aType:
      Data.DB.TFieldType = ftString; aSize: integer=200): TField;

  function db_Field(aFieldName: string; aType: Data.DB.TFieldType; aSize:
      integer=200): TdbFieldRec;

  procedure db_PostDataset (aDataset: TDataset);
  function  db_IsEditState (aDataset: TDataset): boolean;
  procedure db_DatasetEdit (aDataset: TDataset);
  procedure db_SetEditState (aDataset: TDataset);

//  function  db_GetFirstID (aTableName: string): integer;
  procedure db_DisableControls (aDataSetArr: array of TDataSet);
  procedure db_EnableControls (aDataSetArr: array of TDataSet);

//
//  function db_OpenQueryByFieldValue (aQuery    : TADOQuery;
//                                aTableName: string;
//                                aKeyField : string;
//                                aID       : integer): boolean; forward;
//


  // ---------------------------------------------------------------
  // current record
  // ---------------------------------------------------------------

  //used in: Complex_Report
  //---------------------------------------------------------
  // record functions
  //---------------------------------------------------------

//  procedure db_DeleteRecords(aADOConnection: TADOConnection; aTableName: string);


  procedure db_CopyFieldValues (aDest,aSrc: TDataSet;
                                aFieldNames: array of string);

  // ---------------------------------------------------------------

  function  db_LoadTextFromBlobField (aBlob: TBlobField): string;


  //---------------------------------------------------------
  // TADOConnection
  //---------------------------------------------------------

//  function db_OpenTable1(aQuery: TADOQuery; aTableName : string; aParams: array
//      of TDBParamRec): Boolean;


//  function db_OpenTableByID(aQuery : TADOQuery; aTableName: string; aID : integer): Boolean;


//  function db_OpenQuery2(aQuery: TADOQuery; aFieldsToSelect, aTableName: string;
//      AParams: Array of TDBParamRec; aOrderField: string=''): boolean;

//
  function db_OpenQuery      (aQuery    : TADOQuery;
                               aSQL      : string;
                               aParams   : array of variant): boolean; overload;

                               
  //function db_OpenQuery_     (aQuery    : TADOQuery;
//                               aSQL      : string;
//                               aParams   : array of Variant): boolean; overload;


  function db_OpenQuery_ParamList  (aQuery    : TADOQuery;
                               aSQL      : string;
                               aParamList: TdbParamList): boolean; overload;


//  function db_OpenQuery      (aQuery : TADOQuery; aSQL : string): boolean; overload;

(*
  function db_OpenQuery       (aQuery    : TADOQuery;
                               aTableName: string;
                               aKeyField : string;
                               aID       : integer): boolean; overload;
*)

(*
  function db_OpenQueryByID_FixedFields(aQuery: TADOQuery; aFieldNames: array of string; aID:
      integer): Boolean;
*)
//  function db_MakeWhereQuery (aParams: array of TDBParamRec): string;


  //---------------------------------------------------------
  // TADOConnection
  //---------------------------------------------------------

//  procedure db_SetComponentADOConn  (aComponent: TComponent; aADOConnection: TADOConnection);
  procedure db_SetComponentADOConnection  (aComponent: TComponent; aADOConnection: TADOConnection);
  procedure db_SetComponentsADOConn (aComponents: array of TComponent; aADOConnection: TADOConnection);
  function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:
      string): boolean;

  //---------------------------------------------------------
  //
  //---------------------------------------------------------


  procedure db_SetFieldsValues(aFromDataset, aToDataset: TDataset);


  procedure db_ReOpen(aDataset: TDataset);

  procedure db_Clear    (aDataset: TDataset);

  procedure db_ClearArr (aDatasetArr: array of TDataset);


// TODO: db_CopyFolders1111
//procedure db_CopyFolders1111(aSrcDataset, aDestDataset: TDataset);

(*  function db_RecordExists (aDataset: TDataset;
                            aFieldName: string; aFieldValue: Variant): boolean;
*)
  //---------------------------------------------------------
  //
  //---------------------------------------------------------
//  function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:
//      string): boolean;
//

  procedure db_GetRecordsToStrings (aDataset: TDataset;
                                    aKeyField, aNameField: string;
                                    aStrings: TStrings);

  procedure db_CopyRecord (aSrcDataset,aDestDataset: TDataset; aFieldNames: array of string); overload;
  procedure db_CopyRecord (aSrcDataset,aDestDataset: TDataset); overload;


  procedure db_GetRecordIDsToIDList(aDataset: TDataset; aIDList: TIDList);

//  procedure db_MakeParamRecFromQuery (aDataSet: TDataSet; var aRec: TDBParamRecArray;
 //                                     aExceptFieldList: array of string);

  procedure db_RefreshQuery(aQuery: TADOQuery; aID: integer = 0; aIDname: string
      = 'ID');

const
//  FLD_ACTION = 'ACTION';

  FLD_GEOMETRY_STR = 'GEOMETRY_STR';

  FLD_NAME = 'NAME';
  FLD_SCHEMA_NAME = 'SCHEMA_NAME';
  
  
  FLD_field_value = 'field_value';
  


  FLD_ID_STR = 'ID_STR';
  FLD_ID=          'ID';
  //   FLD_USER_ID=     'USER_ID';
  FLD_PROJECT_ID=  'PROJECT_ID';
  FLD_ENABLE=      'ENABLE';
  FLD_User_NAME = 'User_NAME';

  //    FLD_UNAME = 'UNAME';

  FLD_Owner = 'Owner';
  FLD_RIGHT = 'RIGHT';

  FLD_user_id = 'user_id';

//  FLD_GeoRegion_id = 'GeoRegion_id';
  
  FLD_VERSION = 'VERSION';
  FLD_date = 'date';
  FLD_modify_date = 'modify_date';

  FLD_ReSULT = 'RESULT';

  FLD_Visible = 'Visible';


  FLD_DEST_ID = 'DEST_ID';


  FLD_CHECKED           = 'CHECKED';
  FLD_DXTREE_ID         = 'tree_id';
  FLD_DXTREE_PARENT_ID  = 'tree_parent_id';

const
  FLD_SERVER   ='SERVER';
  FLD_DATABASE ='DATABASE';
  FLD_LOGIN    ='LOGIN';
  FLD_PASSWORD ='PASSWORD';
  FLD_DOMAIN ='DOMAIN';

  FLD_recID = 'recID';

  FLD_LOCAL = 'local';
  FLD_Local_name = 'Local_name';

  FLD_CREATE_DATE = 'CREATE_DATE';
  

//  FLD_XREF_ID ='XREF_ID';


//  function db_GetFieldType(aADOConnection: TADOConnection; aTableName,aFieldName: string): TFieldType;

//  procedure db_AddToDBParamRecArray (var aRec: TDBParamRecArray; sFieldName: string; sFieldValue: variant);


const
  //-------------------------------------------------------------------
  // common field names
  //-------------------------------------------------------------------
  STRING_       = 'string_';


  FLD_COMMAND ='COMMAND';

  FLD_INDEX  = 'INDEX';
  FLD_HEADER = 'HEADER';

///  FLD_FIELD_NAME,   ftString),
//  FLD_KEY,    ftBoolean)


{


          FLD_FileSize, iFileSize,
          FLD_FileDate, dtFileDate,


          FLD_LAT1, bl_arr[0].B,
          FLD_LAT1, bl_arr[0].L,

          FLD_LAT2, bl_arr[1].B,
          FLD_LAT2, bl_arr[1].L,

          FLD_LAT3, bl_arr[2].B,
          FLD_LAT3, bl_arr[2].L,

          FLD_LAT4, bl_arr[3].B,
          FLD_LAT4, bl_arr[3].L,

          FLD_COL_COUNT, oRel.RowCount,
          FLD_ROW_COUNT, oRel.ColCount

}




  FLD_READONLY = 'READONLY';

  FLD_TOWER_HEIGHT = 'TOWER_HEIGHT';
  FLD_GROUND_HEIGHT = 'GROUND_HEIGHT';


  FLD_FileDate = 'FileDate';

  FLD_DATA = 'Data';

  FLD_GEOREGION_ID = 'GEOREGION_ID';
  FLD_GEOREGION1_ID = 'GEOREGION1_ID';
  FLD_GEOREGION2_ID = 'GEOREGION2_ID';
       
  FLD_region = 'region';

  FLD_property_id = 'property_id';

  FLD_Comment = 'Comment';

  FLD_POSITION = 'POSITION';
  
  FLD_FieldName ='FieldName';

  FLD_SORT          = 'SORT';
  FLD_IS_GROUP      = 'IS_GROUP';

  FLD_TRIGGER_NAME  = 'TRIGGER_NAME';
  FLD_PROCEDURE_NAME= 'PROCEDURE_NAME';
  FLD_TABLE_NAME    = 'TABLE_NAME';
  FLD_TABLENAME     = 'TABLENAME';

  FLD_RECNO       = 'RecNo';
 // FLD_ID          = 'id';

  FLD_DB_ID       = 'db_id';


  FLD_SITE_NAME = 'SITE_NAME';




  FLD_REC_ID = 'REC_ID';

  FLD_PARENT_ID   = 'parent_id';
  FLD_PARENT_NAME = 'parent_NAME';

//  FLD_PROJECT_ID  = 'project_id';
  FLD_FOLDER_ID   = 'folder_id';
  FLD_IS_EDITED   = 'IS_EDITED';
  FLD_GUID        = 'guid';
  FLD_SORTED_STR  = 'SORTED_STR';
  FLD_GUID_       = 'guid_';
  FLD_KEY         = 'key';
//  FLD_NAME        = 'name';

  FLD_Description = 'Description';
  
  FLD_SIZE        = 'size';
   FLD_IS_FOLDER   = 'is_folder';
  FLD_IMAGE_INDEX = 'image_index';
  FLD_IS_ReadOnly = 'is_ReadOnly';
  FLD_CAPTION     = 'caption';
  FLD_TITLE       = 'title';
  FLD_TYPE        = 'type';
//  FLD_CHECKED     = 'checked';
  FLD_ENABLED     = 'enabled';
  FLD_INDEX_     = 'index_';
  FLD_ITEMS       = 'items';
  FLD_CODE        = 'code';
  FLD_COLOR       = 'color';
  FLD_FILENAME    = 'FileName';
  FLD_FileSize = 'FileSize';

  
  FLD_MODE = 'MODE';

  FLD_ACTION = 'ACTION';

  FLD_IS_FILE_EXISTS= 'IS_FILE_EXISTS';

//  FLD_DATA        = 'Data';
  FLD_NODE        = 'Node';
  FLD_CLASS       = 'class';
  FLD_OBJECT_NAME = 'OBJECT_NAME';
  FLD_LON_STR = 'LON_STR';
  FLD_LAT_STR = 'LAT_STR';

  FLD_LON_WGS = 'LON_WGS';
  FLD_LAT_WGS = 'LAT_WGS';


 // FLD_FEATURE_ID   = 'FEATURE_ID';
 // FLD_FEATURE_TYPE = 'FEATURE_TYPE';



 //new

 // FLD_USER_CREATED_ID = 'USER_CREATED_ID';
 // FLD_USER_MODIFY_ID  = 'USER_MODIFY_ID';
  FLD_USER_CREATED    = 'USER_CREATED';
  FLD_USER_MODIFY     = 'USER_MODIFY';

  FLD_IS_MODIFIED = 'IS_MODIFIED';

  FLD_DATE_MODIFY  = 'DATE_MODIFY';
  FLD_DATE_CREATED = 'DATE_CREATED';

  FLD_IS_EXIST      = 'IS_EXIST';

  FLD_ADDRESS   = 'ADDRESS';

  FLD_georegion = 'georegion';
  

  FLD_STATUS    = 'STATUS';

  FLD_CONTENT   = 'CONTENT';
  FLD_EXPANDED  = 'EXPANDED';
  FLD_CATEGORY  = 'CATEGORY';

  FLD_IMG_INDEX       = 'IMG_INDEX';
  FLD_STATE_IMG_INDEX = 'STATE_IMG_INDEX';

  FLD_SORTED_NAME = 'SORTED_NAME';

  FLD_NUMBER='NUMBER';

  FLD_CHILD_COUNT = 'CHILD_COUNT';

  FLD_ERROR = 'Error';
  FLD_COUNT = 'Count';

  FLD_VALUE = 'VALUE';

  FLD_LAT           = 'lat';
  FLD_LON           = 'lon';

  FLD_LAT1          = 'lat1';
  FLD_LON1          = 'lon1';
  FLD_LAT2          = 'lat2';
  FLD_LON2          = 'lon2';

  FLD_LAT3          = 'lat3';
  FLD_LON3          = 'lon3';
  FLD_LAT4          = 'lat4';
  FLD_LON4          = 'lon4';



  FLD_LAT_MIN       = 'LAT_MIN';
  FLD_LON_MIN       = 'LON_MIN';
  FLD_LAT_MAX       = 'LAT_MAX';
  FLD_LON_MAX       = 'LON_MAX';


//  FLD_SYS_ID        = 'sys_id';

  FLD_HEIGHT        = 'height';
  FLD_HEIGHT1       = 'height1';
  FLD_HEIGHT2       = 'height2';

  FLD_GUID_STR      = 'GUID_STR';

  FLD_IS_EXPANDED   = 'IS_EXPANDED';

  FLD_OBJNAME       = 'OBJNAME';

  FLD_HEIGHT_ABS = 'HEIGHT_ABS';
  
  FLD_MI_TYPE       = 'MI_TYPE';

  FLD_LAYER         = 'LAYER';

  FLD_DEFAULT_COLOR = 'DEFAULT_COLOR';

  FLD_transparent   = 'transparent';

  FLD_Alignment     = 'Alignment';

  FLD_FOLDER_NAME   = 'FOLDER_NAME';
  FLD_VALUE_        = 'value_';
  FLD_VALUE2        = 'value2';

  FLD_FIELD_NAME = 'FIELD_NAME';

//  DXTREE_OFFSET1 = 1000000;


// TODO: db_ClearDBParamRecArray
//procedure db_ClearDBParamRecArray (var aRec: TDBParamRecArray);

// TODO: db_IntArrayMakeSqlCondition
//function db_IntArrayMakeSqlCondition(aFieldName: string; var aArr: TIntArray): string;

//  procedure db_GetCheckedList1(aDataset: TDataset; aIDList: TIDList);

  function db_TypeFieldToString11(aField: TFieldType; aSize: Integer): String;

//  function db_JoinParams(aParams1,aParams2: TDBParamRecArray): TDBParamRecArray;

//  function db_MakeParamRecArray(aParams: array of TDBParamRec): TdbParamRecArray;

type
  TForEachProc = procedure(aDataset: TDataSet; var aTerminated: boolean) of object;

  function db_ForEach(aDataset: TDataset; aProc: TForEachProc): boolean;

  procedure db_ClearReadOnly(aDataSet: TDataSet);


function db_IsTableExists(aADOConnection: TADOConnection; aTableName: string):
    Boolean;

function db_MakeWhereSQL(aStrList: TStringList): string;

function db_TableOpen1(aADOTable: TADOTable; aTableName: string = ''): Boolean;

///////procedure db_TableDeleteRecords1111111(aADOTable: TADOTable); overload;

procedure db_TableDeleteRecords(aADOConnection: TADOConnection; aTableName:
    string); overload;

procedure db_SetFieldValue(aDataset: TDataset; aFieldName : string; aFieldValue : Variant);

function db_TableReOpen(aADOTable: TADOTable; aTableName: string = ''): Boolean;

////procedure db_DatasetDeleteRecords111111111111(aDataSet: TDataSet);

procedure db_SetFieldCaption(aDataset: TDataset; aFieldName : string; aCaption : string);

//ocedure db_SetFieldCaptions111111111(aDataset: TDataset; aFieldNames : array of string; aCaptions : array of string);

procedure db_CreateFieldsForDataset(aDataSet: TDataSet);



function db_ExecStoredProc(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant; aIsOpenQuery: boolean = false): Integer;

function db_OpenStoredProc(aProc: TAdoStoredProc; aProcName: string; aParams: Array of  variant; aIsOpenDataset: boolean = true): Integer;


function db_OpenStoredProc_ADOConnection(aADOConnection: TADOConnection; aProcName:  string; aParams: array of variant): Integer;

procedure db_PostDatasets(aComponent: TComponent);



//function db_MakeInsertString1(aTblName: string; aRec: TDBParamRecArray): string;

//function db_SQLScriptParse(aSQL: string): TStringList;

procedure db_CopyRecordsToStringList(aDataset: TDataset; aStringList: TStrings);

function db_MakeUpdateString(aTableName: string; aID : integer; aParamList: TdbParamList): string;

function db_ExecCommand_list(aADOConnection: TADOConnection; aSQL : string;  aParamList: TdbParamList): boolean;
procedure db_CopyRecord_no_post(aSrcDataset,aDestDataset: TDataset);
function db_Make_dbParamList_for_CopyRecord(aSrcDataset,aDestDataset: TDataset): TdbParamList;
function db_MakeInsertQuery_list1(aTableName: string; aParamList: TdbParamList): string;


procedure db_CopyRecord_ExceptFields(aSrcDataset,aDestDataset: TDataSet;
    aExcept: array of string);

function db_GetLocalComputerName: string;

procedure db_UpdateRecord_NoPost(aDataset: TDataSet; aParams: array of Variant);

function db_AddRecord_(aDataset: TDataset; aParams: array of Variant): Boolean;

procedure db_CopyBlobField(aSrcField, aDestField: TField);


procedure db_SetFieldCaptions(aDataset: TDataset; aFieldNames : array of
    Variant);


procedure db_AddRecordsFromDataSet(aSrcDataset,aDestDataset: TDataset);

function db_DateTimeToSqlDateTime(const DT: TDateTime): WideString;

procedure db_DeleteRecordsFromDataset(aDataset: TDataset);

function db_StoredProc_Open(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;

function db_StoredProc_Exec(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;

function db_ADConnection_OpenFromReg(aRegPath: WideString; aConnectionObject:
    TADOConnection): boolean;

function db_ExecStoredProc_v1(aAdoStoredProc: TAdoStoredProc; aProcName:
    string; aParams: array of Variant; aOpenMode: boolean = false): Integer;

function db_ConnectionStr_to_GDAL_connection(aConnectionStr: string): string;

function Dlg_AdoConnection_setup(aConnectionString: string): string;


type
   TException_DBNETLIB_ConnectionWrite = Class(Exception);



//const
//  EMPTY_PARAM: TDBParamRec = (FieldName: '');


//============================================================================
implementation
//============================================================================

uses
  u_sql,

  d_db_ViewDataset, Vcl.Controls;



function Dlg_AdoConnection_setup(aConnectionString: string): string;
begin
  Result:=PromptDataSource(0, aConnectionString);

//  Result:=ADOConnection1.ConnectionString;

end;

//-------------------------------------------------------------------
function db_ConnectionStr_to_GDAL_connection(aConnectionStr: string): string;
//-------------------------------------------------------------------
var
  oSList: TStringList;
  s: string;

  str_arr : TArray<string>;

begin

  str_arr:=Trim(aConnectionStr).Split([';']);


  oSList:=TStringList.Create;

  for s in str_arr do
    oSList.Add (s);


//Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;
  //Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=megafon_WireLess;Data Source=ASTRA\MSSQLSERVER_2014

//  s:=oSList.Values['Data Source'];
 //MSSQL:server=ASTRA\MSSQLSERVER_2014;database=megafon_WireLess;trusted_connection=yes

  Result:='MSSQL:server=' + oSList.Values['Data Source'] +';'+
          'database='     + oSList.Values['Initial Catalog'] + ';';
//          'trusted_connection=yes'  ;

  if oSList.Values['Persist Security Info'] = 'False' then
    Result:=Result + 'trusted_connection=yes'
  else begin
    Result:=Result + 'UID=' + oSList.Values['User ID'] + ';' +
                     'PWD=' + oSList.Values['Password'] ;

  end;
      //;Password=sa;Persist Security Info=True;User ID=sa

//    UID=SA;PWD=DummyPassw0rd"

  FreeAndNil(oSList);

 // Memo1.Text:=Result;

  // TODO -cMM: Tfrm_Main.GetGDAL_connection default body inserted
end;



//  if oSList.Values['Persist Security Info'] = 'False' then
//    Result:=Result + 'trusted_connection=yes'
//  else begin
//    Result:=Result + 'UID=' + oSList.Values['User ID'] + ';' +
//                     'PWD=' + oSList.Values['Password'] ;
//
//  end;

//  function db_Par(aFieldName: string; aFieldValue: Variant): TDBParamRec; forward;


function db_DateTimeToSqlDateTime(const DT: TDateTime): WideString;
begin
  Result := FormatDateTime('yyyy-MM-dd', DT) + ' ' + FormatDateTime('hh:mm:ss', DT);
end;



procedure db_Log_Error(aMsg : string);
begin
//  g_Logger.Error(aMsg);
end;

//procedure db_LogSys(aMsg : string);
//begin
// // g_Log.SysMsg (aMsg);
//end;


procedure db_LogException(aProcName, aMsg : string);
begin
//  g_Logger.Error(aProcName + ' '+ aMsg);
end;


(*var
  LDataSetPositionArr: TIntArray;
*)


//  function db_OpenQueryS     (aQuery    : TADOQuery;
//                               aSQL      : string): boolean; forward;


  procedure db_CopyRecord_FN (aSrcDataset,aDestDataset: TDataset; aFieldNames: array of string); forward;


  procedure db_SetComponentADOConnSingle (aComponent: TComponent; aADOConnection: TADOConnection); forward;
                          


function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll]);
end;

function Eq (Value1,Value2 : string): boolean;
begin
  Result := AnsiCompareText(Trim(Value1),Trim(Value2))=0;
end;


  {
function db_OpenQuery(aQuery : TADOQuery; aSQL : string): boolean;
begin
  result := db_OpenQueryS (aQuery, aSQL);
end;
   }


//procedure db_CreateField(aDataSet: Data.DB.TDataset; aFieldArr: array of
//    TdbFieldRec);
//begin
//  db_CreateFieldArr (aDataSet, aFieldArr);

//end;



//--------------------------------------------------------------------
function db_ExecCommand (aADOConnection: TADOConnection;
                         aSQL   : string;
                         aParams: array of variant): boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
  oADOCommand: TADOCommand;
begin
//  aSQL:=db_UpdateSqlString (aSQL, aParams);

  oADOCommand:=TADOCommand.Create(nil);

  oADOCommand.Connection:=aADOConnection;
 // oADOCommand.Prepared:= True;

  oADOCommand.CommandText:=aSQL;

  try
    for i:=0 to (Length(aParams) div 2)-1 do
    begin
      oParameter:=oADOCommand.Parameters.FindParam (aParams[i*2]);
      if oParameter<>nil then
        try
        oParameter.Value :=aParams[i*2+1];
        except end;
    end;

    oADOCommand.Execute;
    Result:=True;
  except
    Result:=False;
  end;

  oADOCommand.Free;
end;



//--------------------------------------------------------------------
function db_CreateCalculatedField(aDataSet: TDataset; aFieldName: string; aType:
    Data.DB.TFieldType = ftString; aSize: integer=200): TField;
//--------------------------------------------------------------------

var
  k: Integer;
  oField: TField;

begin
  k:=aDataset.Fields.Count;

  oField:= aDataset.Fields.FindField (aFieldName);
  if Assigned(oField) then
  begin
    db_Log_Error( Format('Field removed: %s', [aFieldName]));

    FreeAndNil(oField);

 //   aDataset.Fields.Remove(oField);
  end;

  k:=aDataset.Fields.Count;


  Result:=db_CreateField (aDataSet,aFieldName,aType,aSize);
  if Assigned(Result) then
  begin
    Result.FieldKind:=fkCalculated;
    Result.DataSet:=aDataSet;
  end;
end;


function db_Field(aFieldName: string; aType: Data.DB.TFieldType; aSize:
    integer=200): TdbFieldRec;
begin
  Result.Name:=aFieldName;
  Result.Type_:=aType;
  Result.Size:=aSize;
end;


var
  L_Bookmark: TBookmark;


//--------------------------------------------------------------------
procedure db_SaveDatasetPosition(aDataset: TDataset);
//--------------------------------------------------------------------
begin
  L_Bookmark:=aDataset.GetBookmark;
  aDataset.DisableControls;
end;

//--------------------------------------------------------------------
procedure db_RestoreDatasetPosition(aDataset: TDataset);
//--------------------------------------------------------------------
begin
  aDataset.GotoBookmark(L_Bookmark);
  aDataset.EnableControls;
end;

//-------------------------------------------------------------------
procedure db_DisableControls (aDataSetArr: array of TDataSet);
//-------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aDataSetArr) do
    aDataSetArr[i].DisableControls;
end;

//-------------------------------------------------------------------
procedure db_EnableControls (aDataSetArr: array of TDataSet);
//-------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aDataSetArr) do
    aDataSetArr[i].EnableControls;
end;



{
// -------------------------------------------------------------------
function db_GetFieldType (aADOConnection: TADOConnection; aTableName, aFieldName: string): TFieldType;
// -------------------------------------------------------------------
var
  oQry: TADOQuery;
begin
  oQry:= TADOQuery.Create(Application);
  oQry.Connection:= aADOConnection;
  db_OpenQueryS(oQry, 'SELECT TOP 1 * FROM '+ aTableName);
  Result:=oQry.FieldByName(aFieldName).DataType;

  oQry.Free;
end;

 }

//-------------------------------------------------------------------
procedure db_CopyRecords (aSrcDataset, aDestDataset: TDataset);
//-------------------------------------------------------------------
var
  i: integer;
  oDestFieldNames: TStringList;
begin
  aSrcDataset.Open;

//  aDestDataset.Close;
  aDestDataset.Open;

  oDestFieldNames:= TStringList.Create;
  aDestDataset.GetFieldNames(oDestFieldNames);

  aSrcDataset.First;

  while not aSrcDataset.EOF do
  begin
    aDestDataset.Append;

    for i:=0 to oDestFieldNames.Count-1 do
    begin
      if Assigned(aSrcDataset.FindField(oDestFieldNames[i])) then
        aDestDataset[oDestFieldNames[i]]:= aSrcDataset[oDestFieldNames[i]];
    end;

    aDestDataset.Post;

    aSrcDataset.Next;
  end;

//  oDestFieldNames.Free;

  FreeAndNil(oDestFieldNames);

//  aDestDataset.Close;      
end;


//-------------------------------------------------------------------
procedure db_AddRecordsFromDataSet(aSrcDataset,aDestDataset: TDataset);
//-------------------------------------------------------------------
begin
  aSrcDataset.First;

  while not aSrcDataset.eof do
  begin
    db_AddRecordFromDataSet(aSrcDataset,aDestDataset);

    aSrcDataset.Next;
  end;
end;



//-------------------------------------------------------------------
procedure db_AddRecordFromDataSet(aSrcDataset,aDestDataset: TDataset);  //andrey
//-------------------------------------------------------------------
begin
  try
   // if not (aDestDataset.State in [dsEdit, dsInsert]) then
    aDestDataset.Append;

    db_CopyRecord_no_post(aSrcDataset,aDestDataset);

    aDestDataset.Post;

  except
    on E:Exception do
      db_LogException('u_db.db_AddRecordFromDataSet', E.Message);
  end;

end;



// oParamList: TdbParamList;


//-------------------------------------------------------------------
function db_Make_dbParamList_for_CopyRecord(aSrcDataset,aDestDataset:
    TDataset): TdbParamList;
//-------------------------------------------------------------------
var i: integer;
  oFieldNames: TStringList;
  oDestField: TField;
  sFieldName: string;
begin
  Result := TdbParamList.Create;;

  try
    oFieldNames:= TStringList.Create;

    aSrcDataset.GetFieldNames(oFieldNames);


    for i:=0 to oFieldNames.Count-1 do
    begin
      sFieldName :=oFieldNames[i];
      oDestField:=aDestDataset.FindField(sFieldName);

      if Assigned(oDestField) and (not oDestField.ReadOnly) then
        Result.AddItem(sFieldName, null);

   //     aDestDataset[sFieldName]:= aSrcDataset[sFieldName];
    end;

    FreeAndNil(oFieldNames);


  except
    on E:Exception do
      db_LogException('u_db.db_CopyRecord w/o aFieldNames', E.Message);

    //  g_Log.AddError('u_db.db_CopyRecord w/o aFieldNames', E.Message);
  end;
end;

//---------------------------------------------------------------------------
procedure db_CopyBlobField(aSrcField, aDestField: TField);
//---------------------------------------------------------------------------
var
  k: Integer;
  oBlobField_dest: TBlobField;
  oBlobField_src: TBlobField;
  oStream: TMemoryStream;
begin
  //----------------------------------------------------
  if aSrcField.DataType = ftBlob then
    if aDestField.DataType = ftBlob then
  begin
    oStream:=TMemoryStream.Create;

    oBlobField_dest:=TBlobField(aDestField);


    oBlobField_src:=TBlobField(aSrcField);
  //  oBlobField_src.SaveToFile('s:\111111.bin');

    oBlobField_src.SaveToStream(oStream);
    oBlobField_dest.LoadFromStream(oStream);

    k:=oStream.Size;
//    k:=oBlobField.BlobSize;

    FreeAndNil(oStream);
  end;

end;


//-------------------------------------------------------------------
procedure db_CopyRecord_no_post(aSrcDataset,aDestDataset: TDataset);
//-------------------------------------------------------------------
var i: integer;
 // dt: TFieldType;
  k: Integer;
  oFieldNames: TStringList;
  oSrcField: TField;
  oDestField: TField;
  sFieldName: string;


  oBlobField_src : TBlobField;
  oBlobField_dest: TBlobField;
  oStream: TMemoryStream;
//  s: string;
  //v: Variant;

begin
  try
    oFieldNames:= TStringList.Create;

    aSrcDataset.GetFieldNames(oFieldNames);


    for i:=0 to oFieldNames.Count-1 do
    begin
      sFieldName :=oFieldNames[i];
      oDestField:=aDestDataset.FindField(sFieldName);

      oSrcField:=aSrcDataset.FindField(sFieldName);


     // v:= aSrcDataset[sFieldName];

//      oDestField.DataTyp

      //dt:=oDestField.DataType;



      if Assigned(oDestField) and (not oDestField.ReadOnly) then
//      oDestField.
//        aDestDataset.FieldByName(sFieldName).Value

{
      case oDestField.DataType of    //
        ftBlob: ;
      end;
}
      //----------------------------------------------------
      if oDestField.DataType <> ftBlob then
        aDestDataset[sFieldName]:= aSrcDataset[sFieldName]

      //----------------------------------------------------
      else if oDestField.DataType = ftBlob then
      begin
        db_CopyBlobField (oSrcField, oDestField);


      end;


    end;

    FreeAndNil(oFieldNames);


  except
    on E:Exception do
      db_LogException('u_db.db_CopyRecord w/o aFieldNames', E.Message);

    //  g_Log.AddError('u_db.db_CopyRecord w/o aFieldNames', E.Message);
  end;
end;


procedure db_SetEditState (aDataset: TDataset);
begin
  if not (aDataset.State in [dsInsert,dsEdit]) then
    aDataset.Edit;
end;


//-------------------------------------------------------------------
procedure db_CreateFields_FromSrcToDest(aSrcDataset, aDestDataset: TDataset);
//procedure db_CreateFields (aSrcDataset, aDestDataset: TDataset);
//-------------------------------------------------------------------
var
  i: Integer;
  k1: Integer;
  k2: Integer;

  oSrcField, oDestField: TField;// TField;

begin
  Assert(aSrcDataset.FieldList.Count>0);


  aDestDataset.Close;
  aDestDataset.FieldList.Clear;


  k1:=aSrcDataset.FieldList.Count;
  k2:=aDestDataset.FieldList.Count;

  for i:=0 to aSrcDataset.FieldList.Count-1 do
  begin
//    aDestDataset.FieldDefList.f

  //  oSrcField:=aSrcDataset.FieldList[i];
    oSrcField:=aSrcDataset.FieldList[i];

    oDestField:=aDestDataset.FieldList.Find(oSrcField.FieldName);

    if not Assigned(oDestField) then
    begin
      if oSrcField.Calculated then
        db_CreateCalculatedField (aDestDataset,
                      oSrcField.FieldName, oSrcField.DataType, oSrcField.DataSize)
      else
//        oSrcField.CreateField(aDestDataset);
      //  aDataSet.FieldDefList[i].CreateField(aDataSet);

       db_CreateField (aDestDataset,
                     oSrcField.FieldName, oSrcField.DataType, oSrcField.DataSize);
    end;
  end;


  Assert(aSrcDataset.FieldList.Count = aDestDataset.FieldList.Count);


//  aDestDataset.Open;

end;

//
//procedure db_UpdateAllRecords_(aDataset: TDataset; aFieldName: string;
//    aFieldValue: variant);
//begin
//  db_UpdateAllRecords (aDataset, [db_Par(aFieldName, aFieldValue)]);
//end;

{
//--------------------------------------------------------------------
procedure db_UpdateAllRecords (aDataset: TDataset; aParams: array of Variant );
//--------------------------------------------------------------------
var
  I: Integer;
begin
  with aDataset do
  begin
    DisableControls;
  //  db_DisableControls([aDataset]);

    First;

    while not EOF do
    begin

      for I := 0 to (length(aParams) div 2)-1 do
        if aDataset[aParams[i].FieldName] <> aParams[i].FieldValue then
        begin
          if aDataset.State <> dsEdit then
            Edit;

          aDataset[aParams[i].FieldName]:= aParams[i].FieldValue;

        end;

//       if aDataset.State = dsEdit then
  //    Post;

      Next;
    end;

     First;

    EnableControls;
 //   db_EnableControls([aDataset]);
  end;
end;
 }

{
//----------------------------------------------------------------------------
procedure db_UpdateRecord (aDataset: TDataset; aFieldName: string; aFieldValue: variant);
//----------------------------------------------------------------------------
begin
  try
    if aDataset[aFieldName] <> aFieldValue then
      db_UpdateRecord(aDataset, [aFieldName, aFieldValue]);

  except
    on E: Exception do raise Exception.Create(aFieldName);
  end;

end;
 }

{

//----------------------------------------------------------------------------
procedure db_UpdateRecord (aDataset: TDataset; aFieldINdex: integer; aFieldValue: variant);
//----------------------------------------------------------------------------
begin
  try
    aDataset.Edit;
    aDataset.Fields[aFieldINdex].Value:=aFieldValue;
    aDataset.Post;
  except
  end;
end;
}

//----------------------------------------------------------------------------
procedure db_UpdateRecord(aDataset: TDataset; aParams: array of Variant);
//----------------------------------------------------------------------------
var i: integer;
begin
  Assert(assigned(aDataset));

  Assert((Length(aParams)) mod 2=0);

 // try
  //  aDataset.Edit;



  try
    aDataset.Edit;

    for i:=0 to (Length(aParams)-1) div 2  do
      if aDataset[aParams[i*2]] <> aParams[i*2+1] then
         aDataset.FieldValues[aParams[i*2]]:=aParams[i*2+1];


//    for i:=0 to High(aParams)  do
  //    aDataset.FieldValues[aParams[i].FieldName]:=aParams[i].FieldValue;
    aDataset.Post;

  except
    on E: Exception do begin
//      gl_DB.ErrorMsg:= E.Message;
      //g_Log.AddRecord('u_db', 'db_UpdateRecord', E.Message);
    end;
  end;
end;



//----------------------------------------------------------------------------
procedure db_UpdateRecord_NoPost(aDataset: TDataSet; aParams: array of Variant);
//----------------------------------------------------------------------------
var i: integer;
begin
  db_SetEditState (aDataset);

  i:=(Length(aParams));

  Assert((Length(aParams)) mod 2=0);

  try
  //  aDataset.Edit;
   for i:=0 to (Length(aParams)-1) div 2  do
      if aDataset[aParams[i*2]] <> aParams[i*2+1] then
         aDataset.FieldValues[aParams[i*2]]:=aParams[i*2+1];

 //   aDataset.Post;

  except
    on E: Exception do begin
//      gl_DB.ErrorMsg:= E.Message;
      //g_Log.AddRecord('u_db', 'db_UpdateRecord_NoPost', E.Message);
    end;
  end;
end;




procedure db_CopyRecord (aSrcDataset,aDestDataset: TDataset; aFieldNames: array of string);
begin
  db_CopyRecord_FN (aSrcDataset,aDestDataset, aFieldNames);
end;

// ---------------------------------------------------------------
function FindInStringArray(aValue: string; aExcept: array of string): Boolean;
// ---------------------------------------------------------------
var
  k: Integer;
begin
  for k := 0 to High(aExcept) do
    if Eq(aExcept[k], aValue) then
    begin
      Result := True;
      Exit;
    end;

  Result := False;
end;



// ---------------------------------------------------------------
procedure db_CopyRecord_ExceptFields(aSrcDataset,aDestDataset: TDataSet;
    aExcept: array of string);
// ---------------------------------------------------------------
var
  i: integer;
  oDestFieldNames: TStringList;
  oDestField, oSrcField: TField;
  sFieldName: string;
begin
  try
    oDestFieldNames:= TStringList.Create;

    aDestDataset.GetFieldNames(oDestFieldNames);

  //  oField:=aSrcDataset.FindField('id');


    for i:=0 to oDestFieldNames.Count-1 do
      if not FindInStringArray(oDestFieldNames[i], aExcept) then
    begin
      sFieldName := oDestFieldNames[i];


      oSrcField :=aSrcDataset.FindField(sFieldName);
      oDestField:=aDestDataset.FindField(sFieldName);

      if Assigned(oSrcField) and (not oDestField.ReadOnly) then
        aDestDataset[sFieldName]:= aSrcDataset[sFieldName];

    end;

    oDestFieldNames.Free;

  except
    on E:Exception do
    begin
      //g_Log.AddRecord('u_db', 'db_CopyRecord_ExceptFields', E.Message);
      Raise;
    end;
  end;
end;


//-------------------------------------------------------------------
procedure db_CopyRecord (aSrcDataset,aDestDataset: TDataset);
//-------------------------------------------------------------------
var i: integer;
  oDestFieldNames: TStringList;
  oDestField, oSrcField: TField;
  sFieldName: string;
begin
  try
    oDestFieldNames:= TStringList.Create;

    aDestDataset.GetFieldNames(oDestFieldNames);

  //  oField:=aSrcDataset.FindField('id');


    for i:=0 to oDestFieldNames.Count-1 do
    begin
      sFieldName := oDestFieldNames[i];

      oSrcField :=aSrcDataset.FindField(sFieldName);
      oDestField:=aDestDataset.FindField(sFieldName);

      if Assigned(oSrcField) and (not oDestField.ReadOnly) then
        aDestDataset[sFieldName]:= aSrcDataset[sFieldName];

    end;

    oDestFieldNames.Free;

  except
    on E:Exception do
    begin
      //g_Log.AddRecord('u_db', 'db_CopyRecord w/o aFieldNames', E.Message);
      Raise;
    end;
  end;
end;



//--------------------------------------------------------------------
function db_ExecCommand_list(aADOConnection: TADOConnection; aSQL : string;
    aParamList: TdbParamList): boolean;
//--------------------------------------------------------------------
var
  i: integer;
  oADOCommand: TADOCommand;
  oParameter: TParameter;
  s: string;
  v: Variant;
begin
  oADOCommand:=TADOCommand.Create(nil);

  oADOCommand.Connection:=aADOConnection;
  oADOCommand.CommandText:=aSQL;


  i:=Length(aSQL);

  try
    for I := 0 to oADOCommand.Parameters.Count - 1 do    // Iterate
    begin
      oParameter:=oADOCommand.Parameters[i];

      try
        v:=aParamList.GetValueByName (oParameter.Name);

        s:=VarToStr(v);

        Assert(s<>'-');

        oParameter.Value := aParamList.GetValueByName (oParameter.Name);

      finally // wrap up

      end;    // try/finally

    end;    // for

    
   // oParameter:=oADOCommand.Parameters.FindParam (aParamList[i].FieldName);

   {
    for i:=0 to aParamList.Count-1 do
//      if not VarIsNull (aParamList[i].FieldValue)  then
      begin
        oParameter:=oADOCommand.Parameters.FindParam (aParamList[i].FieldName);

        if Assigned(oParameter) then
          try
//            oParameter.

            oParameter.Value :=aParamList[i].FieldValue;
          except
            ShowMessage(aParamList[i].FieldName);
          end;
      end;
    }


    oADOCommand.Execute;

    Result:=True;
  except
    Result:=False;
  end;

  FreeAndNil(oADOCommand);

end;



//===========================================================================//
//  Îáùàÿ ïðîöåäóðà îáíîâëåíèÿ çàïðîñà ñ ïîçèöèîíèðîâàíèåì íà òîé æå ñòðîêå
//
//===========================================================================//
procedure db_RefreshQuery (aQuery: TADOQuery; aID: integer = 0; aIDname: string = 'ID');
begin
  if (aID=0) AND (NOT aQuery.IsEmpty) then
    aID:=aQuery.FieldByName(aIDname).AsInteger;
  aQuery.Active:=false;
  aQuery.Active:=true;
  aQuery.Locate(aIDname, aID, []);
end;

//============================================================================//
// Çàãðóçèì òåêñò èç ÌÅÌÎ-ïîëÿ
//
//============================================================================//
function db_LoadTextFromBlobField (aBlob: TBlobField): string;
//function db_LoadTextFromBlobField (aBlob: TBlobField): string;

//----------------------------------------------------------------------------
var lst: TStringList;
    meStream: TMemoryStream;
begin
  if (aBlob.IsNull)
    then
      result:=''
    else
    begin
      lst:=TStringList.Create;
      meStream:=TMemoryStream.Create;

      try
        aBlob.SaveToStream(meStream);
        meStream.Position:=0;
        lst.LoadFromStream(meStream);
        result:=lst.Text;
      finally
        meStream.Free;
        lst.Free;
      end;
    end;
end;

{
//--------------------------------------------------------------------
function db_OpenQueryS(aQuery : TADOQuery; aSQL : string): Boolean;
//--------------------------------------------------------------------
begin
 // result := db_OpenQuery (aQuery, aSQL, [EMPTY_PARAM]);
  result := db_OpenQuery (aQuery, aSQL, []);
end;
 }

 {
//--------------------------------------------------------------------
function db_OpenTableByID(aQuery : TADOQuery; aTableName: string; aID :
    integer): Boolean;
//--------------------------------------------------------------------
begin
  Assert (aQuery.Connection<>nil, 'db_OpenQuery:aQuery.Connection<>nil');

  result := db_OpenQuery (aQuery,
                'SELECT * FROM ' + aTableName + ' WHERE id=:id',
                [FLD_ID, aID]  );
end;
  }

//
////--------------------------------------------------------------------
//function db_OpenQueryByFieldValue(aQuery : TADOQuery; aTableName: string;
//    aKeyField : string; aID : integer): Boolean;
////--------------------------------------------------------------------
//begin
//  db_OpenQuery (aQuery,
//     Format('SELECT * FROM %s WHERE %s=:%s', [aTableName,aKeyField,aKeyField]),
//    [aKeyField, aID]  );
//end;
//


//-------------------------------------------------------------------
procedure db_SetComponentsADOConn (aComponents: array of TComponent; aADOConnection: TADOConnection);
//-------------------------------------------------------------------
var i: Integer;
begin
{  if not Assigned(aADOConnection) then
    raise Exception.Create('ADOConnection=nil');
}

  for i :=0  to High(aComponents) do
    db_SetComponentADOConnSingle (aComponents[i], aADOConnection);

end;

//-------------------------------------------------------------------
procedure db_SetComponentADOConnection (aComponent: TComponent; aADOConnection: TADOConnection);
//-------------------------------------------------------------------
var i: Integer;
    s: string;
begin
  Assert(Assigned(aADOConnection));

{
  if not Assigned(aADOConnection) then
    raise Exception.Create('ADOConnection=nil');
}

  with aComponent do
    for i :=0  to ComponentCount-1 do
  begin
    s:=Components[i].ClassName;

    db_SetComponentADOConnSingle (Components[i], aADOConnection) ;
  end;
end;

//--------------------------------------------------------------------
procedure db_GetRecordsToStrings(aDataset: TDataset;
   aKeyField, aNameField: string; aStrings: TStrings);
//--------------------------------------------------------------------
begin
  aStrings.Clear;

  with aDataset do
  while not EOF do begin
    aStrings.AddObject (FieldByName(aNameField).AsString,
                        Pointer(FieldByName(aKeyField).AsInteger) );
    Next;
  end;
end;

//--------------------------------------------------------------------
procedure db_GetRecordIDsToIDList(aDataset: TDataset; aIDList: TIDList);
//--------------------------------------------------------------------
begin
  aIDList.Clear;

  with aDataset do   while not EOF do
  begin
    aIDList.AddID (FieldValues[FLD_ID]);
    Next;
  end;
end;


//-------------------------------------------------------------------
procedure db_CopyRecord_FN (aSrcDataset,aDestDataset: TDataset; aFieldNames: array of string);
//-------------------------------------------------------------------
var i: integer;
begin
  try
//    db_SetFieldValue (aDestDataset, aSrcDataset[aFieldNames[i]];

    for i:=0 to High(aFieldNames) do
      aDestDataset[aFieldNames[i]]:=aSrcDataset[aFieldNames[i]];
  except
    on E:Exception do
      begin 

      //g_Log.AddRecord('u_db', 'db_CopyRecord', E.Message); Raise; 
      end;
  end;
end;


//-------------------------------------------------------------------
procedure db_CopyRecords(aSrcDataset,aDestDataset: TDataset; aFieldNames: array
    of string);
//-------------------------------------------------------------------
var
  i: integer;
begin
  aDestDataset.Close;
  aDestDataset.Open;

  aDestDataset.DisableControls;
  aSrcDataset.DisableControls;

  with aSrcDataset do
    while not EOF do
  begin
    aDestDataset.Append;
    try
        for i:=0 to High(aFieldNames) do
          if Assigned(aDestDataset.FindField(aFieldNames[i])) then
            aDestDataset[aFieldNames[i]]:=aSrcDataset[aFieldNames[i]];

    //  db_CopyRecord_FN (aSrcDataset,aDestDataset, aFieldNames);
    except
      on E:Exception do
       // begin
        //g_Log.AddRecord('u_db', 'db_CopyRecord', E.Message);
        // Raise; end;

    end;

    aSrcDataset.EnableControls;
    aDestDataset.EnableControls;

    aDestDataset.Post;
    Next;
  end;

  aSrcDataset.EnableControls;
  aDestDataset.EnableControls;
end;

//-------------------------------------------------------------------
procedure db_CopyRecords_FieldNameList (aSrcDataset, aDestDataset: TDataset;
                                        aFieldNames: TStringList);
//-------------------------------------------------------------------
var
  I: Integer;
begin
  aDestDataset.Close;
  aDestDataset.Open;

  aDestDataset.DisableControls;
  aSrcDataset.DisableControls;

  with aSrcDataset do
    while not EOF do
  begin
    aDestDataset.Append;
    for i:= 0 to aFieldNames.Count-1 do
      db_CopyRecord_FN (aSrcDataset, aDestDataset, [aFieldNames[i]]);
    aDestDataset.Post;
    Next;
  end;

  aSrcDataset.EnableControls;
  aDestDataset.EnableControls;
end;




procedure db_CopyFieldValues (aDest,aSrc: TDataSet;
                              aFieldNames: array of string);
var i: integer;
begin
  for i:=0 to High(aFieldNames) do
    aDest.FieldValues[aFieldNames[i]]:=aSrc.FieldValues[aFieldNames[i]];
end;

procedure db_ReOpen(aDataset: TDataset);
begin
  aDataset.Close;
  aDataset.Open;
end;

procedure db_Clear (aDataset: TDataset);
begin
  aDataset.Close;
  aDataset.Open;
end;

procedure db_ClearArr(aDatasetArr: array of TDataset);
var
  I: Integer;
begin
  for I := 0 to High(aDatasetArr) do
    db_Clear (aDatasetArr[i]);
end;

// ---------------------------------------------------------------
function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:  string): boolean;
// ---------------------------------------------------------------
begin                  
  assert (aConnectionString<>'', 'function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:  string): boolean;');

  
  with aADOConnection do
  begin
    LoginPrompt:=False;

    Close;
    ConnectionString:=aConnectionString;
    try
      Open;
    except
    //'[DBNETLIB][ConnectionOpen (Connect()).]SQL Server не существует, или доступ запрещен'
      on E: Exception do
          if Pos('ConnectionWrite',E.Message)>0 then
          begin
    //          Memo1.Lines.Add( '[DBNETLIB][ConnectionWrite (send()).]');

              raise TException_DBNETLIB_ConnectionWrite.Create(E.Message);
          end;


    end;

    Result := Connected;
  end;
end;



//--------------------------------------------------------------------
procedure db_SetFieldsValues(aFromDataset, aToDataset: TDataset);
//--------------------------------------------------------------------
var i: integer;   //sName: string;
begin
  for i := 0 to aFromDataset.FieldCount - 1 do
  try
    db_SetFieldValue(aToDataset,
       aFromDataset.Fields[i].FieldName, aFromDataset.Fields[i].Value);

(*    sName:=aFromDataset.Fields[i].FieldName;
    if not Assigned(aToDataset.Fields.FindField(sName)) then
      Continue;

    aToDataset.FieldByName(sName).Value := aFromDataset.Fields[i].Value;
*)
  except end;
end;



 //-------------------------------------------------------------------
 procedure db_SetComponentADOConnSingle (aComponent: TComponent; aADOConnection: TADOConnection);
 //-------------------------------------------------------------------
 var
   s: string;
 begin
    if not Assigned(aADOConnection) then
      raise Exception.Create('ADOConnection=nil');


    s:=aComponent.ClassName;

    
    if (aComponent is TCustomADODataSet) then
    begin
      TCustomADODataSet(aComponent).Close;
      TCustomADODataSet(aComponent).Connection:=aADOConnection;
    end else

    
    if (aComponent is TADOQuery) then
    begin
      TADOQuery(aComponent).Close;
      TADOQuery(aComponent).Connection:=aADOConnection;
    end else

    if (aComponent is  TADOTable) then begin
      TADOTable(aComponent).Close;
      TADOTable(aComponent).Connection:=aADOConnection;
    end else

    if (aComponent is TADODataset) then
      TADODataset(aComponent).Connection:=aADOConnection
    else


    if (aComponent is TADOCommand) then
      TADOCommand(aComponent).Connection:=aADOConnection
    else

    if (aComponent is TADOStoredProc) then
      TADOStoredProc(aComponent).Connection:=aADOConnection

    else;


end;




//----------------------------------------------------------------------------
procedure db_ClearFields (aDataset: TDataset);
//----------------------------------------------------------------------------
var i: integer;
//s: string;
begin
  with aDataset do begin
    Close;
//    Fields.Clear;
    for i:=Fields.Count-1 downto 0 do
      if Fields[i].FieldName<>'RecId' then
        Fields[i].Free;
//      aDataset.FieldValues[aParams[i].FieldName]:=aParams[i].FieldValue;

    //Open;
  end;
end;

//
//// ---------------------------------------------------------------
//function db_Par(aFieldName: string; aFieldValue: Variant): TDBParamRec;
//// ---------------------------------------------------------------
//begin
//  Result.FieldName :=aFieldName;
//  Result.FieldValue:=aFieldValue;
//
//  case VarType(aFieldValue) of
//    varBoolean: if aFieldValue=True then Result.FieldValue:=1
//                                    else Result.FieldValue:=0;
//  end;
//end;


 {
//----------------------------------------------------------------------------
function db_AddRecord(aDataset: TDataset; aParams: array of TDBParamRec):
    Boolean;
//----------------------------------------------------------------------------
var i: integer;
begin
  Result := True;

  aDataset.Open;

  aDataset.Append;

  for i:=0 to High(aParams)  do
  try
    db_SetFieldValue (aDataset, aParams[i].FieldName, aParams[i].FieldValue);
  //  if Assigned(aDataset.FindField(aParams[i].FieldName)) then
   //   aDataset.FieldValues[aParams[i].FieldName]:=aParams[i].FieldValue;
  except
   //

    on E: Exception do
    begin
      Result := False;
      //g_Log.AddException ('db','db_AddRecord',E.Message);

      Exit;
    end;

  end;

  aDataset.Post;
end;
  }


//----------------------------------------------------------------------------
function db_AddRecord_(aDataset: TDataset; aParams: array of Variant): Boolean;
//----------------------------------------------------------------------------
var i: integer;
begin
  aDataset.Open;

 // Assert(aDataset.Active, 'aDataset.Active');

  Result := True;

 // aDataset.Open;
  aDataset.Append;

  for i:=0 to (Length(aParams) div 2)-1  do
  try
    db_SetFieldValue (aDataset, aParams[i*2], aParams[i*2+1]);
  //  if Assigned(aDataset.FindField(aParams[i].FieldName)) then
   //   aDataset.FieldValues[aParams[i].FieldName]:=aParams[i].FieldValue;
  except
   //

    on E: Exception do
    begin
      Result := False;
      //g_Log.AddException ('db','db_AddRecord_',E.Message);

      Exit;
    end;

  end;

  aDataset.Post;
end;



//----------------------------------------------------------------------------
procedure db_SetFieldValue(aDataset: TDataset; aFieldName : string; aFieldValue : Variant);
//----------------------------------------------------------------------------
begin
  Assert(Assigned(aDataset), 'aDataset not assigned');

  if Assigned(aDataset.FindField(aFieldName)) then
    try
      aDataset.FieldValues[aFieldName]:=aFieldValue;
    except
    end
  else begin
    //CodeSite.Send( 'Assigned(aDataset.FindField(aFieldName))-'+ aFieldName);
  
    //g_Log.Error ('db_SetFieldValue: not Assigned aDataset.FindField(aFieldName); - '+ aFieldName);

  end;

  //  g_Log.Error ('db_SetFieldValue:  aDataset.FindField(aFieldName); - '+ aFieldName);

end;



//----------------------------------------------------------------------------
procedure db_SetFieldCaption(aDataset: TDataset; aFieldName : string; aCaption : string);
//----------------------------------------------------------------------------
var
  oField: TField;
begin
  Assert(Assigned(aDataset), 'aDataset not assigned');

  oField :=aDataset.FindField(aFieldName);
  if Assigned(oField) then
    oField.DisplayLabel :=aCaption
  else
    //g_Log.Error ('db_SetFieldCaption:  aDataset.FindField(aFieldName);'+ aFieldName+ '  :  ' + aDataset.Name);
end;



//----------------------------------------------------------------------------
procedure db_SetFieldCaptions(aDataset: TDataset; aFieldNames : array of
    Variant);
//----------------------------------------------------------------------------
var I: Integer;
  sFieldName: string;
  sValue: string;
begin
  Assert (Assigned(aDataset), 'aDataset not assigned');
  Assert (Length(aFieldNames) mod 2 = 0,  IntToStr(Length(aFieldNames)));

  
  for I := 0 to (Length(aFieldNames) div 2) -1 do
  begin
//    Assert(Length(aFieldNames[i])=2);

    sFieldName:=aFieldNames[i*2];
    sValue    :=aFieldNames[i*2+1];


    db_SetFieldCaption (aDataset, sFieldName, sValue);
  end;
end;



(*
procedure Test;
begin
  db_SetFieldCaptions111111111 (nil, [('11','11'),['22','33']]);

end;    //

*)


//----------------------------------------------------------------------------
procedure db_AddRecord1111(aDataset: TDataset;
                       aFieldNames: array of String;
                       aFieldValues: array of Variant);
//----------------------------------------------------------------------------
var i: integer;
begin
  aDataset.Open;

  aDataset.Append;
  for i:=0 to High(aFieldNames)  do
  try
    db_SetFieldValue (aDataset, aFieldNames[i], aFieldValues[i]);
  //  if Assigned(aDataset.FindField(aFieldNames[i])) then
   //   aDataset.FieldValues[aFieldNames[i]]:=aFieldValues[i];
  except
    on E: Exception do
      //g_Log.AddException ('db','db_AddRecord',E.Message);
  end;

  aDataset.Post;
end;


procedure db_ClearReadOnly(aDataSet: TDataSet);
var
  I: integer;
begin
  for I := 0 to aDataSet.FieldCount - 1 do
    if aDataSet.Fields[i].ReadOnly then
      aDataSet.Fields[i].ReadOnly := False;
end;


procedure db_View(aDataSet: TDataSet; aCaption: string = '');
begin
  Tdlg_db_ViewDataset.ExecDlg (aDataSet, aCaption);
end;

//--------------------------------------------------------------------
function db_CreateField(aDataSet: TDataset; aFieldName: string; aType:
    TFieldType; aSize: integer=200): TField;
//--------------------------------------------------------------------
var
  bActive: Boolean;
  oField: TField;
begin
  Assert(Assigned(aDataset));


  Result := nil;

//  if Assigned(aDataset) then
//  begin
    Result := aDataset.Fields.FindField (aFieldName);

    if Assigned(Result) then
      Exit;

 ///   bActive:=aDataSet.Active;
///    if bActive then
      aDataSet.Close;
//  end;


//    TAnStringField

  case aType of
    ftWideString:begin
                  oField:=TWideStringField.Create(aDataset);
                  oField.Size:=aSize;
                 end;

    ftString:    begin
                  oField:=TStringField.Create(aDataset);
                  oField.Size:=aSize;
                 end;
  //  :  oField:=TIntegerField.Create(aDataset); //äîáàâëåíî 22-08-06
    ftGuid   :  oField:=TStringField .Create(aDataset); //äîáàâëåíî 22-08-06

    ftAutoInc,
    ftSmallInt,
    ftInteger:  oField:=TIntegerField.Create(aDataset);

    ftBoolean:  oField:=TBooleanField.Create(aDataset);
    ftDateTime: begin
                  oField:=TDateTimeField.Create(aDataset);
                  TDateTimeField(oField).DisplayFormat:='dd.mm.yyyy hh:mm';
                end;
    ftDate:     begin
                  oField:=TDateField.Create(aDataset);
                  TDateField(oField).DisplayFormat:='dd.mm.yyyy';
                end;

    ftFloat:    oField:=TFloatField.Create(aDataset);
    ftMemo:     oField:=TMemoField.Create(aDataset);

    ftGraphic,
    ftBlob:     oField:=TBlobField.Create(aDataset);
  else
    raise Exception.Create('db_CreateField - field type is not defined');
    //Exit;                           //çàêîììåíòàðåíî 23-08-06
    oField:=TStringField.Create(aDataset); //äîáàâëåíî 23-08-06
    oField.Size:=aSize;               //äîáàâëåíî 23-08-06
  end;

  oField.FieldName := aFieldName;

  try
    oField.DataSet:=aDataset;
  except end;

//  if Assigned(aDataSet) then
 //   if bActive then
  //    aDataSet.Open;

  Result:=oField;
end;




//--------------------------------------------------------------------
procedure db_CreateFieldsForDataset(aDataSet: TDataSet);
//--------------------------------------------------------------------
var
  I: Integer;
  bActive: Boolean;
  oField: TField;

  ft: TFieldType;

 // oFieldDef: TFieldDef;
begin
  Assert(Assigned(aDataset));

//  bActive:=aDataSet.Active;
//  if bActive then

  aDataSet.Close;

  for I := 0 to aDataSet.FieldDefList.Count - 1 do
  begin
    oField := aDataset.Fields.FindField (aDataSet.FieldDefList[i].Name);

    ft:=aDataSet.FieldDefList[i].DataType;


//    if Assigned(oField) then
//      Continue;

    if not Assigned(oField) then
      aDataSet.FieldDefList[i].CreateField(aDataSet);
               
  end;

 // if bActive then
 //   aDataSet.Open;

end;



//--------------------------------------------------------------------
procedure db_PostDatasets(aComponent: TComponent);
//--------------------------------------------------------------------
var
  i: integer;
begin
  for i:=0 to aComponent.ComponentCount-1 do
    if aComponent.Components[i] is TDataSet then
      db_PostDataset (aComponent.Components[i] as TDataSet);

 // if aDataset.State in [dsInsert,dsEdit] then
 //   aDataset.Post;
end;


//--------------------------------------------------------------------
procedure db_PostDataset (aDataset: TDataset);
//--------------------------------------------------------------------
begin
  if aDataset.State in [dsInsert,dsEdit] then
    aDataset.Post;
end;

procedure db_DatasetEdit (aDataset: TDataset);
begin
  if not (aDataSet.State in [dsEdit,dsInsert]) then
    aDataSet.Edit;
end;

function db_IsEditState (aDataset: TDataset): boolean;
begin
  Result:=(aDataset.State in [dsInsert,dsEdit]);
end;


function db_OpenQuery_ParamList  (aQuery    : TADOQuery;
                             aSQL      : string;
                             aParamList: TdbParamList): boolean;
begin
  Assert(assigned(aParamList));

  Result := db_OpenQuery(aQuery, aSQL, aParamList.MakeArr);

end;


//--------------------------------------------------------------------
function db_OpenQuery_111111111     (aQuery    : TADOQuery;
                             aSQL      : string;
                             aParams   : array of Variant): boolean;
//--------------------------------------------------------------------
  function DoGetValue(aFieldName : string): Variant;
  var
    i: integer;
    sName: string;
    vValue: Variant;
  begin
    result := Null;

    for i:=0 to (Length(aParams) div 2)-1 do
    begin
      sName :=aParams[i*2];
      vValue:=aParams[i*2+1];

      if Eq(aFieldName, sName) then
      begin
        result := vValue;
        exit;
      end;
    end;

    raise Exception.Create(' function DoGetValue(aFieldName : string): Variant;');
  end;



var
  i,j: integer;
  iVarType: TVarType;
  //  oParam: TParameter;

  oParameter: TParameter;
  s: string;
  v: Variant;

begin

  Assert (aQuery.Connection<>nil, 'db_OpenQuery:aQuery.Connection=nil');
  Assert (aQuery.Connection.Connected, 'db_OpenQuery:aQuery.Connection.Connected');
//  Assert (aQuery.Connection.Active, 'aQuery.Connection.Active');

  Result:= false;


  aQuery.Close;

  if aQuery.Parameters.Count>0 then
  begin
    aQuery.ParamCheck:=True;

  end;

////////  aQuery.AutoCalcFields:=False;
  aQuery.SQL.Text:=aSQL;

  i:=aQuery.Parameters.Count;

  for i:=0 to aQuery.Parameters.Count-1 do
  begin
    oParameter:=aQuery.Parameters[i];

    v:=DoGetValue (oParameter.Name);

 //   s:=VarToStr(v);
 //   Assert(s<>'-');


    try
      oParameter.Value := v;

    except // wrap up
      raise Exception.Create('');
    end;    // try/finally




  end;



  try
    //g_Log.SysMsg('u_db.db_OpenQuery()-'+ aSQL);


//    aQuery.ExecuteOptions := aQuery.ExecuteOptions + [eoExecuteNoRecords];

    aQuery.Open;

    Result:= true;
  except
    on E: Exception do begin
    //  gl_DB.ErrorMsg:= E.Message;
      s:= aSQL+CRLF+' Ïàðàìåòðû: ';
      for i:=0 to (Length(aParams) div 2)-1 do
        s:= s+ aParams[2*j]+' - '+ VarToStr(aParams[2*j+1])+' ; ';

      //g_Log.AddExceptionWithSQL('u_db.db_OpenQuery(third)', E.Message, s);
//      gl_Log.AddExceptionWithSQL('u_db.db_OpenQuery(third)', E.Message, s);
    end;
  end;

//  if not aQuery.IsEmpty then
//  i:=aQuery.RecordCount;

end;



//--------------------------------------------------------------------
function db_OpenQuery(aQuery : TADOQuery; aSQL : string; aParams : array of  Variant): boolean;
//--------------------------------------------------------------------

  function DoGetValue(aFieldName : string): Variant;
  var
    i: integer;
  begin
    result := Null;

    for i:=0 to (length(aParams) div 2)-1 do
//    for j:=0 to High(aParams) do
      if Eq(aFieldName, aParams[2*i]) then
      begin
        result := aParams[2*i+1];
        exit;
      end;

    raise Exception.Create(' function DoGetValue(aFieldName : string): Variant;');
  end;



var
  i,j: integer;
  iVarType: TVarType;
  //  oParam: TParameter;

  oParameter: TParameter;
  s: string;
  v: Variant;

begin

  Assert (aQuery.Connection<>nil, 'db_OpenQuery:aQuery.Connection=nil');
  Assert (aQuery.Connection.Connected, 'db_OpenQuery:aQuery.Connection.Connected');
//  Assert (aQuery.Connection.Active, 'aQuery.Connection.Active');

  Result:= false;



  aQuery.Close;

//  if aQuery.Parameters.Count
  aQuery.ParamCheck:=True;
////////  aQuery.AutoCalcFields:=False;
  aQuery.SQL.Text:=aSQL;

  i:=aQuery.Parameters.Count;


  for i:=0 to aQuery.Parameters.Count-1 do
  begin
    oParameter:=aQuery.Parameters[i];

    v:=DoGetValue (oParameter.Name);

 //   s:=VarToStr(v);
 //   Assert(s<>'-');


    try
      oParameter.Value := v;

    except // wrap up
      raise Exception.Create('');
    end;    // try/finally

  end;


  try
    //g_Log.SysMsg('u_db.db_OpenQuery()-'+ aSQL);


//    aQuery.ExecuteOptions := aQuery.ExecuteOptions + [eoExecuteNoRecords];

    aQuery.Open;

    Result:= true;

  except
    on E: Exception do begin
    //  gl_DB.ErrorMsg:= E.Message;
      s:= aSQL+CRLF+' Ïàðàìåòðû: ';

      for i:=0 to (length(aParams) div 2)-1 do
        s:= s+ aParams[2*j]+' - '+VarToStr(aParams[2*j+1])+' ; ';

      //g_Log.AddExceptionWithSQL('u_db.db_OpenQuery(third)', E.Message, s);
//      gl_Log.AddExceptionWithSQL('u_db.db_OpenQuery(third)', E.Message, s);
    end;
  end;

//  if not aQuery.IsEmpty then
//  i:=aQuery.RecordCount;

end;



procedure db_CreateFieldArr (aDataSet: TDataset; aFieldArr: array of TdbFieldRec); 
var i: integer;
begin
  aDataSet.Close;
////////  aDataSet.Fields.Clear;

  for i:=0 to High(aFieldArr) do
    db_CreateField (aDataSet, aFieldArr[i].Name, aFieldArr[i].Type_, aFieldArr[i].Size);

//  aDataSet.Open;
end;

{
//-------------------------------------------------------------------
procedure db_GetCheckedList1(aDataset: TDataset; aIDList: TIDList);
//-------------------------------------------------------------------
// FLD_CHECKED
// FLD_ID
var
  iID: Integer;
begin
  Assert(Assigned(aDataset.FieldByName(FLD_CHECKED)));
  Assert(Assigned(aDataset.FieldByName(FLD_ID)));


  with aDataset do
  begin
  //  iRecNoToRemember:= RecNo;

    DisableControls;
    First;

    while not eof do
    begin
      if (FieldByName(FLD_CHECKED).AsBoolean)  then
      begin
        iID := FieldByName(FLD_ID).AsInteger;
        aIDList.AddID(iID);
      end;

      Next;
    end;

    EnableControls;
    First;
  end;
end;

 }

//--------------------------------------------------------
function IIF(aCondition: boolean; TrueValue, FalseValue: variant): variant;
//--------------------------------------------------------
begin
  if aCondition then Result:=TrueValue else Result:=FalseValue;
end;


// -------------------------------------------------------------------
function db_TypeFieldToString11(aField: TFieldType; aSize: Integer): String;
// -------------------------------------------------------------------
begin
  case aField of
    ftUnknown :	Result:='';

    ftString  : if aSize>255 then
                 Result:='text'
                else
                 Result:='varchar('+AsString(aSize)+')';

    ftSmallint:	Result:='int null';
    ftInteger :	Result:='int null';
    ftWord	  :	Result:='int null';
    ftBoolean : Result:='logical';
    ftFloat	  : Result:='float null';
    ftCurrency: Result:='money null';
    ftDate	  : Result:='date';
    ftTime	  : Result:='date';
    ftDateTime: Result:='date';
    ftMemo    : Result:='memo';
    ftBlob    : Result:='image';
//    ftBlob    : Result:='varbinary';

    //ftAutoInc	: Result:='int IDENTITY PRIMARY KEY';
//    ftAutoInc:  Result:='int PRIMARY KEY';
    ftAutoInc:  Result:='int';
    ftWideString :Result:='varchar('+AsString(aSize)+')';
    ftLargeInt	 :Result:='int null';
//    ftGuid	     :Result:='varchar(40)';
    ftGuid	     :Result:='GUID';
  else
    raise Exception.Create('');
//    Result:='';
  end;
end;

  {
function db_MakeParamRecArray(aParams: array of TDBParamRec): TdbParamRecArray;
var
  I: Integer;
begin
  SetLength(Result, Length(aParams) );

  for I := 0 to High(aParams) do
    Result[i]:=aParams[i];

end;
   }

// ---------------------------------------------------------------
function db_ForEach(aDataset: TDataset; aProc: TForEachProc): boolean;
// ---------------------------------------------------------------
var
  bm: TBookmark;
  bTerminated : Boolean;
begin
  Assert(Assigned(aDataset), 'Dataset not assigned');

  Result:=False;

 // Screen.Cursor := crHourGlass;


  if (not aDataset.Active) or (aDataset.RecordCount=0) then
    Exit;

  bm:=aDataset.GetBookmark;
  bTerminated := False;

  aDataset.DisableControls;
  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      aProc(aDataset, bTerminated);
      if bTerminated then
        Break;

      Next;
    end;

  Result := not bTerminated;

  aDataset.GotoBookmark(bm);

{  if oDataSet.Bo BookmarkValid(bm) then
     oDataSet.GotoBookmark:=bm;

}
  aDataset.EnableControls;
 // Screen.Cursor := crDefault;

end;

// ---------------------------------------------------------------
procedure db_TableDeleteRecords(aADOConnection: TADOConnection; aTableName:
    string);
// ---------------------------------------------------------------
begin
  Assert(Assigned(aADOConnection), 'aADOConnection not assigned');

  aADOConnection.Execute('DELETE FROM ' + aTableName);
end;



//-------------------------------------------------------------------
function db_IsTableExists(aADOConnection: TADOConnection; aTableName: string):
    Boolean;
//-------------------------------------------------------------------
var
  k: Integer;
  oSList: TStringList;
begin
  Assert(Assigned(aADOConnection), 'aADOConnection not assigned');
  Assert(aTableName<>'','aTableName<>''''''');

  oSList:=TStringList.Create;

  aADOConnection.GetTableNames (oSList, False);

  k:=oSList.Count;

  Result := oSList.IndexOf(aTableName)>=0;

  FreeAndNil( oSList);
end;

//-------------------------------------------------------------------
function db_TableReOpen(aADOTable: TADOTable; aTableName: string = ''): Boolean;
//-------------------------------------------------------------------
begin
  aADOTable.Close;
  Result := db_TableOpen1(aADOTable, aTableName);
end;







//-------------------------------------------------------------------
function db_TableOpen1(aADOTable: TADOTable; aTableName: string = ''): Boolean;
//-------------------------------------------------------------------
//var //oConn: TADOConnection;
//  oSList: TStringList;
var
  s: string;
begin
  Assert(Assigned(aADOTable), 'aADOTable not assigned');
  Assert(Assigned(aADOTable.Connection), 'aADOTable.Connection not assigned');

  aADOTable.Close;
  
//  if not Eq(aADOTable.TableName,aTableName) then
//    aADOTable.Close;

//  if aADOTable.Active then
//    Exit;

  Assert(aTableName<>'','aTableName<>''''''');

  Result := False;

 // CursorSQL;
//  aADOTable.Close;

//  if aTableName<>'' then
  aADOTable.TableName:=aTableName;

//  if not db_IsTableExists (aADOTable.Connection, aADOTable.TableName) then
//  begin
//    s := Format('Table not exists: %s; Connection: %s',
//      [ aADOTable.TableName, aADOTable.Connection.ConnectionString]);
//
//    ShowMessage(s);
//    //G_Log.Error (s);
//
//  //  CursorDefault;
//    Exit;
//  end;


 try
   aADOTable.Open;

   result := True;
 except 
   result := False;

 end;
 // CursorDefault;

end;


//-------------------------------------------------------------------
procedure db_SetTableName(aADOTable: TADOTable; aTableName: string);
//-------------------------------------------------------------------
//var //oConn: TADOConnection;
//  oSList: TStringList;
begin
  Assert(Assigned(aADOTable), 'aADOTable not assigned');
  Assert(Assigned(aADOTable.Connection), 'aADOTable.Connection not assigned');
  Assert(aTableName<>'','aTableName<>''''''');

  if aADOTable.Active and Eq(aADOTable.TableName, aTableName) then
    Exit;

//  Assert(aTableName<>'','aTableName<>''''''');


  if not db_IsTableExists (aADOTable.Connection, aADOTable.TableName) then
  begin
    ShowMessage ('Table not exists: '+ aADOTable.TableName);
    //G_Log.Error ('not TableExists: '+ aADOTable.TableName);
    Exit;
  end;

  aADOTable.Close;
  aADOTable.TableName := aTableName;

//  aADOTable.Open;

 // result := True;

end;



function db_MakeWhereSQL(aStrList: TStringList): string;
var
  I: Integer;
begin
  Result := '';

  if aStrList.Count>0 then
    for I := 0 to aStrList.Count - 1 do
    begin
      result := result+ '('+aStrList[i]+')';
      if i< aStrList.Count-1 then
        result := result+ ' and ';
    end;
end;



// ---------------------------------------------------------------
function db_OpenStoredProc_ADOConnection(aADOConnection: TADOConnection; aProcName:   string; aParams: array of Variant): Integer;
// ---------------------------------------------------------------
var oProc: TAdoStoredProc;
begin
  oProc:=TAdoStoredProc.Create(nil);
  oProc.Connection:= aADOConnection;

  Result := db_OpenStoredProc(oProc, aProcName, aParams);

  FreeAndNil( oProc);
end;

//------------------------------------------------------------------------------
function db_OpenStoredProc(aProc: TAdoStoredProc; aProcName: string; aParams: Array of  Variant; aIsOpenDataset: boolean = true): Integer;
//------------------------------------------------------------------------------
var
  s: string;
  i: Integer;
  oParameter: TParameter;
begin
//  Result:= false;

  try
    aProc.Close;
    aProc.ProcedureName:= aProcName;
    i := aProc.Parameters.Count;

    aProc.Parameters.Refresh;

    i := aProc.Parameters.Count;

    for i:=0 to (Length(aParams) div 2)-1 do
    begin

      oParameter:=aProc.Parameters.FindParam('@'+aParams[i*2]);

//      oParameter:=oProc.Parameters.ParamByName('@'+aParams[i].FieldName);
      if Assigned(oParameter) then
        oParameter.Value:= aParams[i*2+1]
      else
        //g_Log.Error ('db_ExecStoredProc - field not found:'+aParams[i].FieldName);


    //  aProc.Parameters.ParamByName(aParams[i].FieldName).Value:= aParams[i].FieldValue;
    end;

    if aIsOpenDataset then
      aProc.Open
    else
      aProc.ExecProc;

    i :=aProc.RecordCount;

//    Result:= true;

    oParameter:=aProc.Parameters.FindParam('@RETURN_VALUE');
    if Assigned(oParameter) then
      Result := oParameter.Value
    else
      Result := 0;


  except
    on E: Exception do
    begin
      if Pos('ConnectionWrite',E.Message)>0 then
      begin
//          Memo1.Lines.Add( '[DBNETLIB][ConnectionWrite (send()).]');

          raise TException_DBNETLIB_ConnectionWrite.Create(E.Message);
      end;



      if Pos('result set', E.Message)=0 then begin
      //  gl_DB.ErrorMsg:= E.Message;
        s:= aProcName+CRLF+' Ïàðàìåòðû: ';
        for i:=0 to (Length(aParams) div 2)-1 do
          s:= s + aParams[2*i]+' - '+VarToStr(aParams[2*i+1]) + ' ; ';

        //g_Log.Error (s);

       // gl_ErrorLog.AddExceptionWithSQL('u_func_db.db_ExecStoredProc', E.Message, s);
      end;
    end;
  end;
end;

{

// ---------------------------------------------------------------
function db_ExecStoredProc(aADOConnection: TADOConnection; aProcName: string; aParams:
    array of TdbParamRec): Integer;
// ---------------------------------------------------------------
var

  oProc: TAdoStoredProc;
  oParameter: TParameter;
begin
  oProc:=TAdoStoredProc.Create(nil);
  oProc.Connection:= aADOConnection;

  Result := db_ExecStoredProc__222222(oProc, aProcName, aParams);

  FreeAndNil( oProc);
end;

  }

  {
// ---------------------------------------------------------------
function db_ExecStoredProc_(aADOConnection: TADOConnection; aProcName: string;
    aParams: array of Variant): Integer;
// ---------------------------------------------------------------
var

  oProc: TAdoStoredProc;
  oParameter: TParameter;
begin
  oProc:=TAdoStoredProc.Create(nil);
  oProc.Connection:= aADOConnection;

  Result := db_ExecStoredProc__new111111(oProc, aProcName, aParams, [] );

  FreeAndNil( oProc);
end;
  }




// ---------------------------------------------------------------
function db_ExecStoredProc_v1(aAdoStoredProc: TAdoStoredProc; aProcName:
    string; aParams: array of Variant; aOpenMode: boolean = false): Integer;
// ---------------------------------------------------------------
var
  oAddStringsList: TStringList;
  s, sAddString: string;
  oParam: TParameter;
  i: Integer;

  oParameter: TParameter;
//  bEqual: boolean;
  k: Integer;
 // oStrStream: TStringStream;
  sFieldName: string;
  v: Variant;
  vt: Tvartype;
  vValue: Variant;

begin
  Assert(Assigned(aAdoStoredProc.Connection), 'aAdoStoredProc.Connection not assigned');


  try
   aAdoStoredProc.Close;
   aAdoStoredProc.ProcedureName:= aProcName;

//
//   aAdoStoredProc.Parameters.Refresh;
//   for I := 0 to aAdoStoredProc.Parameters.Count-1 do
//     oParameter:=aAdoStoredProc.Parameters[i];
//

//     oParameter.


   aAdoStoredProc.Parameters.Clear;

   oParameter:=aAdoStoredProc.Parameters.AddParameter;
   oParameter.Name:='@RETURN_VALUE';
   oParameter.DataType:=ftInteger;
   oParameter.Direction:=pdReturnValue;



//       oParameter:=aAdoStoredProc.Parameters.FindParam('@RETURN_VALUE');

    {
 //   aAdoStoredProc.Prepared:= true;
    i := aAdoStoredProc.Parameters.Count;

    bEqual:=aAdoStoredProc.ProcedureName = aProcName;
    aAdoStoredProc.ProcedureName:= aProcName;

    i := aAdoStoredProc.Parameters.Count;

    if (not bEqual) or (aAdoStoredProc.Parameters.Count=0) then
      aAdoStoredProc.Parameters.Refresh;


    i := aAdoStoredProc.Parameters.Count;
  }

   s := '';
    {
   for i:=0 to aAdoStoredProc.Parameters.Count-1 do
    begin
      oParameter:=aAdoStoredProc.Parameters[i];
      s := s+ '--'+ aAdoStoredProc.Parameters[i].Name;
    end;


    Assert (length(aParams) mod 2 = 0);
    }
//   if assigned(aOutParamList) then
//      for I := 0 to aOutParamList.Count - 1 do    // Iterate
//      begin
//        s:='@'+aOutParamList[i].FieldName;
//
//        oParameter:=aAdoStoredProc.Parameters.FindParam(s);
//        oParameter.Direction:=pdOutput;
//
//        //  TParameterDirection = (pdUnknown, pdInput, pdOutput, pdInputOutput,  pdReturnValue);
//
//      end;    // for
//


    for i:=0 to (length(aParams) div 2)-1 do
    begin
      sFieldName:=aParams[i*2];
      vValue    :=aParams[i*2+1];

      oParameter:=aAdoStoredProc.Parameters.AddParameter;
      oParameter.Name:='@' + sFieldName;
      oParameter.Direction:=pdInput;

      vt:=VarType(vValue);

      S:=VarTypeAsText(vt);

      case VarType(vValue) of
        varDate:    oParameter.DataType:=ftDateTime;
        varDouble:  oParameter.DataType:=ftFloat;

        varBoolean: oParameter.DataType:=ftBoolean;

        varWord,
        varInt64,
        varInteger: oParameter.DataType:=ftInteger;

        varString,
        varUString: oParameter.DataType:=ftString;
      else
        raise Exception.Create(VarTypeAsText(VarType(vValue)));
//        Assert(1<>1);
      end;

     // oParameter:=aAdoStoredProc.Parameters.FindParam('@'+ sFieldName);
      {
//      oParameter:=oProc.Parameters.ParamByName('@'+aParams[i].FieldName);
      if Assigned(oParameter) then
      begin
        if oParameter.DataType = ftString then
        begin
          oStrStream:=TStringStream.Create;
          oStrStream.Clear;
          oStrStream.WriteString(vValue);

//          oParameter.LoadFromStream(oStrStream, ftString);
          oParameter.LoadFromStream(oStrStream, ftMemo);

          v:=oParameter.Value;

//          oParameter.Value:= vValue;

          FreeAndNil(oStrStream);

        end else
        }
          oParameter.Value:= vValue;

    //  end
     // else
        //g_Log.Error ('db_ExecStoredProc - field not found:'+aProcName+'-' + sFieldName);
    end;

 //////////   LogSys('db_ExecStoredProc: '+ aProcName);


   if aOpenMode then
//   if aIsOpenQuery then
     aAdoStoredProc.Open
   else
     aAdoStoredProc.ExecProc;



    oParameter:=aAdoStoredProc.Parameters.FindParam('@RETURN_VALUE');
    Assert(Assigned(oParameter));

//    if Assigned(oParameter) then
      Result := oParameter.Value
//    else
  //    Result := -9999;


//    if assigned(aOutParamList) then
//      for I := 0 to aOutParamList.Count - 1 do    // Iterate
//      begin
//        s:='@'+aOutParamList[i].FieldName;
//
//        oParameter:=aAdoStoredProc.Parameters.FindParam(s);
//
//        if Assigned(oParameter) then
//          aOutParamList[i].FieldValue := oParameter.Value;
//      //  else
//       //   aOutParamList[i].FieldValue :=null;
//
//
//      end;    // for
//




  except
    on E: Exception do
    begin
      s:= aProcName+CRLF+' Ïàðàìåòðû: ';

      for i:=0 to  ( Length(aParams) div 2 ) - 1 do
      begin
        sFieldName:=aParams[i*2];
        vValue    :=aParams[i*2+1];

        s:= s+ sFieldName+' - '+AsString(vValue)+' ; ';
      end;

      //g_Log.AddExceptionWithSQL('u_func_db.db_ExecStoredProc', E.Message, s);
    end;
  end;

end;



function db_StoredProc_Open(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;
begin
  Result:=db_ExecStoredProc (aAdoStoredProc, aProcName, aParams, true);
end;


function db_StoredProc_Exec(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant): Integer;
begin
  Result:=db_ExecStoredProc (aAdoStoredProc, aProcName, aParams, False);

end;



// ---------------------------------------------------------------
function db_ExecStoredProc(aAdoStoredProc: TAdoStoredProc; aProcName: string;
    aParams: array of Variant; aIsOpenQuery: boolean = false): Integer;
// ---------------------------------------------------------------
var
  b: Boolean;
//  oAddStringsList: TStringList;
  s, sAddString: string;
  oParam: TParameter;
  i: Integer;

  oParameter: TParameter;
  bEqual: boolean;
  k: Integer;
  sFieldName: string;
  sFile: string;
  vValue: Variant;

begin
//TLog.Start ('db_ExecStoredProc - '+ aProcName);

  Assert(Assigned(aAdoStoredProc.Connection), 'aAdoStoredProc.Connection not assigned');

//  Screen.Cursor:=crHourGlass;



  try
   aAdoStoredProc.Close;


 //   aAdoStoredProc.Prepared:= true;
    i := aAdoStoredProc.Parameters.Count;

    bEqual:=aAdoStoredProc.ProcedureName = aProcName;
    aAdoStoredProc.ProcedureName:= aProcName;

    i := aAdoStoredProc.Parameters.Count;

    if (not bEqual) or (aAdoStoredProc.Parameters.Count=0) then
      aAdoStoredProc.Parameters.Refresh;

 //   aAdoStoredProc.ExecProc;


    i := aAdoStoredProc.Parameters.Count;


   s := '';

   for i:=0 to aAdoStoredProc.Parameters.Count-1 do
    begin
      oParameter:=aAdoStoredProc.Parameters[i];
      s := s+ '--'+ aAdoStoredProc.Parameters[i].Name;
    end;


    for i:=0 to (Length(aParams) div 2) -1 do
    begin

      sFieldName:=aParams[i*2];
      vValue    :=aParams[i*2+1];

      oParameter:=aAdoStoredProc.Parameters.FindParam('@'+sFieldName);

      Assert (Assigned(oParameter), sFieldName);

      if Assigned(oParameter) then
      begin
        k:=oParameter.Size;

        {
        if ((oParameter.DataType=ftString) and (oParameter.Size>100000))
          or
           (oParameter.DataType=ftVarBytes)
         }

        if (oParameter.DataType=ftVarBytes) then
        begin
          if FileExists(vValue) then
             oParameter.LoadFromFile(vValue, ftVarBytes);

        end
        else
          oParameter.Value:= vValue

      end
      else
        //g_Log.Error ('db_ExecStoredProc - field not found:'+aProcName+'-'+aParams[i].FieldName);
    end;


 //////////   LogSys('db_ExecStoredProc: '+ aProcName);


   if aIsOpenQuery then
     aAdoStoredProc.Open
   else
     aAdoStoredProc.ExecProc;
     


    oParameter:=aAdoStoredProc.Parameters.FindParam('@RETURN_VALUE');
    if Assigned(oParameter) then
      Result := oParameter.Value
    else
      Result := -9999;


  except
    on E: Exception do
    begin
      s:=E.ClassName;

      b:=E.ClassName='EOleException';
      b:=E is EOleException;

      if E.ClassName='EOleException' then
      //Pos('ConnectionWrite',E.Message)>0 then
      begin
//          Memo1.Lines.Add( '[DBNETLIB][ConnectionWrite (send()).]');

          raise TException_DBNETLIB_ConnectionWrite.Create(E.Message);
      end;



      s:= aProcName+CRLF+' params: ';

      for i:=0 to (Length(aParams) div 2) -1 do
      begin
        sFieldName:=aParams[i*2];
        vValue    :=aParams[i*2+1];

        s:= s+ sFieldName+' - '+ VarToStr(vValue)+' ; ';

      end;

      codeSite.SendError(s);
      db_LogException('u_db.db_ExecStoredProc', E.Message + s);
    end;
  end;

//  Screen.Cursor:=crDefault;

//TLog.Stop ();

end;


//----------------------------------------------------------------------------
function db_MakeUpdateString(aTableName: string; aID : integer; aParamList:
    TdbParamList): string;
//----------------------------------------------------------------------------
//        aParams   : array of TDBParamRec
var str,sParamStr: string;  i: integer;
  s: string;
begin
  Assert(aID>0, 'Value <=0');

  s:='';

  for i:=0 to aParamList.Count-1 do
  begin
    s:=s + Format('%s=:%s', [aParamList[i].FieldName, aParamList[i].FieldName]);

    if i<aParamList.Count-1 then
      s:=s + ',';
  end;

  { TODO : ËÅØÀ! Ïåðåíåñè ýòè èçìåíåíèÿ ê ñåáå. Î×ÅÍÜ ÂÀÆÍÎ !!! }
//  Assert(aID>0, 'TDBManager.MakeUpdateQuery: aID=0');

//  if aID>0 then
  Result:=Format('UPDATE %s SET %s WHERE id=%d', [aTableName, s, aID]);

//  else
  //  Result:=Format('UPDATE %s SET %s', [aTableName, sParamStr])


end;



//----------------------------------------------------------------------------
function db_MakeInsertQuery_list1(aTableName: string; aParamList: TdbParamList):
    string;
//----------------------------------------------------------------------------
var
  sParamStr,sValueStr: string;
  i: integer;
  bFirst: boolean;
begin
  sParamStr:='';
  sValueStr:='';
  bFirst:=True;

  for i:=0 to aParamList.Count-1 do
    if aParamList[i].FieldValue <> NULL then
  begin
{     if Eq(aParams[i].FieldName,'guid') then
       aParams[i].FieldName:='['+aParams[i].FieldName+']';
}

    if not bFirst then
    begin
      sParamStr:=sParamStr + ',';
      sValueStr:=sValueStr + ',';
    end;
    bFirst:=False;

    sParamStr:=sParamStr + '[' + aParamList[i].FieldName+ ']';
    sValueStr:=sValueStr + ':' + aParamList[i].FieldName;
  end;

  Result:=Format('INSERT INTO %s (%s) VALUES (%s)', [aTableName, sParamStr, sValueStr]);
end;


//
//procedure db_DeleteRecords(aADOConnection: TADOConnection; aTableName: string);
//begin
//  aADOConnection.Execute('DELETE FROM ' + aTableName);
//end;

// ---------------------------------------------------------------
procedure db_DeleteRecordsFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  aDataset.DisableControls;

  while aDataset.RecordCount>0 do
    aDataset.Delete;
  

  aDataset.EnableControls;
  
end;



// ---------------------------------------------------------------
procedure db_CopyRecordsToStringList(aDataset: TDataset; aStringList: TStrings);
// ---------------------------------------------------------------
begin
  aDataset.First;

  with aDataset do
    while not Eof do
    begin
      aStringList.AddObject(FieldByName(FLD_NAME).AsString,
                            Pointer( FieldByName(FLD_ID).AsInteger));
      Next;
    end;
end;



function TdbParamList.AddItem(aFieldName: string; aFieldValue: Variant):
    TdbParamObj;

begin
  Assert(aFieldName<>'');

  
  Result := TdbParamObj.Create;
  

  Result.FieldName :=aFieldName;
  Result.FieldValue :=aFieldValue;

  AddObject(aFieldName, Result);
  
end;

// ---------------------------------------------------------------
procedure TdbParamList.DelBYFieldName(aFieldName : string);
// ---------------------------------------------------------------
var
  k: Integer;
begin  
  k:=IndexOf(aFieldName);
  if k>=0 then
    Delete(k);
end;

// ---------------------------------------------------------------
function TdbParamList.FindBYFieldName(aFieldName : string): TdbParamObj;
// ---------------------------------------------------------------
var
  I: Integer;
  k: Integer;

begin
  k:=IndexOf(aFieldName);


  Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].FieldName, aFieldName) then
    begin
      result := Items[i];
      exit;
    end;
end;

//---------------------------------------------------------------------------
function TdbParamList.GetValueByName(aFieldName : string): Variant;
//---------------------------------------------------------------------------
var
  oParam: TdbParamObj;
begin
  oParam :=FindBYFieldName (aFieldName);

  if Assigned(oParam) then
    Result := oParam.FieldValue
  else
    raise Exception.Create('');
//    Result := null;
end;


procedure TdbParamList.SetValueByName(aFieldName : string; aValue: Variant);
var
  oParam: TdbParamObj;

begin

//  if Eq(aFieldName, 'property_id') then
//    Assert( AsInteger(aValue) > 0 );


  //if Eq(aFieldName, 'link_id') then
 //   Assert( AsInteger(aValue) > 0 );


  oParam :=FindBYFieldName (aFieldName);

  if Assigned(oParam) then
    oParam.FieldValue :=aValue
  else
    AddItem (aFieldName, aValue);

end;



function TdbParamList.GetItems(Index: Integer): TdbParamObj;
begin
  Result := TdbParamObj(Objects[Index]);
end;
  
//
  {
function TdbParamList.LoadFromArr(aParams: array of TDBParamRec): Integer;
var
  I: Integer;
begin
  for I := 0 to High(aParams) do
    AddItem(aParams[i].FieldName, aParams[i].FieldValue);
end;
  }

function TdbParamList.LoadFromArr_variant(aParams: array of Variant): Integer;
var
  I: Integer;
begin
  Assert(length(aParams) mod 2 =0);

  for I := 0 to (length(aParams) div 2)-1  do
  try
    AddItem(aParams[i*2], aParams[i*2+1]);

  except 
    ShowMessage('function TdbParamList.LoadFromArr_variant(aParams: array of Variant): Integer;');
  //on E: Exception do
  end;

end;

// ---------------------------------------------------------------
procedure TdbParamList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Clear;


  for I := 0 to aDataset.Fields.Count-1 do
  begin    
    AddItem(aDataset.Fields[i].FieldName, aDataset.Fields[i].Value );
  end;

end;


// ---------------------------------------------------------------
function TdbParamList.MakeArr: TVariantArray;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  SetLength(Result, 2*Count);

  for I := 0 to Count - 1 do
  begin
    Result[2*i]     := Items[i].FieldName;
    Result[2*i + 1] := Items[i].FieldValue;
  end;

//  Result := ;
  // TODO -cMM: TdbParamList.MakeArr default body inserted
end;


// ---------------------------------------------------------------
function TdbParamList.MakeInsertSQL(aTableName: string; aIsTest: boolean =
    false): string;
// ---------------------------------------------------------------
var
  sParamStr,sValueStr: string;
  i: Integer;
  s: string;
begin
  sParamStr:='';
  sValueStr:='';

  for i:=0 to Count-1 do
     if not VarIsNull (Items[i].FieldValue)  then
  begin
    sParamStr:=sParamStr + Format(',[%s]',[Items[i].FieldName]);

    if aIsTest then
      s:=Format(',%s', [Items[i].GetValueForSQL()])
    else
      s:=Format(',:%s', [Items[i].FieldName]);

    sValueStr:=sValueStr + s;//Format(',:%s', [Items[i].FieldName]);


  end;

  Result:=Format('INSERT INTO %s (%s) VALUES (%s)', [aTableName,
      Copy(sParamStr,2,100000),  Copy(sValueStr,2,100000)]);

end;


//----------------------------------------------------------------------------
function TdbParamList.MakeUpdateString(aTableName: string; aID : integer):
    string;
//----------------------------------------------------------------------------
//        aParams   : array of TDBParamRec
var
  str,sParamStr: string;
  i: integer;
  s: string;
begin
  Assert(aID>0, 'Value <=0');

  s:='';

  for i:=0 to Count-1 do
  begin
    s:=s + Format('%s=:%s', [Items[i].FieldName, Items[i].FieldName]);

    if i<Count-1 then
      s:=s + ',';
  end;

  { TODO : ËÅØÀ! Ïåðåíåñè ýòè èçìåíåíèÿ ê ñåáå. Î×ÅÍÜ ÂÀÆÍÎ !!! }
//  Assert(aID>0, 'TDBManager.MakeUpdateQuery: aID=0');

//  if aID>0 then
  Result:=Format('UPDATE %s SET %s WHERE id=%d', [aTableName, s, aID]);

//  else
  //  Result:=Format('UPDATE %s SET %s', [aTableName, sParamStr])


end;




// ---------------------------------------------------------------
function TdbParamList.MakeSelectSQL(aTableName: string; aFieldName: string =
    ''): string;
// ---------------------------------------------------------------
var
  sWhere: string;
  i: Integer;
begin
  sWhere:='';

  for i:=0 to Count-1 do
    if Items[i].FieldValue=null then
      sWhere:=sWhere + Format(' and ([%s] IS NULL)', [Items[i].FieldName])
    else
      sWhere:=sWhere + Format(' and ([%s]=:%s)', [Items[i].FieldName, Items[i].FieldName]);

  sWhere:=Copy(sWhere,6,1000000);

  if aFieldName='' then
    Result:=Format('SELECT * FROM %s WHERE %s ', [aTableName, sWhere])
  else
    Result:=Format('SELECT %s FROM %s WHERE %s ', [aFieldName, aTableName, sWhere]);

end;


//--------------------------------------------------------------
function db_GetLocalComputerName: string;
//--------------------------------------------------------------
var
  PRes  : PChar;
  bRes  : boolean;
  iSize : DWORD;
begin
  iSize := MAX_COMPUTERNAME_LENGTH + 1;
  PRes := StrAlloc(iSize);
  BRes := GetComputerName(PRes, iSize);
  if BRes then
    Result := StrPas(PRes);

  StrDispose(PRes);

{
  if Eq(Result, 'local')     or Eq(Result, '(local)') or
     Eq(Result, '127.0.0.1') or Eq(Result, sServerName)
  then
    Result := '(local)';
}

end;

// ---------------------------------------------------------------
destructor TdbParamObj.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(Stream);

  inherited;
  // TODO -cMM: TdbParamObj.Destroy default body inserted
end;

//---------------------------------------------------------------------------
function TdbParamObj.GetValueForSQL: string;
//---------------------------------------------------------------------------
var
  i: Integer;
  s: string;
begin
 // s:=VarTypeAsText(FieldValue);
  if VarIsNull(FieldValue) then
    Result := 'NULL'
  else
  begin
    i:= VarType(FieldValue);

    if VarType(FieldValue) = 256 then
       Result := '''' + VarToStr(FieldValue) + ''''
    else
       Result := VarToStr(FieldValue);

//    i:= VarType(FieldValue);
  //  i:= VarType(FieldValue);

  end;


//  s:=VarTypeAsText(FieldValue);

end;

// ---------------------------------------------------------------
function TdbFieldList.AddItem(aFieldName: string; aType: TFieldType; aSize:  integer=200): TdbFieldItem;
// ---------------------------------------------------------------
begin

  Result := TdbFieldItem.Create;

//  if Eq(aFieldName, 'linkend_id') then
//    aFieldName :=aFieldName;

  Result.Name :=aFieldName;
  Result.Type_:=aType;
  Result.Size :=aSize;

  Add(Result);

end;

// ---------------------------------------------------------------
function TdbFieldList.FindByFieldName(aFieldName : string): TdbFieldItem;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aFieldName) then
    begin
      result := Items[i];
      exit;
    end;
end;


//function TdbFieldList.GetItems(Index: Integer): TdbFieldItem;
//begin
//  Result := TdbFieldItem(inherited Items[Index]);
//
//end;



//--------------------------------------------------------------------
function db_ADConnection_OpenFromReg(aRegPath: WideString; aConnectionObject:  TADOConnection): boolean;
//--------------------------------------------------------------------
var
  sConnectionString: string;

  rLoginRec: TdbLoginRec;
begin
  Assert(Assigned(aConnectionObject), 'Value not assigned');
  Assert(aRegPath <> '', 'aRegPath = ''''');

  db_LoginRec_LoadFromReg (aRegPath, rLoginRec);

  sConnectionString:=db_ADO_MakeConnectionString (rLoginRec);

  try

  //  if (adStateOpen and aConnectionObject.State) <> 0 then
      aConnectionObject.Close;

    aConnectionObject.ConnectionString:=sConnectionString; //,'','',0);
    aConnectionObject.Open;

    Result := True;
  except
    Result := False;
  end;

end;



end.



//
//// ---------------------------------------------------------------
//function db_OpenADOConnectionString(aADOConnection: TADOConnection; aConnectionString:
//    string): boolean;
//// ---------------------------------------------------------------
//begin
//  with aADOConnection do
//  begin
//    Close;
//    ConnectionString:=aConnectionString;
//    try
//      Open;
//    except
//    end;
//
//    Result := Connected;
//  end;
//end;
//
//


{

//------------------------------------------------------------------------------
procedure db_MakeParamRecFromQuery (aDataSet: TDataSet; var aRec: TDBParamRecArray;
                                    aExceptFieldList: array of string);
//------------------------------------------------------------------------------

  //-----------------------------------------------------------------
  function DoFindFieldInExceptList(aField: string; var aTotal: integer): boolean;
  //-----------------------------------------------------------------
  var
    j: Integer;
  begin
    Result:= false;
    for j:= 0 to High(aExceptFieldList) do
    begin
      if Eq(aField, aExceptFieldList[j]) then
      begin
        Dec(aTotal);
        if (aTotal > 0) then
          SetLength(aRec, aTotal);
        Result:=true;
        break;
      end;
    end;    // while
  end;
  // ---------------------------------------------------------------

var
  I, j, iTotal: Integer;
  oFieldList: TStringList;
begin
  oFieldList:= TStringList.Create;

  j:=0;
  aDataSet.GetFieldNames(oFieldList);
  iTotal:=oFieldList.Count;
  SetLength(aRec, iTotal);

  for i:= 0 to iTotal-1 do
  begin
    if DoFindFieldInExceptList(oFieldList[i], iTotal) then
      continue;

    aRec[j].FieldName:=  oFieldList[i];
    aRec[j].FieldValue:= aDataSet[oFieldList[i]];
    Inc(j);
  end;    // for

  oFieldList.Free;
end;
 }


 
                          {
//----------------------------------------------------------------------------
function db_MakeWhereQuery (aParams: array of TDBParamRec): string;
//----------------------------------------------------------------------------
var i: integer;
begin
  Result:='';

  for i:=0 to High(aParams) do
    if aParams[i].FieldName<>'' then
  begin
    Result:=Result + Format (' (%s=:%s) ', [aParams[i].FieldName,aParams[i].FieldName]);

    if i< High(aParams) then
      Result:=Result + ' and ';
  end;
end;

}




{
//--------------------------------------------------------------------
function db_OpenTable1(aQuery: TADOQuery; aTableName : string; aParams: array
    of TDBParamRec): Boolean;
//--------------------------------------------------------------------
var
  sSQL, sWhere: string;
begin
  sWhere:=db_MakeWhereQuery (aParams);
  sSQL:=Format('SELECT * FROM %s ', [aTableName]);
  if sWhere<>'' then
    sSQL:=sSQL + ' WHERE '+ sWhere;

  Result := db_OpenQuery (aQuery, sSQL, aParams);
end;
 }



{

//----------------------------------------------------------------------------
procedure db_UpdateRecordField (aDataset: TDataset; aFieldName: string; aFieldValue: variant);
//----------------------------------------------------------------------------
begin
  db_UpdateRecord(aDataset, [db_Par(aFieldName, aFieldValue)]);
end;
 {
//----------------------------------------------------------------------------
procedure db_UpdateRecord (aDataset: TDataset;
                           aParams: array of TDBParamRec);
//----------------------------------------------------------------------------
var i: integer;
begin
  try
    aDataset.Edit;
    for i:=0 to High(aParams)  do
      aDataset.FieldValues[aParams[i].FieldName]:=aParams[i].FieldValue;
    aDataset.Post;

  except
    on E: Exception do begin
//      gl_DB.ErrorMsg:= E.Message;
      //g_Log.AddRecord('u_db', 'db_UpdateRecord', E.Message);
    end;
  end;
end;
 }


{
//------------------------------------------------------------------------------
function db_JoinParams(aParams1,aParams2: TDBParamRecArray): TDBParamRecArray;
//------------------------------------------------------------------------------
var iLen,i: integer;
begin
  iLen:=Length(aParams1);
  SetLength (Result, Length(aParams1) + Length(aParams2));
  for i:=0 to Length(aParams1)-1 do
    Result[i]:=aParams1[i];
  for i:=0 to Length(aParams2)-1 do
    Result[iLen+i]:=aParams2[i];
end;

//--------------------------------------------------------------------
procedure db_AddToDBParamRecArray (var aRec: TDBParamRecArray; sFieldName: string; sFieldValue: variant);
//--------------------------------------------------------------------
begin
  SetLength(aRec, Length(aRec)+1);
  aRec[High(aRec)].FieldName:=  sFieldName;
  aRec[High(aRec)].FieldValue:= sFieldValue;
end;
 }

   {
// -------------------------------------------------------------------
function db_MoveRecordUpDown(aDataSet: TDataSet; aIndexFieldName: string; aDirectionIsUp:
    boolean): Boolean;
// -------------------------------------------------------------------
var
  iCurIndex, iCurID, iNewID, iNewIndex: integer;
begin
  Result:= false;


  assert(Assigned(aDataSet.FindField(FLD_ID)) );
  assert(Assigned(aDataSet.FindField(aIndexFieldName)) );


  with aDataSet do
  begin
    iCurID    := FieldByName(FLD_ID).asInteger;
    iCurIndex := FieldByName(aIndexFieldName).asInteger;


    if aDirectionIsUp then
      Prior
    else
      Next;


    if aDirectionIsUp then
    begin
      if BOF then exit;
    end
    else
      if EOF then exit;


    iNewIndex:=FieldByName(aIndexFieldName).asInteger;
    iNewID   :=FieldByName(FLD_ID).asInteger;

    db_UpdateRecord_(aDataSet, [aIndexFieldName, iCurIndex]);

    if Locate (FLD_ID, iCurID, []) then
      db_UpdateRecord(aDataSet, [db_Par(aIndexFieldName, iNewIndex)]);


    aDataSet.Locate (FLD_ID, iCurID, []);

    Result:= true;

  end;
end;
}
 {
// -------------------------------------------------------------------
procedure db_MoveRecordTopBottom(aDataSet: TDataSet; aIndexFieldName: string;
    aDirectionIsUp: boolean);
// -------------------------------------------------------------------
begin
  while db_MoveRecordUpDown (aDataSet, aIndexFieldName, aDirectionIsUp) do;
end;
   }


{

//----------------------------------------------------------------------------
procedure db_SetFieldCaptions111111111(aDataset: TDataset; aFieldNames : array
    of TStrArray);
//----------------------------------------------------------------------------
var I: Integer;
begin
  Assert(Assigned(aDataset), 'aDataset not assigned');
//  Assert(Length(aFieldNames) = Length(aCaptions));

  for I := 0 to High(aFieldNames) do
  begin
    Assert(Length(aFieldNames[i])=2);

    db_SetFieldCaption (aDataset, aFieldNames[i][0], aFieldNames[i][1]);
  end;
end;




// ---------------------------------------------------------------
function db_ExecStoredProc__new111111(aAdoStoredProc: TAdoStoredProc;
    aProcName: string; aParams: array of Variant; aFlags: TdbCommandFlags):
    Integer;
// ---------------------------------------------------------------
var
  oAddStringsList: TStringList;
  s, sAddString: string;
  oParam: TParameter;
  i: Integer;

  oParameter: TParameter;
  bEqual: boolean;
  k: Integer;
  oStrStream: TStringStream;
  sFieldName: string;
  v: Variant;
  vValue: Variant;

begin
  Assert(Assigned(aAdoStoredProc.Connection), 'aAdoStoredProc.Connection not assigned');


  try
   aAdoStoredProc.Close;


 //   aAdoStoredProc.Prepared:= true;
    i := aAdoStoredProc.Parameters.Count;

    bEqual:=aAdoStoredProc.ProcedureName = aProcName;
    aAdoStoredProc.ProcedureName:= aProcName;

    i := aAdoStoredProc.Parameters.Count;

    if (not bEqual) or (aAdoStoredProc.Parameters.Count=0) then
      aAdoStoredProc.Parameters.Refresh;


    for I := 0 to aAdoStoredProc.Parameters.Count-1 do
      oParameter:=aAdoStoredProc.Parameters[i];


//name=      '@RETURN_VALUE'
//datatype  ftInteger
    i := aAdoStoredProc.Parameters.Count;


   s := '';

   for i:=0 to aAdoStoredProc.Parameters.Count-1 do
    begin
      oParameter:=aAdoStoredProc.Parameters[i];
      s := s+ '--'+ aAdoStoredProc.Parameters[i].Name;
    end;


    Assert (length(aParams) mod 2 = 0);

//   if assigned(aOutParamList) then
//      for I := 0 to aOutParamList.Count - 1 do    // Iterate
//      begin
//        s:='@'+aOutParamList[i].FieldName;
//
//        oParameter:=aAdoStoredProc.Parameters.FindParam(s);
//        oParameter.Direction:=pdOutput;
//
//        //  TParameterDirection = (pdUnknown, pdInput, pdOutput, pdInputOutput,  pdReturnValue);
//
//      end;    // for
//


    for i:=0 to (length(aParams) div 2)-1 do
    begin
      sFieldName:=aParams[i*2];
      vValue    :=aParams[i*2+1];

      oParameter:=aAdoStoredProc.Parameters.FindParam('@'+ sFieldName);

//      oParameter:=oProc.Parameters.ParamByName('@'+aParams[i].FieldName);
      if Assigned(oParameter) then
      begin
        if oParameter.DataType = ftString then
        begin
          oStrStream:=TStringStream.Create;
          oStrStream.Clear;
          oStrStream.WriteString(vValue);

//          oParameter.LoadFromStream(oStrStream, ftString);
          oParameter.LoadFromStream(oStrStream, ftMemo);

          v:=oParameter.Value;

//          oParameter.Value:= vValue;

          FreeAndNil(oStrStream);

        end else
          oParameter.Value:= vValue;

      end
      else
        //g_Log.Error ('db_ExecStoredProc - field not found:'+aProcName+'-' + sFieldName);
    end;

 //////////   LogSys('db_ExecStoredProc: '+ aProcName);


   if flOpenQuery in aFlags then
//   if aIsOpenQuery then
     aAdoStoredProc.Open
   else
     aAdoStoredProc.ExecProc;



    oParameter:=aAdoStoredProc.Parameters.FindParam('@RETURN_VALUE');
    if Assigned(oParameter) then
      Result := oParameter.Value
    else
      Result := -9999;


//    if assigned(aOutParamList) then
//      for I := 0 to aOutParamList.Count - 1 do    // Iterate
//      begin
//        s:='@'+aOutParamList[i].FieldName;
//
//        oParameter:=aAdoStoredProc.Parameters.FindParam(s);
//
//        if Assigned(oParameter) then
//          aOutParamList[i].FieldValue := oParameter.Value;
//      //  else
//       //   aOutParamList[i].FieldValue :=null;
//
//
//      end;    // for
//




  except
    on E: Exception do
    begin
      s:= aProcName+CRLF+' Ïàðàìåòðû: ';

      for i:=0 to  ( Length(aParams) div 2 ) - 1 do
      begin
        sFieldName:=aParams[i*2];
        vValue    :=aParams[i*2+1];

        s:= s+ sFieldName+' - '+AsString(vValue)+' ; ';
      end;

      //g_Log.AddExceptionWithSQL('u_func_db.db_ExecStoredProc', E.Message, s);
    end;
  end;

end;




{
//------------------------------------------------------------------------------
function db_MakeInsertString1(aTblName: string; aRec: TDBParamRecArray): string;
//------------------------------------------------------------------------------
var i: Integer;
  sStr, sRes, sFlds, sValues: string;
begin
  for i:=0 to High(aRec) do
  begin
    sFlds:= sFlds + aRec[i].FieldName + ',';

    case VarType(aRec[i].FieldValue) of
      varString,
      varOleStr : begin
        sStr:= ReplaceStr(AsString(aRec[i].FieldValue), '''', '''''');

        sValues:= sValues + Format('''%s'',', [sStr]);
      end;
      varBoolean:  sValues:= sValues + AsString(AsBoolean(aRec[i].FieldValue)) + ',';
      varEmpty,
      varNull   : sValues:= sValues + 'NULL,';
      else
        sValues:= sValues + AsString(aRec[i].FieldValue) + ',';
    end;
  end;

  str_DeleteLastChar(sFlds);
  str_DeleteLastChar(sValues);

  Result:= Format('INSERT INTO %s (%s) VALUES (%s)', [aTblName, sFlds, sValues]);
end;
  }


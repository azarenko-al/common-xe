unit u_db_blob;

interface
uses
  ADODB, DB, SysUtils, Variants, Classes,

  u_zlib
  ;


type
    
  TdbBlobRec = record
    TableName: string;
    ID: integer;
    FieldName: string;
    FileName: string;
  end;



  procedure Blob_LoadFromFile(
          aADOConnection: TADOConnection;

          aRec: TdbBlobRec;
{          aTableName: string;
          aID: integer;
          aFieldName: string;
          aFileName: string;
}
          aIsCompressed: boolean);

  function Blob_SaveToFile(
          aADOConnection: TADOConnection;

          aRec: TdbBlobRec;
{
          aTableName: string;
          aID: integer;
          aFieldName: string;
          aFileName: string;
}

          aIsCompressed: boolean): boolean;



implementation

const
    FLD_ID = 'ID';


//----------------------------------------------------------------------------
procedure Blob_LoadFromFile (
                              aADOConnection: TADOConnection;
                              aRec: TdbBlobRec;
                              aIsCompressed: boolean);
//----------------------------------------------------------------------------
var sSQL: string;
  sFile: string;
  oADOCommand: TADOCommand;

  oSStream: TStringStream;
  
begin

  oSStream:=TStringStream.Create;



  if not FileExists(aRec.FileName) then
    raise Exception.Create('File not exist: '+ aRec.FileName);

  sSQL:=Format('UPDATE %s SET %s=:%s WHERE id=:id',
          [aRec.TableName, aRec.FieldName, aRec.FieldName]);


  if aIsCompressed then
  begin
    sFile:= ChangeFileExt (aRec.FileName,'~tmp');
    zlib_CompressFile(aRec.FileName, sFile);

    aRec.FileName:=sFile;
  end;


{

   gl_DB.Blob_SaveToFile(TableName, iID, FLD_CONTENT, sFile);

   DeleteFile(sFile);
}

  oADOCommand:=TADOCommand.Create(nil);
  oADOCommand.Connection:=aADOConnection;

  with oADOCommand do
  begin
    CommandText := sSQL;
    Parameters.ParamByName(FLD_ID).Value :=aRec.ID;
    Parameters.ParamByName(aRec.FieldName).LoadFromFile(aRec.FileName, ftBlob);//ftMemo); // Blob);
  

//    oSStream.WriteString('aaaaaaaa');
//    Parameters.ParamByName(aRec.FieldName).LoadFromStream(oSStream, ftBlob);

    Execute;
  end;

  oADOCommand.Free;

  if aIsCompressed then
    DeleteFile (aRec.FileName);

end;

{
   sFile:=GetTempFileName_('');
   zlib_CompressFile(aRec.FileName, sFile);

   gl_DB.Blob_SaveToFile(TableName, iID, FLD_CONTENT, sFile);

   DeleteFile(sFile);
}


//------------------------------------------------------------------
function Blob_SaveToFile(aADOConnection: TADOConnection;
                         aRec: TdbBlobRec;
                         aIsCompressed: boolean): boolean;

//------------------------------------------------------------------
var sSQL: string;
    oBlobField: TBlobField;
     oADOQuery: TADOQuery;
begin
  Assert (aRec.FieldName<>'', 'aFieldName=''''');
  Assert (aRec.TableName<>'', 'aTableName=''''');


  oADOQuery:=TADOQuery.Create(nil);
  oADOQuery.Connection:=aADOConnection;

 // Query.Close;

  oBlobField:=TBlobField.Create(nil);
  oBlobField.FieldName:=aRec.FieldName;

  oBlobField.DataSet:=oADOQuery;

  sSQL:=Format('SELECT %s FROM %s WHERE id=%d', [aRec.FieldName, aRec.TableName, aRec.ID]);

  oADOQuery.SQL.Text:=sSQL;
  oADOQuery.Open;
//  db_OpenQuery (oADOQuery, sSQL);


 // Result:=Query.Fields[0].Value;

 // ShowMessage('');
//  ForceDirByFileName (aFileName);

  try
    ForceDirectories(ExtractFileDir(aRec.FileName));

    oBlobField.SaveToFile (aRec.FileName);

  {  
  if aIsCompressed then
  begin
    sFile:=GetTempFileName_('');
    zlib_CompressFile(aFileName, sFile);

    aFileName:=sFile;
  end;

}
    Result := True;
  except
    Result := False;
  end;

  oBlobField.Free;
  oADOQuery.Free;


end;




end.

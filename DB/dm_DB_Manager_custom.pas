unit dm_DB_Manager_custom;

interface

uses
  u_log_,

//  u_log,

  u_db,


  SysUtils, Classes, DB, Forms, Variants, CodeSiteLogging,  Dialogs,  Vcl.Controls,
  Data.Win.ADODB;


type
  TdmDB_Manager_custom = class(TDataModule)
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    ADOCommand1: TADOCommand;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
//    procedure DataModuleCreate(Sender: TObject);
  private
 //   FADOConnectionRef :  TADOConnection;

//    function OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string;
 //       aParams: array of Variant): integer; overload;

    function ExecCommand(aSQL: string; aParamList: TdbParamList = nil): Boolean;
    procedure Log(aMsg: string);
 //   procedure SetADOConnection(const Value: TADOConnection);
  public
    Last_Result : integer;
//    class procedure Init;

    procedure Dlg_AdoConnection_setup;

    function StoredProc_Open(aADOStoredProc: TADOStoredProc; aProcName: string;    aParams: array of Variant): integer; overload;

    function StoredProc_Exec(aADOStoredProc: TADOStoredProc; aProcName: string;     aParams: array of Variant): Integer; overload;

    function ADOConnection_Open(aConnectionString: string): Boolean;
//    function OpenStoredProc(aADOStoredProc: TAdoStoredProc; aProcName: string;     aParams: array of Variant): Integer; overload;

    procedure OpenQuery(aADOQuery: TADOQuery; aSQL : string); overload;
    function OpenQuery(aADOQuery : TADOQuery; aSQL : string; aParams : array of  Variant): boolean; overload;


    function OpenTable(aADOTable: TADOTable; aTableName: string = ''): Boolean;


    function UpdateRecordByID(aTableName : string; aID : integer; aParams: array of   Variant): Boolean;
//  public
//    class procedure Init;



// TODO: Template_Site_select_item
//  procedure Template_Site_select_item(aADOStoredProc: TADOStoredProc; aID: Integer);

//    function TransferTech_Copy(aID: Integer; aName: string): integer;
//    function UpdateRecordByID(aTableName : string; aID : integer; aParams: array of
 //       TDBParamRec): boolean; overload;

  //  function UpdateRecordByID_list(aTableName : string; aID : integer; aParamList:
   //     TdbParamList): boolean;


 // public


//    function UpdateRecordByID(aTableName : string; aID : integer; aParamList:
 //       TdbParamList): boolean; overload;

//    property ADOConnection: TADOConnection read FADOConnectionRef write
//        SetADOConnection;

  end;

//
//var
//  dmDB_Manager_custom: TdmDB_Manager_custom;


implementation



{$R *.dfm}

procedure TdmDB_Manager_custom.Dlg_AdoConnection_setup;
begin
  PromptDataSource(0, ADOConnection1.ConnectionString);

end;

//
//const
//  FLD_PMP_CALC_REGION_ID = 'PMP_CALC_REGION_ID';
//

//
//const
//  FLD_GEOMETRY_STR = 'GEOMETRY_STR';
//  FLD_Map_ID = 'Map_ID';
//
//  //FLD_GEOMETRY_LINE_STR ='GEOMETRY_LINE_STR';
////  FLD_GEOMETRY_REVERSE_STR = 'GEOMETRY_REVERSE_STR';
// // FLD_GEOMETRY_ERRORS = 'GEOMETRY_ERRORS';
//
//
//  FLD_FILTER_ID = 'FILTER_ID';
//

{
class procedure TdmDB_Manager_custom.Init;
begin
  Assert(not Assigned(dmDB_Manager_custom));

  Application.CreateForm(TdmDB_Manager_custom, dmDB_Manager_custom);
end;

}


//----------------------------------------------------------------------------
function TdmDB_Manager_custom.UpdateRecordByID(aTableName : string; aID : integer;
    aParams: array of Variant): Boolean;
//----------------------------------------------------------------------------
var
  s: string;
  oParamList: TdbParamList;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecordByID - aTableName=''''');


  oParamList:= TdbParamList.Create;
  oParamList.LoadFromArr_variant(aParams);

  s:=db_MakeUpdateString(aTableName, aID, oParamList);

  Result := ExecCommand (s, oParamList);

  FreeAndNil(oParamList);

end;



//--------------------------------------------------------------------
function TdmDB_Manager_custom.ExecCommand(aSQL: string; aParamList: TdbParamList = nil):
    Boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
  oStrStream: TStringStream;

  vValue: Variant;
   v: Variant; 
begin
  Assert(aSQL<>'');

  ADOCommand1.CommandText:=aSQL;

  if Assigned(aParamList) then
    for i:=0 to aParamList.Count-1 do
    begin
  //    sFieldName:=aParams[i*2];
    //  vValue    :=aParams[i*2+1];
          
      oParameter:=ADOCommand1.Parameters.FindParam (aParamList[i].FieldName);

      vValue:=aParamList[i].FieldValue;
      
      
      Assert(oParameter<>nil);
      
      if Assigned(oParameter) then 
      try
        if oParameter.DataType = ftString then
        begin
          oStrStream:=TStringStream.Create;
          oStrStream.Clear;
          oStrStream.WriteString(vValue);
          
//          oParameter.LoadFromStream(oStrStream, ftString);
          oParameter.LoadFromStream(oStrStream, ftMemo);

          v:=oParameter.Value;
        
//          oParameter.Value:= vValue;

          FreeAndNil(oStrStream);
          
        end else
          oParameter.Value:= vValue;
      
//        oParameter.Value :=aParamList[i].FieldValue;
      except
      end;
    end;

  try
    ADOCommand1.Execute;
    Result:=True;
  except
    Result:=False;
  end;

end;

// ---------------------------------------------------------------
function TdmDB_Manager_custom.StoredProc_Exec(aADOStoredProc: TADOStoredProc;
    aProcName: string; aParams: array of Variant): Integer;
// ---------------------------------------------------------------
begin
//  Assert(Assigned(FADOConnectionRef), 'Value not assigned');

//  Result := db_ExecStoredProc__new(ADOStoredProc1, aProcName, aParams,[]);
  aADOStoredProc.Connection := ADOConnection1;
  Result := db_ExecStoredProc(aADOStoredProc, aProcName, aParams);

  Last_Result:=Result;

//  Result := db_ExecStoredProc_v1(aADOStoredProc, aProcName, aParams);

//  Result := db_ExecStoredProc_(FADOConnectionRef, aProcName, aParams);
end;

// ---------------------------------------------------------------

//
//function TdmDB_Manager_custom.OpenStoredProc(aADOStoredProc: TAdoStoredProc;
//    aProcName: string; aParams: array of Variant): Integer;
//begin
//  aADOStoredProc.Connection := ADOConnection1;
//
//  Result := db_OpenStoredProc (aADOStoredProc, aProcName, aParams,[]);
//end;
//
//
//


procedure TdmDB_Manager_custom.Log(aMsg: string);
begin
  CodeSite.Send(aMsg);

  //g_Log.SysMsg (aMsg);
end;

//-------------------------------------------------------------------
function TdmDB_Manager_custom.ADOConnection_Open(aConnectionString: string):   Boolean;
//-------------------------------------------------------------------
begin
  if ADOConnection1.Connected then
  begin
//    ShowMessage ('ADOConnection1.Connected');
    CodeSite.SendError('ADOConnection1.Connected');
  end;

  Result:=db_OpenADOConnectionString (ADOConnection1, aConnectionString);

//
//  ADOConnection1.Close;
//  ADOConnection1.ConnectionString:=aConnectionString;
//
//  try
//    ADOConnection1.Open;
//    Result:=ADOConnection1.Connected;
//
//  except // wrap up
//    Result:=False;
//
//    CodeSite.SendError(aConnectionString);
//
//  TLog.Error (aConnectionString);
//
// //   ShowMessage (aConnectionString);
//
////    ADOConnection1.ConnectionString:=PromptDataSource(0, aConnectionString);
//
////    Dlg_AdoConnection_setup
//
////    raise Exception.Create(' ADOConnection1:  ' + aConnectionString);
//  end;    // try/finally

end;

// ---------------------------------------------------------------
procedure TdmDB_Manager_custom.OpenQuery(aADOQuery: TADOQuery; aSQL : string);
// ---------------------------------------------------------------
begin
  aADOQuery.Connection := ADOConnection1;
  db_OpenQuery(aADOQuery, aSQL, [] );

//  db_OpenQuery(aADOQuery, aSQL);
end;

function TdmDB_Manager_custom.OpenQuery(aADOQuery : TADOQuery; aSQL : string;  aParams : array of Variant): boolean;
begin
  aADOQuery.Connection := ADOConnection1;
  db_OpenQuery(aADOQuery, aSQL, aParams);
end;


//-------------------------------------------------------------------
function TdmDB_Manager_custom.StoredProc_Open(aADOStoredProc: TADOStoredProc; aProcName:  string; aParams: array of Variant): integer;
//-------------------------------------------------------------------
begin
  Assert(Assigned(ADOStoredProc1), 'Value not assigned');

  aADOStoredProc.Connection := ADOConnection1;
  Result := db_ExecStoredProc(aADOStoredProc, aProcName, aParams, true );
//  Result := db_ExecStoredProc_v1(aADOStoredProc, aProcName, aParams, true );

//// ---------------------------------------------------------------
//function db_ExecStoredProc_v1(aAdoStoredProc: TAdoStoredProc; aProcName:
//    string; aParams: array of Variant; aFlags: TdbCommandFlags): Integer;
//// ---------------------------------------------------------------


//  Result := db_OpenStoredProc(aADOStoredProc, aProcName, aParams, true); //aIsOpenDataset
end;

//-------------------------------------------------------------------
function TdmDB_Manager_custom.OpenTable(aADOTable: TADOTable; aTableName: string = ''): Boolean;
//-------------------------------------------------------------------
//var
//  bm: TBookmark;
begin
  Screen.Cursor:=crHourGlass;

  aADOTable.Close;
  aADOTable.Connection := ADOConnection1;

  if aTableName<>'' then
    aADOTable.TableName:=aTableName;


  try
//    aADOTable.boo

//    aADOTable.Close;
    aADOTable.Open;

   // db_View(aADOTable);

    Result:=True
  except
    Result:=False
  end;

  Screen.Cursor:=crDefault;

end;







end.

  {
   i,iCluCode:integer;
  bm: TBookmark;
  rec:  TrelProfilePointRec;
begin
  Assert(Data.Count=aDataset.RecordCount);

  with aDataset do
  begin
    bm:=GetBookmark;
    DisableControls;

    First;


    while not EOF do
    begin
      i:=FieldByName(FLD_INDEX).AsInteger;

      FillChar(rec, SizeOf(rec), 0);

      rec.Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      rec.rel_h       := FieldByName(FLD_rel_h).AsInteger;
      rec.Clutter_code:= FieldByName(FLD_clu_code).AsInteger;

      Items[i]:=rec;

//      Item

{//      Data.Items[i].Distance_KM :=(FieldByName(FLD_DIST_M).AsFloat / 1000);
      Items[i].Clutter_H   := FieldByName(FLD_clu_h).AsInteger;
      Items[i].rel_h       := FieldByName(FLD_rel_h).AsInteger;
      Items[i].Clutter_code:= FieldByName(FLD_clu_code).AsInteger;
}
      Next;
    end;

    GotoBookmark(bm);
    EnableControls;
  end;

end;

procedure TdmDB_Manager_custom.SetADOConnection(const Value: TADOConnection);
begin
  FADOConnectionRef := Value;

  ADOQuery1.Connection      :=FADOConnectionRef;
  ADOStoredProc1.Connection :=FADOConnectionRef;
  ADOCommand1.Connection    :=FADOConnectionRef;

end;




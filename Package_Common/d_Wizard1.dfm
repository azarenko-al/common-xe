object dlg_Wizard1: Tdlg_Wizard1
  Left = 645
  Top = 399
  Width = 468
  Height = 385
  Caption = 'Custom Wizard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 323
    Width = 460
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      460
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 460
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 297
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 381
      Top = 8
      Width = 75
      Height = 23
      Action = act_Cancel
      Anchors = [akTop, akRight]
      Cancel = True
      ModalResult = 2
      TabOrder = 1
    end
  end
  object pn_Top_: TPanel
    Left = 0
    Top = 0
    Width = 460
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 1
    Visible = False
    object Bevel2: TBevel
      Left = 0
      Top = 57
      Width = 460
      Height = 3
      Align = alBottom
      Shape = bsTopLine
    end
    object pn_Header: TPanel
      Left = 0
      Top = 0
      Width = 460
      Height = 57
      Align = alClient
      BevelOuter = bvLowered
      BorderWidth = 6
      Color = clInfoBk
      TabOrder = 0
      object lb_Action: TLabel
        Left = 8
        Top = 8
        Width = 44
        Height = 13
        Caption = 'lb_Action'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object ActionList1: TActionList
    Left = 433
    Top = 4
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = act_CancelExecute
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredValues = <>
    Left = 408
    Top = 4
  end
end

unit u_Log;

interface

uses
  Forms,SysUtils, Messages, Windows, Classes, Graphics, StdCtrls, ComCtrls, //

//  DbugIntf //SendDebug(aMsg);

  u_func,

 u_files;

type
  TLogMsgType = (lmtError, lmtException, lmtSystemInfo, lmtInformation, lmtAppend);  //lmtCustom,
  // lmtAppend - �������� � ��������� ������

  TOnLogEvent = procedure(Sender: TObject; aMsg: string; aMsgType: TLogMsgType =   lmtInformation) of object;

//  TMsgDlgType = (mtWarning, mtError, mtInformation, mtConfirmation, mtCustom);

  TLog = class(TStringList)
  private
    FFileName: String;
//    FOnLog: TOnLogEvent;
    FMsgCount : Integer;
    FLastTime : TDateTime;
    FRichEdit: TRichEdit;

    procedure AddVirtualRecord(AErrorText: String);
    procedure LogToRichView(aMsg: string; aMsgType: TLogMsgType = lmtInformation);
    procedure SetRichEdit(const Value: TRichEdit);

    procedure WriteToFile(aMsg: string);

  private
    LTime: TDateTime;

  public
    ErrorCount: Integer;

    constructor CreateFile(aFileName: string='');

    procedure SetFileName(aFileName: string);

    procedure AddException(AClassName, aMsg: string); overload;
    procedure AddException(AClassName, aMsg, AErrorText: string); overload;
    procedure AddExceptionWithSQL(AClassName, AErrorText, aSQL: String);

    procedure AddRecord(ADestination, AErrorText: String); overload;
    procedure AddRecord(ANameModule, ANameMethod, AErrorText: String); overload;

    procedure Add(aMsg: string; aMsgType: TLogMsgType = lmtInformation); // override;
    procedure AddError (ADestination, AErrorText: String);    overload;
    procedure AddError (ADestination, AErrorText,AErrorText2 : String);    overload;

//    procedure SysMsg (aMsg: string);
    procedure SysMsg (aMsg: string);
    procedure Error  (aMsg: String);
//    procedure ErrorInClass (AMsg: String);

    procedure Msg(aMsg: string);

    procedure Timer_Start;
    procedure ShowFile;

    procedure ShowTime;
    procedure DeleteLogFile;

    property RichEdit: TRichEdit read FRichEdit write SetRichEdit;

//    property OnLog: TOnLogEvent read FOnLog write FOnLog;
  end;


var
  g_Log: TLog;

//  gLog: TLog;



// ==================================================================
implementation
// ==================================================================

const
//  ERROR_MSG_TO_INTERFACE = '!!!  ��������� ����������� ������(����������). ����������, ���������� � �������������.  !!!';

  NAME_MODULE = '������: ';
  NAME_METHOD = '�������: ';

const
  LOG_ERROR_COLOR = clRed;
  DEF_SYSMSG_COLOR = clNavy;




constructor TLog.CreateFile(aFileName: string='');
begin
  inherited Create;
  SetFileName (aFileName);
end;


procedure TLog.SetFileName(aFileName: string);
begin
  if aFileName='' then
   Exit;

  FFileName:=aFileName;

  ForceDirectories (ExtractFileDir (FFileName));

 // ForceDirByFileName(FFileName);
end;


//---------------------------------------------------------
procedure TLog.AddVirtualRecord(AErrorText: String);
//---------------------------------------------------------
begin
////////////  FErrorStrings.Add1(DateTimeToStr(now)+': '+AErrorText);
  //PostEvent(WE_LOG, PAR_NAME, AErrorText);
  WriteToFile (AErrorText);
end;

procedure TLog.AddException(AClassName, aMsg: string);
const
  ERROR_MSG_TO_LOG = 'Unit: %s. ������: %s. ';
var
  S: String;
begin
  S:=Format(ERROR_MSG_TO_LOG, [AClassName, aMsg]);
  Add(s, lmtException);
end;


//---------------------------------------------------------
procedure TLog.AddException(AClassName, aMsg, AErrorText: string);
//---------------------------------------------------------
const
  ERROR_MSG_TO_LOG = '������: %s. %s. ����� ������: %s. ';
var
  S: String;
begin
//  PostEvent(WE_LOG, PAR_NAME, ERROR_MSG_TO_INTERFACE);
//////  AddRecord(AClassName, Format(ERROR_MSG, [AClassName, AErrorText]));

  //S:=DateTimeToStr(now)+' '+
  S:=Format(ERROR_MSG_TO_LOG, [AClassName, aMsg, AErrorText]); //+' '+CRLF+CRLF;
 // WriteToFile(s);


// lmtError, lmtException,
  Add(s, lmtException);
end;

//---------------------------------------------------------
procedure TLog.AddExceptionWithSQL(AClassName, AErrorText, aSQL: String);
//---------------------------------------------------------
const
  ERROR_MSG_TO_LOG = '������: %s. ����� ������: %s. Sql: %s';
var
  S: String;
begin
//  PostEvent(WE_LOG, PAR_NAME, ERROR_MSG_TO_INTERFACE);

//  S:=DateTimeToStr(now)+' '+
  S:=Format(ERROR_MSG_TO_LOG, [AClassName, AErrorText, aSQL]); //+' '+CRLF+CRLF;

  Add(s, lmtException);
//  WriteToFile(s);
end;

//---------------------------------------------------------
procedure TLog.AddError(ADestination, AErrorText: String);
//---------------------------------------------------------
const
  ERROR_MSG = '������! ������: %s. ����� ������: %s.';
begin
  Error(ADestination + Format(ERROR_MSG, [ADestination, AErrorText]));
end;

//---------------------------------------------------------
procedure TLog.AddError(ADestination, AErrorText, AErrorText2: String);
//---------------------------------------------------------
const
  ERROR_MSG = '������! ������: %s. ����� ������: %s, %s';
begin
  Error(ADestination + Format(ERROR_MSG, [ADestination, AErrorText, AErrorText2]));
end;


//---------------------------------------------------------
procedure TLog.Error(aMsg: String);
//---------------------------------------------------------
begin
  Inc(ErrorCount);
  Add(aMsg, lmtError);
end;



//---------------------------------------------------------
procedure TLog.WriteToFile(aMsg: string);
//---------------------------------------------------------
var
  F: TextFile;
begin
  if FFileName='' then
    Exit;


  AssignFile (F, FFileName);

  if FileExists(FFileName) then
    System.Append(F)
  else
    System.Rewrite(F);

//  Writeln(F, DateTimeToStr(now)+' : '+aMsg);
  Writeln(F, aMsg);
  CloseFile(F);

end;





//---------------------------------------------------------
procedure TLog.ShowFile;
//---------------------------------------------------------
begin
 (* Assert(FFileName<>'', 'FFileName<>''''');

  if FileExists(FFileName) then
    ShellExec('notepad.exe', FFileName)
  else
    ShowMessage('���� �� ������ - '+ FFileName);
*)
end;

//---------------------------------------------------------
procedure TLog.AddRecord(ADestination, AErrorText: String);
//---------------------------------------------------------
begin
  WriteToFile(ADestination+' '+AErrorText);
end;

//---------------------------------------------------------
procedure TLog.AddRecord(ANameModule, ANameMethod, AErrorText: String);
//---------------------------------------------------------
begin
  WriteToFile(NAME_MODULE+ ANameModule+' '+NAME_METHOD+ ANameMethod+' '+AErrorText);
end;


procedure TLog.Msg(aMsg: string);
begin
  Add(aMsg);
end;

//---------------------------------------------------------
procedure TLog.Add(aMsg: string; aMsgType: TLogMsgType = lmtInformation);
//---------------------------------------------------------
begin
  Inc(FMsgCount);

  FLastTime:=Now();

//  if aMsgType<>lmtAppend then
//    aMsg:=Format('%s | %s', [FormatDateTime ('dd.mm.yyyy hh:mm:ss',Now), aMsg]);


  if assigned(RichEdit) then
    LogToRichView(aMsg, aMsgType);

  if aMsgType in [lmtError,lmtException] then
    WriteToFile (aMsg);

  inherited AddObject(aMsg, Pointer(aMsgType));
end;



//-------------------------------------------------------------------
procedure TLog.LogToRichView(aMsg: string; aMsgType: TLogMsgType = lmtInformation);
//-------------------------------------------------------------------
begin
  case aMsgType of
    lmtSystemInfo: RichEdit.SelAttributes.Color:=DEF_SYSMSG_COLOR; // Exit; //!!!!!!!!!!!!!!!!!!

    lmtError,
    lmtException:  RichEdit.SelAttributes.Color:=LOG_ERROR_COLOR;

  else
    RichEdit.SelAttributes.Color:=RichEdit.Font.Color;
  end;


  if aMsgType<>lmtAppend then
  begin
    RichEdit.Lines.Add (aMsg);
    SendMessage (RichEdit.Handle, EM_LINESCROLL, 0, 1);
  end
  else
    with RichEdit do
      Lines[Lines.Count-1]:=Lines[Lines.Count-1]+ aMsg;


 // dlg_ScrollMemo (RichEdit.Handle);

end;


//---------------------------------------------------------
procedure TLog.SysMsg(aMsg: string);
//---------------------------------------------------------
begin
  Add(aMsg, lmtSystemInfo);
//  WriteToFile (aMsg);
 // SendDebug(aMsg); //GEXperts

end;

//---------------------------------------------------------
procedure TLog.DeleteLogFile;
//---------------------------------------------------------
begin
  SysUtils.DeleteFile(FFileName);
end;

// ---------------------------------------------------------------
procedure TLog.SetRichEdit(const Value: TRichEdit);
// ---------------------------------------------------------------
begin
  FRichEdit := Value;

  if Assigned(Value) then
  begin
    FRichEdit.Lines.Clear;
    FRichEdit.ScrollBars := ssBoth;
    FRichEdit.Lines.Text := Text;
  end;
end;


procedure TLog.Timer_Start;
begin
  LTime:= Now;
end;


procedure TLog.ShowTime;
var
  dt: TDateTime;
  s: string;
begin
  dt:= Now - LTime;
  s := FormatDateTime ('hh:mm:ss:zzz', dt);
  Add ('Execution time: '+ s);
end;


//var
//  s : string;

initialization
//  s := ExtractFileName (Application.ExeName);
//  s := ChangeFileExt(s, '.txt');
//  s := IncludeTrailingBackslash(GetTempFileDir())+ '_'+ s;

//  s := ExtractFileName (Application.ExeName);
 // s := ChangeFileExt('_' + s, '.log');


//  s := ChangeFileExt(Application.ExeName, '.log');

  g_Log:=TLog.Create; //File(s);

//  gLog :=g_Log;

// ShowMessage(Application.Name);
// g_Log:=TLog.Create ('c:\temp\rpls\_log.txt');

finalization
  FreeAndNil(g_Log);

end.

{

initialization
//  s := ExtractFileName (Application.ExeName);
//  s := ChangeFileExt(s, '.txt');
//  s := IncludeTrailingBackslash(GetTempFileDir())+ '_'+ s;

//  s := ExtractFileName (Application.ExeName);
 // s := ChangeFileExt('_' + s, '.log');


  s := ChangeFileExt(Application.ExeName, '.log');

  g_Log:=TLog.CreateFile(s);

//  gLog :=g_Log;

// ShowMessage(Application.Name);
// g_Log:=TLog.Create ('c:\temp\rpls\_log.txt');

finalization

  Assert(Assigned(g_Log), 'Value not assigned');

  FreeAndNil(g_Log);

end.


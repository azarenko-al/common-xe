﻿unit d_Progress;

interface

uses
  Windows, Messages, SysUtils, Classes, Forms, Dialogs, 
  StdCtrls, ComCtrls, ActnList, Controls, System.Actions

  ;

type
  TProcedureEvent  = procedure of object;


  Tdlg_Progress = class(TForm)
    ActionList1: TActionList;
    act_Stop: TAction;
    act_Close: TAction;
    btn_Stop: TButton;
    lb_Msg1: TLabel;
    ProgressBar1: TProgressBar;
    lb_Progress1: TLabel;
    lb_Progress2: TLabel;
    ProgressBar2: TProgressBar;
    lb_Msg2: TLabel;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DoActions(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormHide(Sender: TObject);
  private
    FRegPath: string;
    
    FOnStart: TProcedureEvent;
    FOnStart1: TProcedureEvent;

  //  FOnStart1: TProgressStartEvent;

    FShowDetails: Boolean;
    FTerminated: Boolean;

    FOldProgress: integer;

    function GetTerminated: Boolean;
    procedure SetTerminated(const Value: Boolean);
    
  public
// TODO: ShowDetails
//  procedure ShowDetails (aValue: boolean);

    procedure DoOnLog(aMsg: string; aMsgType: TMsgDlgType = mtInformation);

    procedure DoOnProgress1 (aProgress,aTotal: integer; var aTerminated: boolean);
    procedure DoOnProgress2 (aProgress,aTotal: integer; var aTerminated: boolean);

    procedure Progress1(aProgress,aTotal: integer);
    procedure Progress2(aProgress,aTotal: integer);

    procedure ProgressMsg (aMsg: string);
    procedure ProgressMsg2 (aMsg: string);

    procedure Log (aMsg: string);


    procedure DoOnProgressMsg (aMsg: string);
    procedure DoOnProgressMsg2 (aMsg: string);
    procedure DoOnCaption (aMsg: string);

    constructor CreateForm(aCaption: string); //overload;
 //   class procedure Init;

    property OnStart: TProcedureEvent read FOnStart write FOnStart;
    property OnStart1: TProcedureEvent read FOnStart1 write FOnStart1;

//    property OnStart: TProgressStartEvent read FOnStart write FOnStart;
  //  property OnStart1: TProgressStartEvent read FOnStart1 write FOnStart1;

    property Terminated: Boolean read GetTerminated write SetTerminated;
  end;


  
function Progress_ExecDlg(aCaption: string; aOnStartEvent: TProcedureEvent): Boolean;
function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''): Boolean;

procedure Progress_SetProgress1(aProgress,aTotal: integer; var aTerminated: boolean);
procedure Progress_SetProgress2(aProgress,aTotal: integer; var aTerminated: boolean);

procedure Progress_SetProgressMsg1(aMsg: string);
procedure Progress_SetProgressMsg2(aMsg: string);

function Progress_Show(aCaption: string): Boolean;

procedure Progress_Free;


//procedure Progress_SetProgressMsg2(aMsg: string);




var
  dlg_Progress: Tdlg_Progress;


//================================================================
implementation {$R *.DFM}
//================================================================


const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';

//  DEF_HEIGHT = 140;

  DEF_HEIGHT = 155;

  function FormatProgress (aProgress,aTotal: integer): string; forward;


{class procedure Tdlg_Progress.Init;
begin
  if not Assigned(dlg_Progress) then
    Application.CreateForm(Tdlg_Progress, dlg_Progress);
end;
}

// ---------------------------------------------------------------
function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''): Boolean;
// ---------------------------------------------------------------
begin
  Application.CreateForm(Tdlg_Progress, dlg_Progress);

  if aCaption='' then aCaption:='Выполнение..';

  dlg_Progress.Caption:=aCaption;

//  dlg_Progress:=Tdlg_Progress.CreateForm(aCaption);
//  dlg_Progress.OnStart:=aOnStartEvent;
  dlg_Progress.OnStart1:=aOnStartEvent;
  dlg_Progress.ShowModal;

  Assert(Assigned(dlg_Progress), 'Value not assigned');

  Result := not dlg_Progress.Terminated;

  FreeAndNil(dlg_Progress);
end;


constructor Tdlg_Progress.CreateForm(aCaption: string);
begin
  if aCaption='' then aCaption:='Выполнение';

  inherited Create (nil);

  Caption:=aCaption;
end;

// ---------------------------------------------------------------
procedure Tdlg_Progress.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

 // ShowMessage('step1');

//zzz  FormStorage1.IniFileName:=FRegPath;
//  FormStorage1.IniSection:='Window';
 //zz FormStorage1.Active:=True;

 // ShowMessage('step2');

//  FormStorage1.IniFileName:=REGISTRY_COMMON_FORMS + ClassName +'\';
 // FormStorage1.Active:=True;

//////  RichEdit1.Align:=alClient;

  lb_Msg1.Caption:='';
  lb_Msg2.Caption:='';

  lb_Progress1.Caption:='';
  lb_Progress2.Caption:='';

  Height:=DEF_HEIGHT;

//  ClientHeight:=pn_Top.Height-2;
 // btn_Details.Action:=act_Details_show;

 ///// pn_Top.Align:=alClient;

 //zzzz ShowDetails (False);
end;                                                             

// ---------------------------------------------------------------
procedure Tdlg_Progress.FormActivate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Screen.Cursor:=crAppStart;
  Application.ProcessMessages;


  if Assigned(FOnStart) then
  begin
    Application.ProcessMessages;
    FOnStart();

    btn_Stop.Action:=act_Close;

//zzzzzzzzz    if cb_Close_on_done.Checked then
     // Hide;
     // Close;
      PostMessage (Handle, WM_CLOSE, 0,0);
  end;

  // ----------------------------------
  if Assigned(FOnStart1) then
  begin
    Application.ProcessMessages;
    FOnStart1();

    btn_Stop.Action:=act_Close;

   //zzzzzz if cb_Close_on_done.Checked then
     // Hide;
     // Close;
      PostMessage (Handle, WM_CLOSE, 0,0);
  end;

end;


procedure Tdlg_Progress.FormShow(Sender: TObject);
begin
  ProgressBar1.Max:=0;
  ProgressBar2.Max:=0;

end;


procedure Tdlg_Progress.Log (aMsg: string);
begin
  DoOnLog(aMsg);
end;

procedure Tdlg_Progress.DoOnLog(aMsg: string; aMsgType: TMsgDlgType =
    mtInformation);
begin

 (* Application.ProcessMessages;


  if aMsgType=mtError then
    RichEdit1.SelAttributes.Color:=clRed
  else
    RichEdit1.SelAttributes.Color:=Font.Color;


  RichEdit1.Lines.Add(aMsg);
  SendMessage(RichEdit1.Handle, EM_LINESCROLL, 0, 1);

*)
end;

//----------------------------------------------------------------------------
function FormatProgress (aProgress,aTotal: integer): string;
//----------------------------------------------------------------------------
var d1,d2: double;
begin
  if aTotal=0 then
    Result:=''
  else begin
    d1:=aProgress;
    d2:=aTotal;
    Result:=Format('%1.0n / %1.0n', [d1,d2]);
  end;
end;


procedure Progress_SetProgress1(aProgress,aTotal: integer; var aTerminated: boolean);
begin
  if Assigned(dlg_Progress) then
    dlg_Progress.DoOnProgress1(aProgress,aTotal,aTerminated);
end;

procedure Progress_SetProgress2(aProgress,aTotal: integer; var aTerminated: boolean);
begin
  if Assigned(dlg_Progress) then
    dlg_Progress.DoOnProgress2(aProgress,aTotal,aTerminated);
end;

procedure Progress_SetProgressMsg1(aMsg: string);
begin
  if Assigned(dlg_Progress) then
    dlg_Progress.DoOnProgressMsg(aMsg);
end;


procedure Progress_SetProgressMsg2(aMsg: string);
begin
  if Assigned(dlg_Progress) then
    dlg_Progress.DoOnProgressMsg2(aMsg);
end;



procedure Tdlg_Progress.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin

//  if not FShowDetails then
 //   Height:=DEF_HEIGHT;

end;

//----------------------------------------------------------------------------
procedure Tdlg_Progress.DoOnProgress1 (aProgress,aTotal: integer; var aTerminated: boolean);
// var aPause : boolean; var aSkiped : boolean);
//----------------------------------------------------------------------------
begin
  ProgressBar1.Max:=aTotal;
  ProgressBar1.Position:=aProgress;

  lb_Progress1.Caption:=FormatProgress (aProgress, aTotal);
  aTerminated:=Terminated;

  Application.ProcessMessages;
end;


procedure Tdlg_Progress.DoOnProgress2 (aProgress,aTotal: integer; var aTerminated: boolean);
//var aPause : boolean; var aSkiped : boolean);
begin
  ProgressBar2.Max:=aTotal;
  ProgressBar2.Position:=aProgress;
  lb_Progress2.Caption:=FormatProgress (aProgress, aTotal);

  aTerminated:=Terminated;
  Application.ProcessMessages;
end;


// TODO: ShowDetails
//procedure Tdlg_Progress.ShowDetails (aValue: boolean);
//begin
// (* FShowDetails:=aValue;
//
//RichEdit1.Visible:=aValue;
//
//case aValue of
//  True:  begin
//        //   BorderStyle:=bsSizeable;
//           Height:=4 * DEF_HEIGHT;
//           btn_Details.Action:=act_Details_hide;
//         end;
//  False: begin
//           Height:=DEF_HEIGHT;
//
//           //BorderStyle:=bsSingle;
//         //  Height:=pn_Top.Height;
//           btn_Details.Action:=act_Details_show;
//         end;
//end;*)
//end;



procedure Tdlg_Progress.FormDestroy(Sender: TObject);
begin
  Screen.Cursor:=crDefault;

end;


procedure Tdlg_Progress.DoActions(Sender: TObject);
begin
  if Sender=act_Stop then
  begin
    FTerminated:=true;
    Application.ProcessMessages;

  end else

  if Sender=act_Close then
  begin
    Close;
  end else ;


(*  if Sender=act_Details_show then begin
    ShowDetails (True);
  end else

  if Sender=act_Details_hide then begin
    ShowDetails (False);
  end;*)

//
end;


procedure Tdlg_Progress.DoOnProgressMsg(aMsg: string);
begin
  lb_Msg1.Caption:=aMsg;
end;

procedure Tdlg_Progress.DoOnProgressMsg2(aMsg: string);
begin
  lb_Msg2.Caption:=aMsg;

end;


procedure Tdlg_Progress.DoOnCaption(aMsg: string);
begin
  Caption:=aMsg;

end;

procedure Tdlg_Progress.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;

end;


procedure Tdlg_Progress.FormHide(Sender: TObject);
begin
  Screen.Cursor:=crDefault;
end;

function Tdlg_Progress.GetTerminated: Boolean;
begin
  Result := FTerminated;
end;

procedure Tdlg_Progress.Progress1(aProgress,aTotal: integer);
begin
  DoOnProgress1 (aProgress,aTotal,FTerminated);
end;


procedure Tdlg_Progress.Progress2(aProgress,aTotal: integer);
begin
  DoOnProgress2 (aProgress,aTotal,FTerminated);
end;


procedure Tdlg_Progress.SetTerminated(const Value: Boolean);
begin
  FTerminated:=Value;
end;


procedure Tdlg_Progress.ProgressMsg(aMsg: string);
begin
  DoOnProgressMsg(aMsg);
end;

procedure Tdlg_Progress.ProgressMsg2(aMsg: string);
begin
  DoOnProgressMsg2(aMsg);
end;



// ---------------------------------------------------------------
function Progress_ExecDlg(aCaption: string; aOnStartEvent: TProcedureEvent):
    Boolean;
// ---------------------------------------------------------------
begin
  Application.CreateForm(Tdlg_Progress, dlg_Progress);

  if aCaption='' then aCaption:='Выполнение';

  dlg_Progress.Caption:=aCaption;

//  dlg_Progress:=Tdlg_Progress.CreateForm(aCaption);
  dlg_Progress.OnStart:=aOnStartEvent;
  dlg_Progress.ShowModal;

  Result := not dlg_Progress.Terminated;

  FreeAndNil(dlg_Progress);
end;



// ---------------------------------------------------------------
function Progress_Show(aCaption: string): Boolean;
// ---------------------------------------------------------------
begin
  Assert (not Assigned(dlg_Progress));

  Application.CreateForm(Tdlg_Progress, dlg_Progress);

  if aCaption='' then aCaption:='Выполнение';

  dlg_Progress.Caption:=aCaption;
  dlg_Progress.FormStyle:=fsStayOnTop;

//  dlg_Progress:=Tdlg_Progress.CreateForm(aCaption);
//  dlg_Progress.OnStart:=aOnStartEvent;
  dlg_Progress.Show;

//  Result := not dlg_Progress.Terminated;

//  FreeAndNil(dlg_Progress);
end;

// ---------------------------------------------------------------
procedure Progress_Free;
// ---------------------------------------------------------------
begin
  FreeAndNil(dlg_Progress);
end;



end.
    

 

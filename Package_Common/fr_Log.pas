unit fr_Log;

interface

uses
  Classes, Controls, Forms,
  ComCtrls,


  u_log, rxPlacemnt, StdCtrls

  ;

type
  Tframe_Log = class(TForm)
    RichEdit1: TRichEdit;
    FormPlacement1: TFormPlacement;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    class procedure ShowForm;

  end;

(*var
  frame_Log: Tframe_Log;
*)

implementation

//uses fr_Log;
{$R *.dfm}

var
  Fframe_Log: Tframe_Log;



procedure Tframe_Log.FormCreate(Sender: TObject);
begin
  RichEdit1.Align:=alClient;

//  g_Log.OnLog:=DoOnLog;
  g_Log.RichEdit  := RichEdit1;

//  RichEdit1.Lines.Text := g_Log.Text;
end;

procedure Tframe_Log.FormDestroy(Sender: TObject);
begin
  g_Log.RichEdit:=nil;
//  g_Log.OnLog:=nil;

end;

class procedure Tframe_Log.ShowForm;
begin
  if not Assigned(Fframe_Log) then
    Application.CreateForm(Tframe_Log, Fframe_Log);

  Fframe_Log.Show;

end;


end.


//-------------------------------------------------------------------
//procedure Tframe_Log.DoOnLog(Sender: TObject; aMsg: string; aMsgType: TlogMsgType);
//-------------------------------------------------------------------
//begin
//  case aMsgType of
//    lmtSystemInfo: RichEdit1.SelAttributes.Color:=DEF_SYSMSG_COLOR; // Exit; //!!!!!!!!!!!!!!!!!!
//
//    lmtError,
//    lmtException:  RichEdit1.SelAttributes.Color:=LOG_ERROR_COLOR;
//
//  else
//    RichEdit1.SelAttributes.Color:=Font.Color;
//  end;
//
//
//  if aMsgType<>lmtAppend then
//  begin
//    RichEdit1.Lines.Add (aMsg);
//    SendMessage (RichEdit1.Handle, EM_LINESCROLL, 0, 1);
//  end
//  else
//    with RichEdit1 do
//      Lines[Lines.Count-1]:=Lines[Lines.Count-1]+ aMsg;
//
//
//  dlg_ScrollMemo (RichEdit1.Handle);
//
//end;


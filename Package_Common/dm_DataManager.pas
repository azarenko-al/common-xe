unit dm_DataManager;

interface

uses
  SysUtils, Classes, ADODB, DB, Forms, Variants,

 // u_log,

  u_func,
  u_db
//  u_const_db

  ;


type
  TdmDataManager = class(TDataModule)
    ADOQuery1: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    ADOCommand1: TADOCommand;
  private
    FADOConnectionRef :  TADOConnection;

    procedure Log(aMsg: string);
    procedure SetADOConnection(const Value: TADOConnection);
  public
    function OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName: string;
        aParams: array of TDBParamRec): integer; overload;

  public
    function ExecCommand(aSQL: string; aParams: array of TDBParamRec): Boolean;
    function ExecCommand_Update(aTableName, aKeyFieldName: string; aParams: array
        of TDBParamRec; aKeyValue: string): boolean;
    class procedure Init;
        
    property ADOConnection: TADOConnection read FADOConnectionRef write
        SetADOConnection;
  end;


var
  dmDataManager: TdmDataManager;



implementation
{$R *.dfm}


class procedure TdmDataManager.Init;
begin
  Assert(not Assigned(dmDataManager));

  Application.CreateForm(TdmDataManager, dmDataManager);
end;


//--------------------------------------------------------------------
function TdmDataManager.ExecCommand(aSQL: string; aParams: array of
    TDBParamRec): boolean;
//--------------------------------------------------------------------
begin
  result :=db_ExecCommand (ADOConnection, aSQL, aParams);
end;

function TdmDataManager.ExecCommand_Update(aTableName, aKeyFieldName: string;
    aParams: array of TDBParamRec; aKeyValue: string): boolean;
var
  s: string;
begin
//  s:= Format('UPDATE %s '  , []);

 // Result := ExecCommand
end;



procedure TdmDataManager.Log(aMsg: string);
begin
  //g_Log.SysMsg (aMsg);
end;

// ---------------------------------------------------------------
function TdmDataManager.OpenStoredProc(aADOStoredProc: TADOStoredProc; aProcName:
    string; aParams: array of TDBParamRec): integer;
begin
  Assert(Assigned(ADOStoredProc1), 'Value not assigned');

  aADOStoredProc.Connection := FADOConnectionRef;

  Result := db_ExecStoredProc__(aADOStoredProc, aProcName, aParams, True);
end;


// ---------------------------------------------------------------
procedure TdmDataManager.SetADOConnection(const Value: TADOConnection);
begin
  FADOConnectionRef := Value;

  ADOQuery1.Connection      :=FADOConnectionRef;
  ADOStoredProc1.Connection :=FADOConnectionRef;
  ADOCommand1.Connection    :=FADOConnectionRef;

end;


















end.


unit u_VirtFile;

interface

uses Windows,SysUtils,Dialogs
     ;

type

  //----------------------------------------
  TVirtualFile = class
  //----------------------------------------
  private                   
     FPageSize  : int64;
     FHMapping  : THandle;
     FBufferSize: integer;
     FPageFileOffset: integer;
     FData      : Pointer;

     FOpenMode : LongWord ;

     function SetFileMapPage (aPageOffset : integer) : boolean;
     procedure SetOpenMode(Value: LongWord);
  protected
  public
     Active1: boolean;

     FileSize : Cardinal;
     FileName : string;

     HeaderLen1: integer; //����� ���������
     RecordLen1: integer; //����� ������ ��� ����� �� �������

     constructor Create;
     destructor Destroy; override;

     function Open(aFileName: string; aOpenMode: byte=fmOpenRead): boolean;
     procedure Close ();

     function GetRecordCount: integer;
     function ReadBuffer(aFrom, aLen: int64; aBuffer: pointer): Boolean;
//     function ReadBuffer(aFrom, aLen: Integer; var aBuffer): Boolean;

     function ReadHeader (var aBuffer): boolean;

     function ReadRecord(aIndex: int64; aBuffer: Pointer): boolean;
//     function ReadRecord (aIndex: integer; var aBuffer): boolean;

   //  procedure SaveToTxtFile(aFileName: string);

     function WriteRecord(aIndex: int64; aBuffer: Pointer): boolean;

//     property OpenMode: LongWord read FOpenMode write SetOpenMode;

  end;



//===============================================================
implementation
//===============================================================

//  function GetFileSize_ (aFileName: string): Cardinal; forward;
//  function FileDateTime (const FileName: string): TDateTime; forward;
  function GetLastErrorMessage (aErrorCode: integer): string; forward;

{
file management routines

const fmOpenRead       = $0000;

const fmOpenWrite      = $0001;
const fmOpenReadWrite  = $0002;
const fmShareCompat    = $0000;
const fmShareExclusive = $0010;
const fmShareDenyWrite = $0020;
const fmShareDenyRead  = $0030;
const fmShareDenyNone  = $0040;
}



//---------------------------------------------------------------
// TVirtualFile
//---------------------------------------------------------------
constructor TVirtualFile.Create;
//---------------------------------------------------------------
var si: SYSTEM_INFO;
begin
  inherited;

  GetSystemInfo(si);
  FPageSize := si.dwAllocationGranularity * 5;  //64k x 20 = 1.2 mb

  FOpenMode:=fmOpenRead;
end;



destructor TVirtualFile.Destroy;
begin
  Close();
  inherited;
end;


function TVirtualFile.GetRecordCount: integer;
begin
  if RecordLen1>0
    then Result:=(FileSize - HeaderLen1) div RecordLen1
    else Result:=0;
end;


//-------------------------------------------------------------------
function TVirtualFile.ReadHeader (var aBuffer): boolean;
//-------------------------------------------------------------------
var src: Pointer;
begin
  if not Active1 then
  begin
    Result:=False; Exit;
  end;

  SetFileMapPage(0);

  src :=Pointer(Longint(FData)); // + HeaderLen + Index*RecordLen);
  Move (src^, aBuffer, HeaderLen1);
  Result:=true;
end;



//-------------------------------------------------------------------
function TVirtualFile.ReadBuffer(aFrom, aLen: int64; aBuffer: pointer):
    Boolean;
//-------------------------------------------------------------------
// aLen - data len
var
  iInPageOffs,iPageOffs: int64;
  iLen1, iLen2: Integer;
  src,src1,src2: Pointer;
  dest: Pointer;

  p,p1: Pointer;
begin

  if (not Active1) then
  begin
    raise Exception.Create('');
    Result:=False; Exit;
  end;

 // iOffs := HeaderLen + aIndex*RecordLen;
  iPageOffs := FPageSize * Trunc(aFrom div FPageSize);

  if (FPageFileOffset<>iPageOffs) then
    SetFileMapPage(iPageOffs);

  if FData = nil then
  begin
    Result:=False;
    Exit;
  end;

  //�������� ������ � �������� ��������
  iInPageOffs := aFrom - iPageOffs ;

  try
    if iInPageOffs + aLen -1< FPageSize then   //if iInPageOffs + aLen < FPageSize then
    begin
      src := Pointer(Longint(FData) + iInPageOffs);
      Move (src^, aBuffer^, aLen);
    end else
    begin
      GetMem(p, aLen);

      iLen1:=FPageSize - iInPageOffs;  // ������ ����� ������
      iLen2:=aLen - iLen1;              // ������ �����

      src1 := Pointer(Longint(FData) + iInPageOffs);
      Move (src1^, p^, iLen1);

      SetFileMapPage(iPageOffs+FPageSize);
      src2  := Pointer(Integer(FData) + 0);

      p1  := Pointer(Integer(p) + iLen1);
      Move (src2^, p1^, iLen2);

      Move (p^, aBuffer^, aLen);

      FreeMem(p);

    end;

  except

  end;

  Result:=true;
end;



//---------------------------------------------------------------
function TVirtualFile.ReadRecord(aIndex: int64; aBuffer: Pointer): boolean;
//---------------------------------------------------------------
//var
//  src: Pointer;
 // iOffs: integer;
 // iLen1, iLen2, iInPageOffs, iPageOffs : integer;
begin
{  if not Active1 then begin
    raise Exception.Create('not Active1');
    Result:=False; Exit;
  end;
}

  Assert (RecordLen1>0, RecordLen1.ToString);

  try
    Result := ReadBuffer(HeaderLen1 + aIndex*RecordLen1, RecordLen1, aBuffer);

  except on E: Exception do
  end;
end;


//---------------------------------------------------------------
function TVirtualFile.Open(aFileName: string; aOpenMode: byte=fmOpenRead): boolean;
//---------------------------------------------------------------
var
  hFile: integer;

begin
  if Active1 then //begin
    Close;
//    Result:=true; Exit;
 // end;

  if not FileExists(aFileName) then begin
    Result:=False; Exit;
  end;

  if aFileName<>'' then
   FileName:=aFileName;

//  FileDate:=FileDateTime(FileName);
//  FileSize:=GetFileSize (FileName);

  // virt file should be not 0 size
//  if FileSize=0 then
//  begin
//    raise Exception.Create('FileSize=0');
//    Exit;
//  end;

  Active1:=false;
  Result:=Active1;
  if not FileExists(FileName) then Exit;

   //��������� ����

  FOpenMode:=aOpenMode;

  case FOpenMode of
    fmOpenRead:
      hFile:=FileOpen (PChar(FileName), fmShareDenyNone); // fmOpenRead

    fmOpenReadWrite:
      hFile:=FileOpen (PChar(FileName), fmOpenReadWrite);//fmShareDenyNone); //fmOpenReadWrite);//
  end;
  {
   if FOpenMode = fmOpenRead then begin
 //    ShowMessage('hFile:=FileOpen (PChar(FileName), fmShareDenyWrite)');
   end
   else begin
 //    ShowMessage('hFile:=FileOpen (PChar(FileName), fmShareDenyNone);');

   end;    }

  if (hFile < 0) then begin
    raise Exception.Create ('Error open file '''+aFileName+'''');
    Exit;
  end;

  FileSize:=Windows.GetFileSize(hFile,nil);
  if FileSize=0 then begin
     try
       raise Exception.Create ('FileSize=0');
     except end;
     CloseHandle(hFile);
     Exit;
  end;


   {������� ������-������������ ����}
  case FOpenMode of
    fmOpenRead:      FHMapping:=CreateFileMapping (hFile, nil,PAGE_READONLY, 0, 0, nil);
    fmOpenReadWrite: FHMapping:=CreateFileMapping (hFile, nil,PAGE_READWRITE, 0, 0, nil);
  end;


  CloseHandle(hFile);
  if FHMapping=0 then begin
    try
       raise Exception.Create ('can''t create file mapping');
    except end;
      Exit;
  end;


  FPageFileOffset:=-1;

  SetFileMapPage (0);


  Active1:=true;
  Result:=Active1;
end;

//---------------------------------------------------------------
function TVirtualFile.SetFileMapPage (aPageOffset : integer) : boolean;
//---------------------------------------------------------------
var
  s: string;
  Data1    : Pointer;
  iPageSize : integer;
begin
{  if not Active1 then begin
    Result:=False; Exit;
  end;
}
  if FPageFileOffset = aPageOffset then
    Exit;

  s:=FileName;


  Result := True;

  if Assigned(FData) then begin
    UnMapViewOfFile(FData);
  end;

  if (FileSize-aPageOffset) < FPageSize  then
    FBufferSize := FileSize - aPageOffset
  else
    FBufferSize := FPageSize;

  // ���������� ���������� ������ � �������� ������������

  //FData := MapViewOfFile (FHMapping, FILE_MAP_READ, 0, aPageOffset, FBufferSize);
//  FData := MapViewOfFile (FHMapping, FILE_MAP_ALL_ACCESS {FILE_MAP_WRITE} {FILE_MAP_READ}, 0, aPageOffset, FBufferSize);
  case FOpenMode of
    fmOpenRead:
      FData := MapViewOfFile (FHMapping, FILE_MAP_READ , 0, aPageOffset, FBufferSize);

    fmOpenReadWrite:
      FData := MapViewOfFile (FHMapping, FILE_MAP_ALL_ACCESS , 0, aPageOffset, FBufferSize);
  //  else
   //   raise Exception.Create('');
  end;


  Assert (FData<>nil);

  if Assigned(FData) then
    FPageFileOffset:=aPageOffset;


 // FILE_MAP_READ
//  Data1 := MapViewOfFile (FHMapping, FILE_MAP_COPY, dwFileOffsetHigh, dwFileOffsetLow, iPageSize+1);
  // �������� ��������� �� ������� ������, � ������� ������������ ����


end;

//---------------------------------------------------------------
procedure TVirtualFile.Close;
//---------------------------------------------------------------
begin
  if Active1 then
  begin
    if Assigned(FData) then begin
      UnMapViewOfFile(FData);
      FData:=nil;
    end;

    CloseHandle(FHMapping);
    Active1:=false;
  end;
end;



procedure TVirtualFile.SetOpenMode(Value: LongWord);
begin
  FOpenMode:=Value;

  if not FOpenMode in [fmOpenRead,fmOpenReadWrite] then
    ShowMessage('procedure TVirtualFile.SetOpenMode(Value: LongWord)');

end;



function GetLastErrorMessage (aErrorCode: integer): string;
var
  FErrorString: PChar;
begin
  FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
                 nil, aErrorCode, LANG_USER_DEFAULT, @FErrorString, 0, nil  );
  Result:=StrPas (FErrorString);
end;



//---------------------------------------------------------------
function TVirtualFile.WriteRecord(aIndex: int64; aBuffer: Pointer): boolean;
//---------------------------------------------------------------
var
  p,src,dest: Pointer;
  i,iLen1, iLen2, iOffs, iInPageOffs, iPageOffs : integer;

begin
  Assert(FOpenMode=fmOpenReadWrite, 'function TVirtualFile.WriteRecord (aIndex:integer; var aBuffer): boolean;');

  if not Active1 then begin
    Result:=False; Exit;
  end;

  Assert (RecordLen1<>0, 'RecordLen<>0');

  iOffs := HeaderLen1 + aIndex*RecordLen1;
  iPageOffs := FPageSize * Trunc(iOffs div FPageSize);

 // tmp:=aBuffer;

  if (FPageFileOffset<>iPageOffs) then
    SetFileMapPage(iPageOffs);

  iInPageOffs := iOffs - iPageOffs ;

  if iInPageOffs + RecordLen1 < FPageSize then
  begin
     dest := Pointer(Longint(FData) + iInPageOffs);
     Move (aBuffer^, dest^,  RecordLen1);
  end else

  begin
     iLen1:=FPageSize - iInPageOffs;
     iLen2:=RecordLen1 - iLen1;

     dest := Pointer(Longint(FData) + iInPageOffs);
     Move (aBuffer^, dest^,  iLen1);

     SetFileMapPage(iPageOffs+FPageSize);
//     src  := Pointer(aBuffer) + iLen1;

{     p:=Pointer(aBuffer);
     i:=Longint(Pointer(aBuffer));
     i:=Longint(aBuffer);
}
     src  := Pointer(Longint(aBuffer) + iLen1);
//     src  := Pointer(Longint(Pointer(aBuffer)) + iLen1);
     dest := Pointer(Longint(FData) + 0);

     Move (src^ ,dest^,  iLen2);
  end;

  Result:=true;
end;


end.






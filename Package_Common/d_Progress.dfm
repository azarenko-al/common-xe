object dlg_Progress: Tdlg_Progress
  Left = 1536
  Top = 339
  Cursor = crAppStart
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 139
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020040000000000E80200001600000028000000200000004000
    0000010004000000000000020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000007777888888888800000
    00000000000007777888888008F0000000000000000008777888888008F00000
    000000C0000008778888877008F0000000CCCCCCC00007778888777888F00000
    0C0000C0000008788888888888F000000C0000000000088888CCC88888800000
    0C00000000000FFF88CFC999999000000C0000000000088FF8CCC8888F800000
    0C00000000000888FFF987788FF0000000000000000008888FF9877778800000
    000000000000088888F9888778F0000000000000000008888889888777F00000
    00008880000007788889F8877780000000088888000007788889F88777F00000
    08888008F00007788889F8FFFFF0000778888008880000000000000000000777
    7788888888900000000000000000007777888888998F00000000000000000008
    778888998877F000000000000000000088888C888877FF000000000000000000
    08888C98888777F0000000000000000000FFFCF9888777FF0000000000000000
    00088CFF988FFF00000000000000000000008C88F9FF00000000000000000000
    00000C788F000000000000000000000000000C77000000000000000000000000
    00000C0000000000000000000000000000000C00000000000000000000000000
    00000C0000000000000000000000000000000C0000000000000000000000FFFF
    0000FFFF0000FFFF0000FFFF0000FFDF0000FC070000FBDF0000FBFF0000FBFF
    0000FBFF0000FBFF0000FFFF0000FFFF0000FF9F0000FE0F0000F8070000E003
    0000800100000000FFFF80007FFFC0003FFFE0001FFFF0000FFFF80007FFFC00
    0FFFFE003FFFFF00FFFFFF83FFFFFF8FFFFFFFBFFFFFFFBFFFFFFFBFFFFF}
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  DesignSize = (
    427
    139)
  PixelsPerInch = 96
  TextHeight = 14
  object lb_Msg1: TLabel
    Left = 5
    Top = 5
    Width = 40
    Height = 18
    Caption = 'lb_Msg1'
  end
  object lb_Progress1: TLabel
    Left = 358
    Top = 5
    Width = 58
    Height = 18
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    Caption = 'lb_Progress'
    ExplicitLeft = 345
  end
  object lb_Progress2: TLabel
    Left = 350
    Top = 41
    Width = 64
    Height = 18
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    Caption = 'lb_Progress2'
    ExplicitLeft = 337
  end
  object lb_Msg2: TLabel
    Left = 5
    Top = 45
    Width = 40
    Height = 18
    Caption = 'lb_Msg2'
  end
  object btn_Stop: TButton
    Left = 170
    Top = 95
    Width = 83
    Height = 25
    Cursor = crAppStart
    Action = act_Stop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object ProgressBar1: TProgressBar
    Left = 5
    Top = 20
    Width = 413
    Height = 20
    Cursor = crAppStart
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    ExplicitWidth = 400
  end
  object ProgressBar2: TProgressBar
    Left = 5
    Top = 60
    Width = 413
    Height = 20
    Cursor = crAppStart
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    ExplicitWidth = 400
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 28
    Top = 88
    object act_Stop: TAction
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      OnExecute = DoActions
    end
    object act_Close: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      OnExecute = DoActions
    end
  end
end

unit u_files;

interface
uses
  rxFileUtil,
  ShlObj, ShellAPI, IOUtils, Windows,SysUtils, Vcl.Forms,
  IdGlobalProtocols,

  //  ShlObj,
  
 // ShellAPI,
//  IOUtils.TFile.WriteAllText(aFileName, s);

  u_func,

  System.Classes, System.Types

  ;

//procedure ShellExec_Notepad_temp(aText: string; aExt: string = '');
//procedure ShellExec_Notepad_temp(aText: string; aExt: string = '');

//function GetApplicationFileDateAsStr: string;

//  function ScanDir1(aPath, aExtension: string; aFiles: TStrings): Integer;


//  function dlg_BrowseDirectory(var AFolderName: string; aDlgText: string): Boolean;

  function GetApplicationDir (): string;
  function CorrectFileName(aFileName: string): string;

//  function GetTempFileDir (): string;
//  function GetTempFileNameWithExt(aExt: string): string;
//  function GetTempXmlFileName (): string;

//  procedure FindFilesByMask_11(aDir,aMask: string; aList: Tstrings; aNeedClear:
///      boolean);

//  procedure FindFilesByMaskEx(const aPath, aExtensions: string; var aFiles: TStringList);


  //---------------------------------------------------
  // Directories
  //---------------------------------------------------

//  procedure FindDirectoriesWithoutChilds(aDir: ShortString; var aList: TStringList);
//  procedure FindDirectories (aDir: string; aList: Tstrings);

  procedure ClearDir(aPath: string);
//  function  ClearDirRecursive  (aPath: string): Boolean;

//  procedure ForceDirectories   (aDir: string);
  procedure ForceDirByFileName (aFileName: string);

 // procedure RemoveEmptyDir11111111111(aDirList: TstringList);

//  procedure DirectoryCopy (aSrcDir,aDestDir: string);

  //---------------------------------------------------
  // shell functions
  //---------------------------------------------------
  function ExtractFileNameNoExt (aFileName: string): string;

//  procedure MoveFile (aFileName, aDestName: string);

  function GetFileSize_(aFileName: string): Int64;

  function  GetFileDate (aFileName: string): TDateTime;

  //FileUtil

//  procedure ShellExec       (aFileName,aFileParams: string);
                  

//  function  IsDirEmpty      (aDir: string): boolean; export;
//  procedure BuildFileList11111(aFileMask: string; astrings: Tstrings);
//  procedure BuildFileNameNoExtList (aFileMask:  string; astrings: Tstrings);

  function  GetParentFileDir       (aFileName: string): string;
//  function ChangeParentFileDir1111(aFileName,aNewDir: string): string;

//  procedure FileCopy_Src_Dest(aSrcFileName, aDestFileName: string);

  procedure StrToTxtFile(aStr: string; aFileName: string);
  function TxtFileToStr(aFileName: string): string;

//  function PathIsLocal11111111111(aFileName: string): boolean;

//  function GetFileExt(aFileName: string): string;

 // function GetCommonApplicationDataDir_Local_AppData(aApplicationName: string):
  //    string;

//  function RunApp(aFileName, aParamStr: string): boolean;

  function GetFileModifyDate(aFileName: string): TDateTime;

  //function mapx_FileSize(aFileName: string): int64;

function GetParentFolderName_v1(aFileName: string): string;

function GetCommonApplicationDataDir_Local_AppData(aApplicationName: string):
    string;

function ScanDir(aPath, aExtension: string; aFiles: TStrings = nil):
    TStringDynArray;

//  function ScanDir1(aPath, aExtensions: string ; var aFiles: TStringList):
  //    Integer;

//function GetSpecialFolderPath_AppData: string;

//function GetSpecialFolderPath_MyDocuments: string;

//procedure Dir_delele(const aDir: String);

//procedure FileCopy(aSrcFileName, aDestFileName: string);



implementation


//uses
//   Vcl.Forms;


//function SysGetTempFileName(aExt: string = '.tmp'): string; forward;

//---------------------------------------------------------------
function GetFileSize_(aFileName: string): Int64;
//---------------------------------------------------------------
begin
//  Result := rxFileUtil.GetFileSize(aFileName);

//uses IdGlobalProtocols;
//function FileSizeByName(const AFilename: TIdFileName): Int64;

  Result := FileSizeByName(aFileName);
  if Result<0 then
    Result:=0;

end;





procedure StrToTxtFile(aStr: string; aFileName: string);
var list: TstringList;
begin
  ForceDirByFileName(aFileName);

  list:=TstringList.Create;
  list.Text:=aStr;
  list.SaveToFile (aFileName);
  list.Free;
end;




function TxtFileToStr(aFileName: string): string;
var oList: TstringList;
begin
  oList:=TstringList.Create;
  oList.LoadFromFile (aFileName);
  Result:=oList.Text;
  oList.Free;
end;


//-----------------------------------------------------------------------
function ScanDir(aPath, aExtension: string; aFiles: TStrings = nil):
    TStringDynArray;
//-----------------------------------------------------------------------
//  ScanDir1(aDir, '*.mdb', oFiles);
var
  sPath : string;
begin

  if not TDirectory.Exists(IncludeTrailingBackslash(aPath)) then
    Exit;

  Result:=TDirectory.GetFiles (IncludeTrailingBackslash(aPath) , aExtension, TSearchOption.soAllDirectories) ;




//  TDirectory.Getf


//  TDirectory.GetFiles('C:\', '*.dll', TSearchOption.soAllDirectories);

  if assigned(aFiles) then
  begin
    aFiles.Clear;

    for sPath in Result  do
      aFiles.Add(sPath);{assuming OpenPictureDialog1 is the name you gave to your OpenPictureDialog control}

  end;

end;





// ---------------------------------------------------------------
function GetSpecialFolderPath_AppData: string;
// ---------------------------------------------------------------
const
  CSIDL_LOCAL_APPDATA  = $001c;  // <user name>\Local Settings\Applicaiton Data (non roaming)

var
  r: Bool;
  sPath: array[0..MAX_PATH] of Char;

begin
//   CSIDL_CSIDL_APPDATA

  r := ShGetSpecialFolderPath(0, sPath, CSIDL_LOCAL_APPDATA, False) ;
//  r := ShGetSpecialFolderPath(0, sPath, CSIDL_APPDATA, False) ;
  if not r then
    raise Exception.Create('Could not find APPDATA folder location.') ;

  Result := IncludeTrailingBackslash( sPath);
end;



//-------------------------------------------------------------------
function GetCommonApplicationDataDir_Local_AppData(aApplicationName: string):
    string;
//-------------------------------------------------------------------
//var
//  sAppName : string;
begin
  Assert(aApplicationName<>'', 'GetCommonApplicationDataDir_Local_AppData - aApplicationName');

  Result:= GetSpecialFolderPath_AppData();

  Result:= IncludeTrailingBackslash(Result)+ aApplicationName+ '\';

 // exit;

  ///////////////////////////
   {
   with TRegistry.Create do
    try
      RootKey := HKEY_CURRENT_USER;

//        REGSTR_PATH_SPECIAL_FOLDERS   = REGSTR_PATH_EXPLORER + '\Shell Folders';

//      if OpenKey('Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders\', False) then

      if OpenKey(REGSTR_PATH_SPECIAL_FOLDERS, False) then
         Result :=ReadString('Local AppData');
      CloseKey;

    finally
      Free;
    end;


  Assert(Result<>'');

  Result:= IncludeTrailingBackslash(Result)+ aApplicationName+ '\';
   }
end;



function ExtractFileNameNoExt (aFileName: string): string;
begin
  Result:=stringReplace (ExtractFileName(aFileName), ExtractFileExt(aFileName), '', [rfReplaceAll]);
end;


function GetParentFileDir (aFileName: string): string;
begin
  Result:=IncludeTrailingBackslash(ExtractFileDir (ExcludeTrailingBackslash(aFileName)));
end;




function GetParentFolderName_v1(aFileName: string): string;
//var
//  s: string;
begin
//  s:= (ExtractFileDir (aFileName));
//  s:= ExcludeTrailingBackslash (ExtractFileDir (aFileName));


  Result:= ExtractFileName ( ExcludeTrailingBackslash (ExtractFileDir (aFileName)));

//  Result:= IncludeTrailingBackslash(Result);
end;


procedure ForceDirByFileName (aFileName: string);
begin
  ForceDirectories (ExtractFileDir (aFileName));
end;


function GetApplicationDir (): string;
begin
  Result:=IncludeTrailingBackslash(ExtractFileDir(Application.ExeName));
end;



function GetFileDate (aFileName: string): TDateTime;
begin
  Result :=  rxFileUtil.FileDateTime (aFileName);
end;


//-------------------------------------------------------------------
procedure ClearDir(aPath: string);
//-------------------------------------------------------------------
begin
 // Result:=
  aPath:=IncludeTrailingBackslash(aPath);

  rxFileUtil.ClearDir (aPath, True);

//procedure TForm.Remove(Dir: String);
//var
//  Result: TSearchRec; Found: Boolean;
//begin
//  Found := False;
//  if FindFirst(Dir + '\*', faAnyFile, Result) = 0 then
//    while not Found do begin
//      if (Result.Attr and faDirectory = faDirectory) AND (Result.Name <> '.') AND (Result.Name <> '..') then Remove(Dir + '\' + Result.Name)
//      else if (Result.Attr and faAnyFile <> faDirectory) then DeleteFile(Dir + '\' + Result.Name);
//      Found := FindNext(Result) <> 0;
//    end;
//  FindClose(Result); RemoveDir(Dir);
//end;


 // TDirectory.Delete(aPath) ;

//  Result:=True;

 // Result:=rxFileUtil.ClearDir (aPath, True);

//  Result:=rxFileUtil.ClearDir (aPath, False);

//  Result:=rxFileUtil.DeleteFiles (IncludeTrailingBackslash(aPath) + '*.*');

//  Result:=rxFileUtil.ClearDir (aPath, False);
end;



// ---------------------------------------------------------------
function CorrectFileName(aFileName: string): string;
// ---------------------------------------------------------------
//remove . , : -> _
// ---------------------------------------------------------------

  function ReplaceStr (Value, OldPattern, NewPattern: string): string;
  begin
    Result:=stringReplace (Value, OldPattern, NewPattern, [rfReplaceAll, rfIgnoreCase]);
  end;


begin
  Result := ReplaceStr(aFileName, '/',' ');
  Result := ReplaceStr(Result,    '\',' ');
  Result := ReplaceStr(Result,    '.',' ');
  Result := ReplaceStr(Result,    ':',' ');
  Result := ReplaceStr(Result,    '>',' ');
  Result := ReplaceStr(Result,    '<',' ');
  Result := ReplaceStr(Result,    ',',' ');
end;


//function GetFileExt(aFileName: string): string;
//begin
//  Result := ExtractFileExt (aFileName);
//end;



//------------------------------------------------------
function  mapx_FileExists (aFileName: string): boolean;
//------------------------------------------------------
begin
  Result:=
    (FileExists(ChangeFileExt (aFileName, '.dat')) and
     FileExists(ChangeFileExt (aFileName, '.id'))  and
     FileExists(ChangeFileExt (aFileName, '.map'))  and
     FileExists(ChangeFileExt (aFileName, '.tab'))
    )

    or

    (FileExists(ChangeFileExt (aFileName, '.tif')) and
     FileExists(ChangeFileExt (aFileName, '.tab'))
    )


end;


// ---------------------------------------------------------------
function GetFileModifyDate(aFileName: string): TDateTime;
// ---------------------------------------------------------------
var
  SR: TSearchRec;
  r : integer;
  LocalTime : tFileTime;
  DOSFileTime : DWord;
begin
  Result := 0;
  if not FileExists(aFileName) then exit;

  r := FindFirst(aFileName, faAnyFile, SR);

  FileTimeToLocalFileTime(SR.FindData.ftLastWriteTime, LocalTime); // Compensate for time zone
  FileTimeToDosDateTime(LocalTime, LongRec(DOSFileTime).Hi,
     LongRec(DOSFileTime).Lo);
  Result := FileDateToDateTime(DOSFileTime);
end;





end.



(*

function ReplaceInvalidFileNameChars(const aFileName: string; const aReplaceWith: Char = '_'): string;
var
  i: integer;
begin
  Result := aFileName;
  for i := Low(Result) to High(Result) do
    if not TPath.IsValidFileNameChar(Result[i]) then
      Result[i] := aReplaceWith;
  end;
end;




{


var
  oStrList: TStringList;
  aFileDir: string;
begin
  aFileDir :='C:\Onega\Maps\Maps';


  oStrList:=TStringList.Create;

  ScanDir1(aFileDir, '*.tab;*.id',  oStrList);

//  FindFilesByMaskEx (aFileDir, '.rlf2;.ter;.hgt',  oStrList);
 //// FindFilesByMaskEx (aFileDir, '.tab',  oStrList);

  ShellExec_Notepad_temp (oStrList.Text);


  FreeAndNil(oStrList);


end.

{



var
  oStrList: TStringList;
  aFileDir: string;
begin

  oStrList:=TStringList.Create;

  aFileDir := 'e:\gis';


{ TODO : reduce }
  // -------------------------------------------------------------------
//  FindFilesByMaskEx (aFileDir, '.rlf;.ter;.hgt',  oStrList);
  FindFilesByMaskEx (aFileDir, '.rlf2;.ter;.hgt',  oStrList);


                  //    '.rlf;.ter;.hgt', oStrList);
// ---------------------------------------------------------------

// Sample call:
//FileSearchEx('C:\Temp', '.txt;.tmp;.exe;.doc', FileList);
   //    FindFilesByMaskEx (aFileDir, '.rlf;.ter;.hgt',  oStrList);//, False);




//  FindFilesByMask_ (aFileDir, '*.ter',  oStrList, False);
//  FindFilesByMask_ (aFileDir, '*.hgt',  oStrList, False);
//  FindFilesByMask_ (aFil
 // FindFilesByMaskEx(aFileDir, eDir, '*.zip',  oStrList, False);
  // -------------------------------------------------------------------

  ShellExec_Notepad_temp(oStrList.Text);

//  oStrList.Free;

  FreeAndNil(oStrList);


{
begin
//  Randomize;
}
(*
var
  s : string;
begin
  s := GetCommonApplicationDataDir_Local_AppData('rpls');

  s := '';*)



//begin

end.




(*



*)

(*
//-------------------------------------------------------------------
procedure FindDirectories (aDir: string; aList: Tstrings);
//-------------------------------------------------------------------
  procedure DoFind (aDir: string);
  var
    rInfoRec: TSearchRec;
    s       : string;
  begin
    aDir:=IncludeTrailingBackslash(aDir);

    if FindFirst(aDir + '*.*',faDirectory,rInfoRec)=0 then
    begin
      repeat
        s:=rInfoRec.Name;
        if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)>0)
          then begin
            aList.Add(aDir+s);
            DoFind (aDir+s);
          end;
      until
         FindNext(rInfoRec)<>0;

      SysUtils.FindClose(rInfoRec);
    end;

  end;


begin
  aList.Clear;

  DoFind (aDir);
end;

*)


(*
// -------------------------------------------------------------------
procedure DirectoryCopy(aSrcDir,aDestDir: string);
// -------------------------------------------------------------------

    function GetShortDir(aPath: string): string;
    var
      n: Integer;
    begin
      n:=length(aPath);
      repeat dec(n) until (n=1) or (aPath[n]='\');
      Result:=Copy(aPath,n+1,Length(aPath)-n);
    end;


var
  sr: TSearchRec;
  list: array of TSearchRec;
  I: Integer;
  nameSource,dirDest: string;

begin
// Result:=False;
 try
   if not DirectoryExists(aSrcDir) then
     Exit;

   aSrcDir :=ExcludeTrailingBackslash(aSrcDir );
   aDestDir:=ExcludeTrailingBackslash(aDestDir);

   if FindFirst(aSrcDir+'\*.*',faAnyFile,sr)<>0 then
     Exit;

   SetLength(list,0);
   repeat
     SetLength(list,Length(list)+1);
     list[High(list)]:=sr;
   until FindNext(sr)<>0;
   FindClose(sr);

   for I:=0 to High(list) do
   begin
     Application.ProcessMessages;

     nameSource:=aSrcDir+'\'+list[I].Name;
     dirDest:=aDestDir+'\'+GetShortDir(aSrcDir);

     if ((list[I].Attr and faDirectory)<>0) then
     begin
       if (list[I].Name<>'.') and (list[I].Name<>'..') then
         DirectoryCopy(nameSource,dirDest);
     end
     else
       FileCopy_Src_Dest(nameSource,dirDest+'\'+list[I].Name);
   end;

 //  Result:=True;
 except
   Exit;
 end;

end;

*)




  {
// ---------------------------------------------------------------
procedure FindFilesByMaskEx_old(const aPath, aExtensions: string; var aFiles:  TStringList);
// ---------------------------------------------------------------

// Sample call:
//FileSearchEx('C:\Temp', '.txt;.tmp;.exe;.doc', FileList);


const
  FileMask = '*.*';
var
  Rec: TSearchRec;
  sPath: string;
begin
  sPath := IncludeTrailingBackslash(aPath);

  if FindFirst(sPath + FileMask, faAnyFile - faDirectory, Rec) = 0 then
    try
      repeat
        if AnsiPos(ExtractFileExt(Rec.Name), aExtensions) > 0 then
          aFiles.Add(sPath + Rec.Name);
      until
         FindNext(Rec) <> 0;
    finally
      SysUtils.FindClose(Rec);
    end;

  if FindFirst(sPath + '*.*', faDirectory, Rec) = 0 then
    try
      repeat
        if ((Rec.Attr and faDirectory) <> 0) and (Rec.Name <> '.') and
          (Rec.Name <> '..')
        then
          FindFilesByMaskEx(sPath + Rec.Name, aExtensions, aFiles);
      until
        FindNext(Rec) <> 0;

    finally
      FindClose(Rec);
    end;
end;
  }




  {
//-------------------------------------------------------------------
function IsDirEmpty (aDir: string): boolean;
//-------------------------------------------------------------------
var
  rInfoRec: TSearchRec;
begin
  aDir:=IncludeTrailingBackslash(aDir);

  Result:= (FindFirst(aDir + '*.*',faDirectory + faAnyFile,rInfoRec)=0 )
end;
}




(*
//-------------------------------------------------------------------
procedure FindDirectoriesWithoutChilds(aDir: ShortString; var aList: TStringList);
//-------------------------------------------------------------------
var
  rInfoRec: TSearchRec;
  s       : string;
begin
  aList.Clear;

  aDir:=IncludeTrailingBackslash(aDir);

  if FindFirst(aDir + '*.*', faDirectory, rInfoRec)=0 then
  begin
    repeat
      s:= rInfoRec.Name;
      if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)>0)
        then begin
          aList.Add(s);  //aDir+
          ///DoFind (aDir+s);
        end;
    until
       FindNext(rInfoRec)<>0;

    SysUtils.FindClose(rInfoRec);
  end;
end;

*)
(*



//-------------------------------------------------------------------
procedure FindFilesByMask_11(aDir,aMask: string; aList: Tstrings; aNeedClear:
    boolean);
//-------------------------------------------------------------------
// example: FindFilesByMask_ ('t:\temp', '*.dcu', strList);
//-------------------------------------------------------------------

  procedure DoFind (aDir: string);
  var
    rInfoRec: TSearchRec;
    s       : string;
  begin
    aDir:=IncludeTrailingBackslash(aDir);

    // -------------------------
    if FindFirst(aDir + '*.*',faDirectory,rInfoRec)=0 then
    begin
      repeat
        s:=rInfoRec.Name;
        if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)<>0)
          then DoFind (aDir + s);
      until
         FindNext(rInfoRec)<>0;

      SysUtils.FindClose(rInfoRec);
    end;


    // -------------------------
    if FindFirst(aDir + aMask, faAnyFile,rInfoRec)=0 then
    begin
      repeat
        s:=rInfoRec.Name;
        if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)=0)
        then
          aList.Add(aDir + rInfoRec.Name);
      until
         FindNext(rInfoRec)<>0;

      SysUtils.FindClose(rInfoRec);
    end;

  end;


begin
  if aNeedClear then
    aList.Clear;

  if Trim(aDir)='' then
    Exit;

  DoFind (aDir);
end;
*)



(*
//------------------------------------------------------------------
function PathIsLocal11111111111(aFileName: string): boolean;
//------------------------------------------------------------------
var
  S: string;
  c: Pchar;
begin
  Result:= false;

  if (aFileName[1]='\') and (aFileName[2]='\') then
    exit;

  S:= aFileName[1]+':';
  c:= @S[1];

  Result:= GetDriveType(c) = DRIVE_FIXED;
end;
*)


{
//----------------------------------------------------
procedure BuildFileList11111(aFileMask: string; astrings: Tstrings);
//----------------------------------------------------
var SearchRec: TSearchRec;
begin
  astrings.Clear;

  try
    if (FindFirst(ExpandFileName(aFileMask), faAnyFile, SearchRec) = 0) then
      repeat
        if (SearchRec.Name[1] <> '.') and
          (SearchRec.Attr and faVolumeID <> faVolumeID) and
          (SearchRec.Attr and faDirectory <> faDirectory) then
        begin
          astrings.Add(ExtractFilePath(aFileMask) + SearchRec.Name);
        end;
      until FindNext(SearchRec) <> 0;
  finally
    SysUtils.FindClose(SearchRec);
  end;
end;
 }


 
(*
//----------------------------------------------------
procedure BuildFileNameNoExtList (aFileMask: string; astrings: Tstrings);
//----------------------------------------------------
var i: integer;
begin
  BuildFileList11111 (aFileMask, astrings);
  for i:=0 to astrings.Count-1 do
    astrings[i]:=ExtractFileNameNoExt(astrings[i]);
end;
*)



{
//-------------------------------------------------------------------
procedure RemoveEmptyDir11111111111(aDirList: TstringList);
//-------------------------------------------------------------------
var I: Integer;
  list:TstringList;
  list1:TstringList;
begin

  list:=TstringList.Create;
  list1:=TstringList.Create;

  for I := 0 to aDirList.Count-1 do
  begin
    FindFilesByMask_11(aDirList[i], '*.*', list, True);

    if list.Count > 0 then
      list1.Add(aDirList[i]);
    list.Clear;
  end;    // for

  aDirList.Clear;

  for I := 0 to list1.Count-1 do
  begin
    aDirList.Add(list1[i]);
  end;    // for

  list.Free;
  list1.Free;

end;

}



(*
//-------------------------------------------------------------------
function ClearDirRecursive (aPath: string): Boolean;
//-------------------------------------------------------------------
begin
  Result:=rxFileUtil.ClearDir (aPath, True);
end;
*)




//
//
//// ---------------------------------------------------------------
//function ScanDir1(aPath, aExtensions: string; aFiles: TStrings): Integer;
//// ---------------------------------------------------------------
//
//   procedure DoScan(aDirPath, aExtension: string);
//   var
//     rSearchRec: TSearchrec;
//
//     a,i:integer;
//
//     //oExtStrList: TStringList;
//     sExt: string;
//   begin
//     aDirPath := IncludeTrailingBackslash(aDirPath);
//
//   end;
//
//
//var
//  SearchRec: TSearchrec;
//
//  a,i:integer;
//
//  oExtStrList: TStringList;
//  sExt: string;
//
//begin
//
//
//end;


// 
//
//  Assert(Length(aPath)>=3);
//
//
//  aPath := IncludeTrailingBackslash(aPath);
//
//
// // aFiles.Clear;
//
//  oExtStrList:=TStringList.Create;
//  oExtStrList.Delimiter:=';';
//  oExtStrList.DelimitedText:=aExtensions;
//
////  aPath := IncludeTrailingBackslash(aPath);
//
//  FindFirst(aPath + '*.*',faDirectory,SearchRec); {. found}
//  FindNext(SearchRec);    {.. found}
//
//  a:=FindNext(SearchRec);
//
//  while a=0 do
//  begin
//
//    if (SearchRec.Attr and faDirectory)>0 then
//      ScanDir1(aPath + SearchRec.Name, aExtensions, aFiles);
//
//    a:=FindNext(SearchRec);
//
//  end;
//
//  FindClose(SearchRec);
//
//
//  for i:=0 to oExtStrList.Count-1 do
//  begin
//    sExt :=oExtStrList[i];
//
//    a:=FindFirst(aPath + sExt, faAnyFile, SearchRec);
//
//    while a=0 do
//    begin
//      aFiles.Add(aPath + SearchRec.Name);
//
//      a:=FindNext(SearchRec);
//    end;
//
//    FindClose(SearchRec);
//  end;
//
//  FreeAndNil(oExtStrList);
//
//  Result := aFiles.Count;


{


// for all platforms (Windows\Unix), uses IOUtils.
function ReplaceInvalidFileNameChars(const aFileName: string; const aReplaceWith: Char = '_'): string;
var
  i: integer;
begin
  Result := aFileName;
  for i := Low(Result) to High(Result) do
    if not TPath.IsValidFileNameChar(Result[i]) then
      Result[i] := aReplaceWith;
  end;
end.




{
function GetApplicationFileDateAsStr: string;
begin
  Result :=FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));
  // TODO -cMM: GetApplicationFileDateAsStr default body inserted
//  Result := ;
end;
 }


 {
// ---------------------------------------------------------------
procedure ShellExec_Notepad_temp(aText: string; aExt: string = '');
// ---------------------------------------------------------------
var
  sFile: string;
begin
  sFile :=SysGetTempFileName(aExt);

  strToTxtFile(aText, sFile);

 // ShellExecute(Handle,
 // zz'open', 'c:\MyDocuments\Letter.doc', nil, nil, SW_SHOWNORMAL);

  ShellExecute(0, 'open',
     'notepad.exe', PChar(sFile), nil, SW_SHOWNORMAL);

 // if aFileName<>'' then
 //   ShellExecute(0,nil, pChar(string(aFileName)), pChar(string(aFileParams)), nil, SW_NORMAL);

//  ShellExecute(0,nil, pChar(aFileName), pChar(aFileParams), nil, SW_NORMAL);
end;
  }


{
//------------------------------------------------
function SysGetTempFileName(aExt: string = '.tmp'): string;
//------------------------------------------------
begin
  Randomize;

  Result:=GetTempFileDir() + IntToStr(Random (1000000)) + aExt;
  if FileExists (Result) then
    Result:=SysGetTempFileName();
end;


//------------------------------------------------
function GetTempFileNameWithExt(aExt: string): string;
//function GetTempFileNameWithExt (aExt: string): string;
//------------------------------------------------
begin
  Result:=SysGetTempFileName();
  if aExt<>'' then
    Result:=ChangeFileExt (Result, '.'+aExt);
end;



function GetTempXmlFileName(): string;
begin
  Result:=GetTempFileNameWithExt('xml');
end;
}




//function GetParentFolderName_v1 (aFileName: string): string;
//begin
//  Result:=IncludeTrailingBackslash(ExtractFileDir (ExcludeTrailingBackslash(aFileName)));
//end;



(*
function ChangeParentFileDir1111(aFileName,aNewDir: string): string;
var
  sDir: string;
begin
  sDir:=ExtractFileDir(ExtractFileDir(aFileName));
  sDir:=IncludeTrailingBackslash(sDir) + aNewDir + '\';
  Result:=sDir + ExtractFileName(aFileName);
end;

*)


   {
//------------------------------------------------
function GetTempFileDir (): string;
//------------------------------------------------
begin
  Result:= IncludeTrailingBackslash ( GetTempDir() );
end;


procedure MoveFile (aFileName, aDestName: string);
begin
  rxFileUtil.MoveFile (aFileName, aDestName);
end;



function dlg_BrowseDirectory(var AFolderName: string; aDlgText: string): Boolean;
var
  S: string;
begin
  s:=AFolderName;
  Result := rxFileUtil.BrowseDirectory(s,aDlgText,0);
  AFolderName:=s;
end;

 }




{
//------------------------------------------------------
function mapx_FileSize(aFileName: string): int64;
//------------------------------------------------------
begin
  Result:= -1;

  if not mapx_FileExists(aFileName) then
    exit;

  Result:= GetFileSize_(ChangeFileExt (aFileName, '.dat')) +
           GetFileSize_(ChangeFileExt (aFileName, '.id')) +
           GetFileSize_(ChangeFileExt (aFileName, '.map')) +
           GetFileSize_(ChangeFileExt (aFileName, '.tab'));

  if FileExists(ChangeFileExt (aFileName, '.ind')) then
    Result:= Result +
           GetFileSize_(ChangeFileExt (aFileName, '.ind'));
end;
 }

{
// ---------------------------------------------------------------
function GetFileSize_(aFileName: string): Int64;
// ---------------------------------------------------------------
begin
  Result:= RxFileUtil.GetFileSize_ (aFileName);

end;
 }

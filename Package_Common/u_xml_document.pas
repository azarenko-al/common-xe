unit u_xml_document;

interface
uses
   Forms, XMLDoc,  Xml.XMLIntf,  System.SysUtils, System.Variants,

   u_files,

   u_func;


type
  TXmlDocumentEx = class
  private
  public
    XmlDocument: TXmlDocument;

    DocumentElement: IXMLNode;

    procedure SaveToFile(const AFileName: String);
    procedure LoadFromFile(const AFileName: String);

    procedure LoadFromText(const AText: String);

    constructor Create;

    destructor Destroy; override;
    function SaveToText: string;

    procedure InitKML;
  end;

//  TXMLDoc = TXmlDocumentEx;


  TxmlAttrib = record
    Name: string;
    Value: Variant;
  end;

  function xml_GetIntAttr    (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0): Integer;
  function xml_GetFloatAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''): Double;
  function xml_GetStrAttr(aNode: IXMLNode; aAttrName: string; aDefault:string=''): string;



  function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
  procedure xml_SetAttribs(aNode: IXMLNode; aAttribs: array of TxmlAttrib);

  function xml_GetAttr(aNode: IXMLNode; aAttrName: string; aDefault: string=''):
      Variant;

  function xml_AddNode111(aParentNode: IXMLNode; aTagName: string; aAttribs: array  of TxmlAttrib): IXMLNode;

  function xml_AddNode(aParentNode: IXMLNode; aTagName: string; aValues: array of
      Variant): IXMLNode;


  function xml_Par(aName: string; aValue: Variant): TxmlAttrib;

  function xml_FindNode(aNode: IXMLNode; aTagName: string): IXMLNode;

  function xml_AddNodeTag(aParentNode: IXMLNode; aTagName: string): IXMLNode;

  function xml_FindNodeAttr(aNode: IXMLNode; aTagName, aAttName: string): string;

  function xml_IsElement(aNode: IXMLNode): boolean;

procedure xml_UpdateNode(aNode: IXMLNode; aAttribs: array of TxmlAttrib);

function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):  IXMLNode;

const
  DEF_FILTER_XML = 'XML (*.xml)|*.xml';


const
  TAG_PARAM  = 'param';

  ATT_ID                = 'id';
  ATT_VALUE             = 'value';
  ATT_COMMENT          = 'comment';

  ATT_NAME = 'name';

  ATT_ANGLE = 'ANGLE';

  TAG_ITEM               = 'ITEM';
  TAG_FILEDIR           = 'FILEDIR';
  TAG_FILENAME          = 'FILENAME';
  TAG_GROUP             = 'GROUP';

//  ATT_ID                = 'id';
 // ATT_VALUE             = 'value';

  ATT_CODE              = 'code';

  ATT_HINT              = 'hint';

  ATT_WATER_CLUTTER_CODE='WATER_CLUTTER_CODE';
  ATT_WATER_COLOR       ='WATER_CLUTTER';

  ATT_COMMENTS          = 'comments';

  ATT_HEIGHT            = 'HEIGHT';
  ATT_HEIGHT_MIN        = 'HEIGHT_MIN';
  ATT_HEIGHT_MAX        = 'HEIGHT_MAX';
  ATT_COLOR_MIN         = 'COLOR_MIN';
  ATT_COLOR_MAX         = 'COLOR_MAX';

//  ATT_KUP_FILENAME      = 'KUP_FILENAME';
//  ATT_KGR_FILENAME      = 'KGR_FILENAME';

  ATT_KupFileName      = 'KupFileName';
  ATT_KGRFILENAME      = 'KGRFILENAME';



  ATT_IS_FOLDER         = 'is_folder';
  ATT_CAPTION           = 'caption';
  ATT_PARENT_ID         = 'parent_id';
  ATT_children          = 'children';
  ATT_ReadOnly          ='is_readonly';
 // ATT_COMMENT           = 'comment';
  ATT_TYPE              = 'type';
 // ATT_NAME              = 'name';
  ATT_ITEMS             = 'items';
  ATT_PARAMS            = 'PARAMS';
  ATT_PARENT_TYPE       = 'parent_type';
  ATT_XREF_ObjectName   = 'XREF_ObjectName';
  ATT_XREF_FieldName    = 'XREF_FieldName';
  ATT_COLOR             = 'color';
  ATT_FILEDIR           = 'FILEDIR';

  ATT_MIN_VALUE ='MIN_VALUE';
  ATT_MAX_VALUE ='MAX_VALUE';
  ATT_MIN_COLOR ='MIN_COLOR';
  ATT_MAX_COLOR ='MAX_COLOR';





  ATT_LAT               = 'b';
  ATT_LON               = 'l';
  ATT_SITE_NAME         = 'SITE_NAME';

  ATT_FILENAME          = 'FILENAME';
  ATT_CHECKED           = 'CHECKED';
  ATT_EXPANDED          = 'EXPANDED';
  ATT_ENABLED           = 'ENABLED';
  ATT_KEY               = 'key';

  ATT_ALLOW_ZOOM        = 'ALLOW_ZOOM';
  ATT_AUTOLABEL         = 'AutoLabel';
  ATT_LabelBackColor    = 'LabelBackColor';
  ATT_ZOOM_MIN          = 'ZOOM_MIN';
  ATT_ZOOM_MAX          = 'ZOOM_MAX';
  ATT_Selectable        = 'SELECTABLE';

  ATT_CELL_NAME         = 'CELL_NAME';
 // TAG_PARAM             = 'param';
  ATT_FREQ              = 'FREQ';



implementation


// ---------------------------------------------------------------
constructor TXmlDocumentEx.Create;
// ---------------------------------------------------------------
var
  vChild: IXMLNode;
begin
  XmlDocument:=TXMLDocument.Create(Application);
//  XmlDocument:=TXMLDocument.Create(nil);

  XmlDocument.Options:=XmlDocument.Options + [doNodeAutoIndent];

  XmlDocument.Active:=true;

  XmlDocument.Version:='1.0';
 // XmlDocument.Encoding:='UTF-8';
  XMLDocument.Encoding:='windows-1251';
                             
//  XmlDocument.DOMDocument.appendChild(
//      XmlDocument.DOMDocument.createProcessingInstruction(
//      'xml-stylesheet', 'type="text/xsl" href=""'));


  DocumentElement:=XmlDocument.AddChild('Document');


(*
  with XMLDocument1 do
  begin
    DOMDocument.appendChild(
      DOMDocument.createProcessingInstruction(
      'xml-stylesheet', 'type="text/xsl" href="example.xsl"'));
  end;

*)


(*
  vChild:=XmlDocument.AddChild('kml');
  vChild.Attributes['xmlns']:='http://earth.google.com/kml/2.2';

  Doc:=vChild.AddChild('Document');



//  inherited Create(AOwner);

 // kmldoc:=TXMLDocument.Create(Application);
  Options:=Options + [doNodeAutoIndent];
  Active:=true;
 // Version:='1.0';
 // Encoding:='UTF-8';

  vChild:=AddChild('kml');
  vChild.Attributes['xmlns']:='http://earth.google.com/kml/2.2';

  Document:=vChild.AddChild('Document');
*)

(*  kmldoc:=TXMLDocument.Create(Application);
  kmldoc.Options:=kmldoc.Options + [doNodeAutoIndent];
  kmldoc.Active:=true;
  kmldoc.Version:='1.0';
  kmldoc.Encoding:='UTF-8';
  vChild:=kmldoc.AddChild('kml');
  vChild.Attributes['xmlns']:='http://earth.google.com/kml/2.2';
  Doc:=vChild.AddChild('Document');
*)

end;


destructor TXmlDocumentEx.Destroy;
begin
  FreeAndNil(XmlDocument);

  inherited;
end;


// ---------------------------------------------------------------
procedure TXmlDocumentEx.InitKML;
// ---------------------------------------------------------------
var
  i: Integer;
  s: string;
  vChild: IXMLNode;
begin
  XmlDocument.ChildNodes.Clear;

  XmlDocument.Version:='1.0';
 // XmlDocument.Encoding:='UTF-8';

//  XMLDocument1.Version :='1.0';
  XMLDocument.Encoding:='UTF-8';

(*
  <?xml version="1.0" encoding="iso-8859-1"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2">
*)

  vChild:=XmlDocument.AddChild('kml');

//  DocumentElement:=vChild.AddChild('Document');


  vChild.Attributes['xmlns']:='http://www.opengis.net/kml/2.2';
  vChild.Attributes['xmlns:gx']:='http://www.google.com/kml/ext/2.2';

//  XMLDocument.NSPrefixBase := '';


  DocumentElement:=vChild.AddChild('Document');


end;

procedure TXmlDocumentEx.SaveToFile(const AFileName: String);
begin
  ForceDirByFileName(aFileName);

  XmlDocument.SaveToFile(ChangeFileExt(AFileName, '.xml'));
//  XmlDocument.SaveToFile(AFileName);
end;

procedure TXmlDocumentEx.LoadFromFile(const AFileName: String);
var
  k: Integer;
  s: string;
  v: IXMLNode;
begin
  Assert(FileExists(aFileName));


  if not FileExists(aFileName) then
    Exit;

  XmlDocument.LoadFromFile(AFileName);


//  v := XmlDocument.DocumentElement;
 // s:=v.LocalName;

//  Document:=XmlDocument.ChildNodes.FindNode('Document');
  DocumentElement:=XmlDocument.DocumentElement;

 
 // if VarIsNull(vRoot) then
  //  Exit;


  k:=DocumentElement.ChildNodes.Count;
  Assert(k>0);

end;


procedure TXmlDocumentEx.LoadFromText(const AText: String);
begin
  XmlDocument.Active := False;
  XmlDocument.XML.Text :=aText;
  XmlDocument.Active := True;

  DocumentElement:=XmlDocument.DocumentElement;

///  XmlDocument.SaveToFile('c:\P111222.xml');

end;


function TXmlDocumentEx.SaveToText: string;
begin
  Result := XmlDocument.XML.Text;
end; 


//--------------------------------------------------------------------
function xml_GetAttr(aNode: IXMLNode; aAttrName: string; aDefault: string=''):
    Variant;
//--------------------------------------------------------------------
var
  i: integer;
  sName: string;
  vNode: IXMLNode;

begin
  Assert(aNode<>nil);


  for i:=0 to aNode.AttributeNodes.Count-1 do
  begin
    vNode:=aNode.AttributeNodes.Nodes[i];
    sName:=vNode.LocalName;
    if Eq(sName, aAttrName) then begin Result:=vNode.Text; Exit; end;
  end;

  Result:=aDefault;
end;



//--------------------------------------------------------------------
function xml_AddNode111(aParentNode: IXMLNode; aTagName: string; aAttribs: array  of TxmlAttrib): IXMLNode;
//--------------------------------------------------------------------
var i : integer;
begin
  Result := aParentNode.AddChild(aTagName);

  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
     Result.Attributes[aAttribs[i].Name] := aAttribs[i].Value;
   except end;

end;


//--------------------------------------------------------------------
procedure xml_UpdateNode(aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//--------------------------------------------------------------------
var i : integer;
begin
 // Result := aParentNode.AddChild(aTagName);

  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
     aNode.Attributes[aAttribs[i].Name] := aAttribs[i].Value;
   except end;

end;


function xml_Att (aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=LowerCase(aName);
  Result.Value:=aValue;
end;


//--------------------------------------------------------------------
procedure xml_SetAttribs (aNode: IXMLNode; aAttribs: array of TxmlAttrib);
//--------------------------------------------------------------------
var i : integer;
begin
  for i := 0 to High(aAttribs) do
   if not VarIsNull (aAttribs[i].Value) then
   try
     aNode.SetAttribute (aAttribs[i].Name, aAttribs[i].Value);
   except end;
end;




function xml_Par(aName: string; aValue: Variant): TxmlAttrib;
begin
  Result.Name:=aName;
  Result.Value:=aValue;
end;


function  xml_GetIntAttr  (aNode: IXMLNode; aAttrName: string; aDefault:Integer=0):  Integer;
begin
  Result := AsInteger(xml_GetAttr(aNode,aAttrName, IntToStr(aDefault)));
end;

function xml_GetFloatAttr  (aNode: IXMLNode; aAttrName: string; aDefault:string=''):  Double;
begin
  Result := AsFloat(xml_GetAttr(aNode,aAttrName,aDefault));
end;

function xml_GetStrAttr(aNode: IXMLNode; aAttrName: string;
    aDefault:string=''): string;
begin
  Result := AsString(xml_GetAttr(aNode,aAttrName,aDefault));
end;

//-----------------------------------------------------------
function xml_FindNodeAttr(aNode: IXMLNode; aTagName, aAttName: string): string;
//-----------------------------------------------------------
var
  vNode: IXMLNode;
begin
  vNode:=xml_FindNode (aNode, aTagName);
//  if VarExists(vNode) then
  if vNode<>nil then
    Result := xml_GetAttr(vNode, aAttName)

  else
    Result := '';

 //    sValue:=xml_GetAttrByTagName (aParamsNode, 'MapType', 'value');

end;




//-----------------------------------------------------------
function xml_FindNode(aNode: IXMLNode; aTagName: string): IXMLNode;
//-----------------------------------------------------------
var
  i:integer;
  vNode: IXMLNode;
begin
  if Assigned(aNode) then
    for i:=0 to aNode.childNodes.Count-1 do
    begin
      vNode:=aNode.childNodes[i];

      if Eq(vNode.LocalName, aTagName) then
      begin
        Result := vNode;
        Exit;
      end;
    end;

  Result:=nil;

end;

//--------------------------------------------------------------------
function xml_AddNodeTag(aParentNode: IXMLNode; aTagName: string): IXMLNode;
//--------------------------------------------------------------------
begin
  Result := aParentNode.AddChild(aTagName);
end;


function xml_IsElement(aNode: IXMLNode): boolean;
begin
  Result:=(aNode.NodeType =ntElement);
  //  nodeTypeString = 'element');
end;


 {
// ---------------------------------------------------------------
procedure xml_Transform(aInXML, aInXSL, aOutXML: String);
// ---------------------------------------------------------------
var
  oInXML:  TXMLDocument;
  oXSL:    TXMLDocument;
  oOutXML: TXMLDocument;
begin

  oInXML  := TXMLDocument.Create(nil);
  oXSL    := TXMLDocument.Create(nil);
  oOutXML := TXMLDocument.Create(nil);

  try
    oInXML.LoadFromFile(AInXML);
    oXSL.LoadFromFile(AInXSL);
    oOutXML.Active := True;
    oInXML.DocumentElement.TransformNode(oXSL.DocumentElement, oOutXML);
    oOutXML.SaveToFile(AOutXML);
  finally
    oInXML.Free;
    oXSL.Free;
    oOutXML.Free;
  end;
end;
}

// ---------------------------------------------------------------
function xml_AddNode(aParentNode: IXMLNode; aTagName: string; aValues: array of
    Variant): IXMLNode;
// ---------------------------------------------------------------
var i : Integer;
begin
  Assert(aTagName<>'');
  Assert(Assigned(aParentNode));
  Assert(Length(aValues) mod 2 = 0);


  Result := aParentNode.AddChild(aTagName);


  for i := 0 to (Length(aValues) div 2)-1 do
   if not VarIsNull (aValues[i*2]) then
   try
     Result.Attributes[aValues[i*2]] := aValues[i*2+1];
   except
   end;
end;



//-----------------------------------------------------------
function xml_GetNodeByPath(aNode: IXMLNode; aPathArr: array of string):  IXMLNode;
//-----------------------------------------------------------
var i,j:integer;
  vElement: IXMLNode;
  strArr: array of string;
  sTag: string;
begin
  Result:=nil;

  if VarIsNull(aNode) then
    Exit;

//  iCOunt:=aNode.childNodes.Length;

  if High(aPathArr)>=0 then
    for i:=0 to aNode.childNodes.Count-1 do
    begin
      vElement:=aNode.childNodes[i];

      if not xml_IsElement(vElement) then
        Continue;

      sTag:=vElement.LocalName;// Ta gName;

      if sTag='' then Continue;

      if Eq (sTag, aPathArr[0]) then
        if High(aPathArr)=0
         then begin
           Result:=vElement;
           Exit;
         end else
         begin
           SetLength(strArr, High(aPathArr));
           for j:=0 to High(aPathArr)-1 do
             strArr[j]:=aPathArr[j+1];

           Result:=xml_GetNodeByPath (vElement, strArr);
         end;
    end;


end;








(*
var
  o: TXmlDocumentEx;
*)
begin

(*
  o:=TXmlDocumentEx.Create(nil);
  o.LoadFromFile();
*)

  //FreeAndNil(o);

end.
